#! python3.4
from pysupply import __version__

APPLICATION_NAME = "pysupply"
APPLICATION_AUTHOR = "Stefan Lehmann"
APPLICATION_VERSION = __version__

import pysupply.logging as logging
logging.init_logger(APPLICATION_NAME + ".log")
from pysupply.logging import logger

import sys
from PyQt5.QtCore import QLocale, QTranslator, QSettings
from PyQt5.QtWidgets import QApplication
from pysupply.gui.mainwindow import MainWindow
import pysupply.logging as logging


if "--debug" in sys.argv:
    from PyQt5.QtCore import pyqtRemoveInputHook
    pyqtRemoveInputHook()

logger.debug("=== Starting Application === ")
# do backup of database
import pysupply.backup

app = QApplication(sys.argv)
app.setApplicationName(APPLICATION_NAME)
app.setApplicationVersion(APPLICATION_VERSION)
app.setOrganizationName(APPLICATION_AUTHOR)

# Translation
locale = QLocale.system().name().split("_")

# Translator for Qt internals
qtTranslator = QTranslator()
if qtTranslator.load("qtbase_" + locale[0], ":/transl"):
    app.installTranslator(qtTranslator)
    logger.debug("Translation for Qt internals loaded")

# Translator for Application texts
appTranslator = QTranslator()
if appTranslator.load("pysupply_" + locale[0], ":/transl"):
    app.installTranslator(appTranslator)
    logger.debug("Translation for application texts loaded")

if "--reset" in sys.argv:
    settings = QSettings()
    settings.clear()

main = MainWindow()
main.show()
app.exec_()
