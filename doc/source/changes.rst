Changelog
=========
Version 1.0.4
-------------
* changed 'documents' dir in 'articles'
* orderitems table manufacturer column now with Autocomplete feature

Version 1.0.3
-------------
* backup mechanism for database
* fixed bug related to usage of multiple session objects
* sort suppliers in combobox of articlelist

Version 1.0.2
-------------
* add documents to order
* directories for projects
* update catalog treeview if catalog items are added or removed to/from article
* lock orders
* find ordered articles in the catalog catalog via context menu

Version 1.0.1
-------------
* properties for articles (property, value), maybe in one column
* add and remove orders