.. pysupply documentation master file, created by
   sphinx-quickstart on Mon Jul 28 07:55:08 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysupply's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 3

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

