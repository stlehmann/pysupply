from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
order_items = Table('order_items', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('order_id', Integer),
    Column('catalog_id', Integer),
    Column('distributor_id', Integer),
    Column('article_id', Integer),
    Column('count', Integer, default=ColumnDefault(1)),
    Column('name', String, default=ColumnDefault('')),
    Column('articlenr', String, default=ColumnDefault('')),
    Column('manufacturer', String),
    Column('manufacturer_nr', String),
    Column('ppu', Float),
    Column('packing_unit', Float),
    Column('comment', String, default=ColumnDefault('')),
    Column('position', Integer, default=ColumnDefault(0)),
    Column('added_to_stock', Boolean, default=ColumnDefault(False)),
    Column('received', Boolean, default=ColumnDefault(False)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['order_items'].columns['packing_unit'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['order_items'].columns['packing_unit'].drop()
