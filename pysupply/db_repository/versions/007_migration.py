from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
order_items = Table('order_items', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('order_id', INTEGER),
    Column('catalog_id', INTEGER),
    Column('distributor_id', INTEGER),
    Column('article_id', INTEGER),
    Column('count', INTEGER),
    Column('name', VARCHAR),
    Column('articlenr', VARCHAR),
    Column('manufacturer', VARCHAR),
    Column('manufacturer_nr', VARCHAR),
    Column('ppu', FLOAT),
    Column('comment', VARCHAR),
    Column('position', INTEGER),
    Column('added_to_stock', BOOLEAN),
    Column('received', BOOLEAN),
    Column('packing_unit', FLOAT),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['order_items'].columns['packing_unit'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['order_items'].columns['packing_unit'].create()
