from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
withdrawals = Table('withdrawals', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('article_id', INTEGER),
    Column('quantity', INTEGER),
    Column('comment', VARCHAR(length=256)),
    Column('removed_from_stock', BOOLEAN),
)

stockchanges = Table('stockchanges', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('article_id', Integer),
    Column('quantity_change', Integer),
    Column('comment', String(length=256)),
    Column('applied', Boolean, default=ColumnDefault(False)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['withdrawals'].drop()
    post_meta.tables['stockchanges'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['withdrawals'].create()
    post_meta.tables['stockchanges'].drop()
