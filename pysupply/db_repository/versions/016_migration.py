from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
withdrawals = Table('withdrawals', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('article_id', Integer),
    Column('quantity', Integer),
    Column('comment', String(length=256)),
    Column('removed_from_stock', Boolean, default=ColumnDefault(False)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['withdrawals'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['withdrawals'].drop()
