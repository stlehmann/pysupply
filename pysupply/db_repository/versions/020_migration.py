from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
stockchanges = Table('stockchanges', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('article_id', Integer),
    Column('original_quantity', Integer),
    Column('quantity_change', Integer),
    Column('resulting_quantity', Integer),
    Column('comment', String(length=256)),
    Column('applied', Boolean, default=ColumnDefault(False)),
    Column('date', Date),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['stockchanges'].columns['original_quantity'].create()
    post_meta.tables['stockchanges'].columns['resulting_quantity'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['stockchanges'].columns['original_quantity'].drop()
    post_meta.tables['stockchanges'].columns['resulting_quantity'].drop()
