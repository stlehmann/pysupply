from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
material = Table('material', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('quantity', Integer, default=ColumnDefault(0)),
    Column('article_id', Integer),
    Column('materiallist_id', Integer),
    Column('order_item_id', Integer),
    Column('removed_from_stock', Boolean, default=ColumnDefault(False)),
    Column('ordered', Boolean, default=ColumnDefault(False)),
    Column('comment', String, default=ColumnDefault('')),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['material'].columns['order_item_id'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['material'].columns['order_item_id'].drop()
