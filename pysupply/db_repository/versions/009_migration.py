from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
distributors = Table('distributors', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String),
    Column('address', String),
    Column('contact_person', String(length=64)),
    Column('conditions', String),
    Column('shipping_costs', Float),
    Column('website', String),
    Column('username', String),
    Column('password', String),
    Column('plugin_name', String),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['distributors'].columns['contact_person'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['distributors'].columns['contact_person'].drop()
