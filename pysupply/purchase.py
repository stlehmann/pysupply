__author__ = 'Lehmann'

import itertools
from pysupply.db import OrderItems, Purchase


class PurchaseOption(object):
    def __init__(self):
        self.purchases = []
        self.purchase_items = []

    def __repr__(self):
        return "<PurchaseOption(purchase_count=%i, item_count=%i, total=%.2f)>" % \
               (self.count(), self.item_count(), self.total())

    def total(self):
        return sum(p.total() for p in self.purchases)

    def count(self):
        return len(self.purchases)

    def item_count(self):
        return len(self.purchase_items)


def create_purchase_options(requisitions):
    def _create_purchase_items():
        def _purchase_item_gen():
            for req_item in requisitions:
                yield [(req_item, catalog_item)
                       for catalog_item in req_item.article.catalog_items]
        return list(_purchase_item_gen())

    def _create_possibilities(purchase_items):
        def _possibility_gen():
            raw_possibilities = itertools.product(*_create_purchase_items())
            for pos in raw_possibilities:
                possibility = PurchaseOption()
                for req_item, catalog_item in pos:
                    purchase_item = OrderItems()
                    purchase_item.requisition_item = req_item
                    purchase_item.catalog_item = catalog_item
                    possibility.purchase_items.append(purchase_item)
                yield possibility
        return list(_possibility_gen())

    def _create_purchases(possibilities):
        for possibility in possibilities:
            possibility_ = PurchaseOption()

            distributors = []
            for purchase_item in possibility.purchase_items:
                distributor = purchase_item.catalog_item.distributor
                if distributor not in distributors:
                    distributors.append(distributor)
                    purchase = Purchase(distributor=distributor)
                    possibility_.purchases.append(purchase)
                else:
                    index = distributors.index(distributor)
                    purchase = possibility_.purchases[index]
                purchase_item.purchase = purchase
                possibility_.purchase_items.append(purchase_item)
            yield possibility_

    purchase_items = _create_purchase_items()
    options = _create_possibilities(purchase_items)
    sorted_options = sorted(_create_purchases(options), key=lambda p: p.total())
    return sorted_options