__author__ = 'lehmann'

import os
import re
import pickle
import glob
from shutil import copyfile
from datetime import timedelta, datetime

from pysupply.config import HOME, DB_FILENAME
from pysupply.logging import logger

BACKUP_INTERVAL = timedelta(minutes=30)
BACKUP_COUNT = 10
BACKUP_BASENAME = "pysupply_backup"


def filepath(i):
    return os.path.join(HOME, BACKUP_BASENAME + "%i.db" % i)


def get_filename(i):
    return BACKUP_BASENAME + "%i.db" % i


def number(filename: str):
    res = re.search("\d+", filename)
    assert res is not None
    return int(res.group())


def backup():
    db_filepath = os.path.join(HOME, DB_FILENAME)
    files = glob.glob(os.path.join(HOME, BACKUP_BASENAME + "*.db"))
    sources = map(os.path.basename, files)
    sources = sorted(sources, key=lambda file: number(file))
    destinations = [
        get_filename(i) for (i, file) in enumerate(sources, start=2)
    ]

    count = len(sources)
    for i, (src, dst) in enumerate(reversed(list(zip(sources, destinations)))):
        if (count - i + 1) > BACKUP_COUNT:
            os.remove(os.path.join(HOME, src))
            logger.debug("Deleted old Backupfile '%s'" % src)
        else:
            os.rename(os.path.join(HOME, src),
                 os.path.join(HOME, dst))
            logger.debug("Moved Backupfile '%s' to '%s'" % (src, dst))

    copyfile(db_filepath, filepath(1))
    logger.debug("Database Backup to '%s'" % filepath(1))


try:
    with open("backup.pkl", "rb") as f:
        last_backup = pickle.load(f)
except FileNotFoundError:
    last_backup = None

if last_backup is None or datetime.now() >= last_backup + BACKUP_INTERVAL:
    logger.debug("Start Database Backup")
    backup()
    last_backup = datetime.now()
    with open("backup.pkl", "wb") as f:
        pickle.dump(last_backup, f)
else:
    diff = last_backup + BACKUP_INTERVAL - datetime.now()
    logger.debug("Time 'til next Database Backup: %s" % str(diff))
