import os
import logging
import traceback


logger = None


def log_uncaught_exceptions(ex_cls, ex, tb):
    if logger is None:
        return
    logger.critical(''.join(traceback.format_tb(tb)))
    logger.critical('{0}: {1}'.format(ex_cls, ex))


def init_logger(filename):
    global logger
    logger = logging.getLogger("pysupply")
    logger.setLevel(logging.DEBUG)

    # delete old logging file
    if os.path.isfile(filename):
        os.remove(filename)

    # filehandler for logging in file
    fh = logging.FileHandler(filename)
    fh.setLevel(logging.DEBUG)

    # streamhandler for Concole output
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # formatter
    formatter = logging.Formatter("%(asctime)s - %(name)s %(levelname)s: "
                                  "%(message)s")
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    # add handlers to logger
    logger.addHandler(fh)
    logger.addHandler(ch)
    # sys.excepthook = log_uncaught_exceptions
