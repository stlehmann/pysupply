"""
mainwindow.py - Mainwindow for pysupply GUI

copyright (c) 2015 by Stefan Lehmann
licenced under the MIT license

"""

import sys
import functools
import logging

from PyQt5.QtCore import Qt, QSettings, QCoreApplication
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QMainWindow, QApplication, QDockWidget, \
    QMenuBar, QDialog, QMdiArea, QMessageBox, QToolBar, QFileDialog

from pysupply.gui.article import ArticleDialog
from pysupply.gui.catalog import CatalogWidget
from pysupply.gui.distributors import DistributorsDialog
from pysupply.gui.helper import create_action
from pysupply.gui.order import OrderWidget
from pysupply.gui.dialogs import OrderDialog
from pysupply.gui.project import ProjectWidget
from pysupply.gui.projectexplorer import ProjectExplorer, ProjectDialog
from pysupply.gui.articlelist import ArticlelistWidget
from pysupply.gui.material import MaterialWidget
from pysupply.gui.user_information import UserDialog
from pysupply.plugins.docx.export import export_order_to_docx
import pysupply.db as db
from pysupply.db import session


S_GRP_MAINWINDOW = "pysupply/mainwindow"
S_WINDOW_STATE = "window_state"
S_WINDOW_GEOMETRY = "window_geometry"
S_STOCK_ONLY = "stock_only"
S_OPENED_WINDOWS = "opened_windows"

logger = logging.getLogger("pysupply.gui.mainwindow")


class MainWindow(QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.mdi = QMdiArea()
        self.mdi.subWindowActivated.connect(self.refresh_menu)

        self.init_dockwidgets()
        self.init_actions()
        self.init_menus()
        # self.init_toolbars()

        self.projectexplorer.lockstate_changed.connect(self.lockstate_changed)

        self.setCentralWidget(self.mdi)
        self.resize(800, 600)
        self.load_settings()

    def init_menus(self):
        menubar = QMenuBar()

        # file menu
        self.fileMenu = menubar.addMenu(self.tr("&File"))
        self.fileMenu.addAction(self.orderOnlineAction)
        self.fileMenu.addActions([

            self.newProjectAction,
            self.openProjectAction
        ])
        self.fileMenu.addSeparator()
        self.fileMenu.addActions([
            self.newOrderAction,
            self.openOrderAction,
            self.saveOrderAction,
            self.showUserInformationAction,
            self.exportOrderToDocxAction
        ])
        self.fileMenu.addSeparator()
        self.fileMenu.addActions([
            self.quitAction
        ])

        # view menu
        self.viewMenu = menubar.addMenu(self.tr("&View"))
        self.viewMenu.addActions([
            self.projectsDockWidget.toggleViewAction(),
            self.catalogDockWidget.toggleViewAction(),
            self.showArticlelistAction,
            self.stockOnlyAction
        ])
        self.viewMenu.addSeparator()
        self.viewMenu.addActions([
            self.showDistributorsAction
        ])

        # windows menu
        self.windowMenu = menubar.addMenu(self.tr("&Windows"))
        self.windowMenu.aboutToShow.connect(self.update_window_menu)
        self.setMenuBar(menubar)

        # info menu
        menubar.addAction(self.infoAction)

    def init_actions(self):
        # new order
        self.newOrderAction = create_action(
            self, self.tr("New order"), image=":icons/new.png",
            slot=self.new_order
        )

        # open order
        self.openOrderAction = create_action(
            self, self.tr("Open order..."), image=":icons/open.png",
            shortcut=QKeySequence.Open, slot=self.open_order
        )

        # save order
        self.saveOrderAction = create_action(
            self, self.tr("&Save"), image=":icons/save.png",
            shortcut=QKeySequence.Save, slot=self.save
        )

        # new project
        self.newProjectAction = create_action(
            self, self.tr("&New project"), image=":icons/new.png",
            shortcut=QKeySequence.New, slot=self.new_project
        )

        # open project
        self.openProjectAction = create_action(
            self, self.tr("Open project..."), image=":icons/open.png",
            shortcut="ctrl+shift+o", slot=self.show_projectdlg
        )

        # user settings
        self.showUserInformationAction = create_action(
            self, self.tr('User Information...'), slot=self.show_user_information
        )

        # export order to docx
        self.exportOrderToDocxAction = create_action(
            self, self.tr('Export order to docx...'), slot=self.handle_export_to_docx
        )

        # quit
        self.quitAction = create_action(
            self, self.tr("Quit"), shortcut=QKeySequence.Quit,
            image=":icons/quit.png", slot=self.close
        )

        # show distributors
        self.showDistributorsAction = create_action(
            self, self.tr("Show distributors..."), slot=self.show_distributors
        )

        # show articlelist
        self.showArticlelistAction = create_action(
            self, self.tr("articlelist"), slot=self.show_articlelist
        )

        # order online
        self.orderOnlineAction = create_action(
            self, self.tr('order online'), slot=self.order_online
        )

        # stock only
        self.stockOnlyAction = create_action(
            self, self.tr("stock only"), checkable=True,
            slot=self.stock_only_changed
        )

        # window next
        self.windowNextAction = create_action(
            self, self.tr("&Next"), QKeySequence.NextChild,
            slot=self.mdi.activateNextSubWindow
        )

        # window previous
        self.windowPrevAction = create_action(
            self, self.tr("&Previous"), QKeySequence.PreviousChild,
            slot=self.mdi.activatePreviousSubWindow
        )

        # window cascase
        self.windowCascadeAction = create_action(
            self, self.tr("Casca&de"),
            slot=self.mdi.cascadeSubWindows
        )

        # window tile
        self.windowTileAction = create_action(
            self, self.tr("&Tile"),
            slot=self.mdi.tileSubWindows
        )

        # window restore
        self.windowRestoreAction = create_action(
            self, self.tr("&Restore All"),
            slot=self.windowRestoreAll
        )

        # window minimize
        self.windowMinimizeAction = create_action(
            self, self.tr("&Iconize All"),
            slot=self.windowMinimizeAll
        )

        # window close
        self.windowCloseAction = create_action(
            self, self.tr("&Close"),
            slot=self.mdi.closeActiveSubWindow, shortcut=QKeySequence.Close
        )

        # info
        self.infoAction = create_action(
            self, self.tr("&Info"),
            slot=self.show_info, shortcut=QKeySequence.HelpContents
        )

    def init_toolbars(self):
        toolbar = QToolBar()
        self.addToolBar(toolbar)
        toolbar.addAction(self.saveOrderAction)

    def init_dockwidgets(self):
        # CatalogDockWidget
        self.catalogWidget = CatalogWidget()
        self.catalogDockWidget = QDockWidget(self.tr("Catalog"), self)
        self.catalogDockWidget.setWidget(self.catalogWidget)
        self.catalogDockWidget.setObjectName("catalogDockWidget")

        # ProjectExplorerDockWidget
        self.projectexplorer = ProjectExplorer(self)
        self.projectsDockWidget = QDockWidget(self.tr("Projects"), self)
        self.projectsDockWidget.setWidget(self.projectexplorer)
        self.projectsDockWidget.setObjectName("projectsDockWidget")

        self.addDockWidget(Qt.LeftDockWidgetArea, self.projectsDockWidget)
        self.addDockWidget(Qt.BottomDockWidgetArea, self.catalogDockWidget)

    def category_selected(self, category_id):
        self.articlesWidget.filter_category(category_id)

    def category_changed(self, category_id):
        self.articlesWidget.refresh()

    def cleanup_database(self):
        empty_articles = session.query(db.Article).filter(db.Article.name.is_(None)).all()
        for article in empty_articles:
            session.delete(article)

    def closeEvent(self, event):
        self.cleanup_database()
        session.commit()
        session.flush()
        self.save_settings()
        self.catalogWidget.save_settings()
        self.projectexplorer.save_settings()

        for sw in self.mdi.subWindowList():
            w = sw.widget()
            try:
                w.save_settings()
            except AttributeError:
                pass

        QMainWindow.closeEvent(self, event)

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_MAINWINDOW)

        # geometry
        try:
            self.restoreGeometry(settings.value(S_WINDOW_GEOMETRY))
        except TypeError as e:
            logger.debug(e)

        # stock only
        try:
            self.stock_only = settings.value(S_STOCK_ONLY, type=bool)
        except TypeError as e:
            logger.debug(e)

        # opened windows
        try:
            opened_windows = settings.value(S_OPENED_WINDOWS, defaultValue=[])
            for label, data, geometry in opened_windows:
                if label == 'project':
                    project = session.query(db.Project).get(data)
                    widget = self.open_project(project)
                elif label == 'order':
                    order = session.query(db.Order).get(data)
                    widget = self.open_order(order)
                elif label == "materiallist":
                    materiallist = session.query(db.MaterialList).get(data)
                    widget = self.open_materiallist(materiallist)
                else:
                    continue
                if widget is not None:
                    widget.restoreGeometry(geometry)

        except (TypeError, ValueError) as e:
            logger.warning(e)

        # window state
        try:
            self.restoreState(settings.value(S_WINDOW_STATE))
        except TypeError as e:
            logger.debug(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_MAINWINDOW)

        # window geometry
        settings.setValue(S_WINDOW_GEOMETRY, self.saveGeometry())

        # stock only
        settings.setValue(S_STOCK_ONLY, self.stock_only)

        # opened windows
        opened_windows = []
        for sw in self.mdi.subWindowList():
            widget = sw.widget()

            if type(widget) == ProjectWidget:
                label = 'project'
                data = widget.project.id

            elif type(widget) == OrderWidget:
                label = 'order'
                data = widget.order.id

            elif type(widget) == MaterialWidget:
                label = 'materiallist'
                data = widget. materiallist.id

            geometry = sw.saveGeometry()
            opened_windows.append((label, data, geometry))

        settings.setValue(S_OPENED_WINDOWS, opened_windows)

        # window state
        settings.setValue(S_WINDOW_STATE, self.saveState())

    def find_orderWidget(self, order):
        open_orders = [
            sw.widget() for sw in self.mdi.subWindowList()
            if isinstance(sw.widget(), OrderWidget)
        ]
        for widget in open_orders:
            if widget.order.id == order.id:
                return widget


    def handle_export_to_docx(self):
        subwindow = self.mdi.currentSubWindow()
        if subwindow is None or not isinstance(subwindow.widget(), OrderWidget):
            return
        order = subwindow.widget().order
        if order is None:
            return
        filename = QFileDialog.getSaveFileName(
            self, QApplication.applicationName(),
            filter=self.tr('Word Documents (*.docx);;All Files (*.*)'),
        )[0]
        if filename in (None, ''):
            return
        export_order_to_docx(order, output=filename)

    def lockstate_changed(self, order: db.Order, locked: bool):
        for subwindow in self.mdi.subWindowList():
            assert isinstance(subwindow.widget(), OrderWidget)
            orderwidget = subwindow.widget()
            if orderwidget.order.id == order.id:
                orderwidget.session.refresh(orderwidget.order)
                orderwidget.set_lockstate(locked)

    def new_order(self):
        orderWidget = OrderWidget(self)
        # orderWidget.lookup_article.connect(self.catalogWidget.find_article)
        self.mdi.addSubWindow(orderWidget)
        orderWidget.show()

    def new_project(self):
        project = db.Project()
        session.add(project)
        session.commit()
        self.projectexplorer.refresh()
        self.open_project(project)

    def open_order(self, order=None):
        # if no order is given open dialog
        if order is None or not isinstance(order, db.Order):
            dlg = OrderDialog(self)
            if dlg.exec_() == OrderDialog.Rejected:
                return
            order = dlg.current_order()

        for subwindow in self.mdi.subWindowList():
            if isinstance(subwindow.widget(), OrderWidget):
                existing_order = subwindow.widget().order
                if order.id == existing_order.id:
                    self.mdi.setActiveSubWindow(subwindow)
                    return

        if order is None:
            return

        orderWidget = OrderWidget(self, order.id)

        sw = self.mdi.addSubWindow(orderWidget)
        orderWidget.show()
        # orderWidget.showMaximized()
        return sw

    def open_article(self, article):
        dlg = ArticleDialog(article, self)
        dlg.exec_()

    def open_project(self, project):
        # look if any subwindow with current project already open
        subwindows = (sw for sw in self.mdi.subWindowList()
                      if isinstance(sw.widget(), ProjectWidget))

        for sw in subwindows:
            if sw.widget().project.id == project.id:
                self.mdi.setActiveSubWindow(sw)
                return

        projectWidget = ProjectWidget(project, self)
        sw = self.mdi.addSubWindow(projectWidget)
        projectWidget.show()
        # projectWidget.showMaximized()
        return sw

    def open_materiallist(self, materiallist):
        subwindows = (sw for sw in self.mdi.subWindowList()
                      if isinstance(sw.widget(), MaterialWidget))

        for sw in subwindows:
            if sw.widget().materiallist.id == materiallist.id:
                self.mdi.setActiveSubWindow(sw)
                return

        materialWidget = MaterialWidget(materiallist, self)
        sw = self.mdi.addSubWindow(materialWidget)
        materialWidget.show()
        # materialWidget.showMaximized()
        return sw

    def order_online(self):
        sw = self.mdi.currentSubWindow()
        if not isinstance(sw.widget(), OrderWidget):
            return
        order = sw.widget().order
        distributor = order.distributor
        plugin = distributor.plugin
        if plugin in (None, ''):
            return
        plugin.order_online(order)

    def refresh_menu(self, subwindow):
        if subwindow is None:
            self.orderOnlineAction.setVisible(False)
        else:
            if isinstance(subwindow.widget(), OrderWidget):
                self.orderOnlineAction.setVisible(True)
                distributor = subwindow.widget().order.distributor
                self.orderOnlineAction.setEnabled(
                    distributor is not None and
                    distributor.plugin is not None and
                    hasattr(distributor.plugin, 'order_online')
                )
            else:
                self.orderOnlineAction.setVisible(False)

    def save(self):
        session.commit()
        self.projectexplorer.treeview.viewport().update()

    def show_distributors(self):
        dlg = DistributorsDialog(self)
        if dlg.exec_() == QDialog.Accepted:
            session.commit()
        else:
            session.rollback()

    def show_info(self):
        QMessageBox.about(
            self, QApplication.applicationName(),
            "%s Version %s\n"
            "Copyright (c) by %s" %
            (
                QCoreApplication.applicationName(),
                QCoreApplication.applicationVersion(),
                QCoreApplication.organizationName(),
            )
        )

    def show_projectdlg(self):
        dlg = ProjectDialog(self)
        dlg.exec_()

    def show_articlelist(self):
        # look if any subwindow with a stocklist already open
        subwindows = (sw for sw in self.mdi.subWindowList()
                      if isinstance(sw.widget(), ArticlelistWidget))

        try:
            sw = next(subwindows)
            self.mdi.setActiveSubWindow(sw)

        except StopIteration:
            articlelistWidget = ArticlelistWidget(self)
            articlelistWidget.stock_only = self.stockOnlyAction.isChecked()
            self.catalogWidget.category_changed.connect(
                articlelistWidget.filter_category)
            self.mdi.addSubWindow(articlelistWidget)
            articlelistWidget.showMaximized()
            articlelistWidget.refresh()

    def show_user_information(self):
        dlg = UserDialog(self)
        dlg.show()

    def stock_only_changed(self):
        # set catalog to stock only
        self.catalogWidget.stock_only = self.stockOnlyAction.isChecked()
        self.catalogWidget.refresh()

        # set article list to stock only
        subwindows = (sw for sw in self.mdi.subWindowList()
                      if isinstance(sw.widget(), ArticlelistWidget))

        for sw in subwindows:
            w = sw.widget()
            w.stock_only = self.stockOnlyAction.isChecked()

    def update_window_menu(self):
        self.windowMenu.clear()
        self.windowMenu.addActions([
            self.windowNextAction, self.windowPrevAction,
            self.windowCascadeAction, self.windowTileAction,
            self.windowRestoreAction, self.windowMinimizeAction,
            self.windowCloseAction
        ])
        self.windowMenu.addSeparator()
        windows = self.mdi.subWindowList()
        menu = self.windowMenu
        for i, window in enumerate(windows, 1):
            title = window.windowTitle()
            if i == 10:
                self.windowMenu.addSeparator()
                menu = self.windowMenu.addMenu(self.tr("&More"))
            accel = ""
            if i < 10:
                accel = "&%d " % i
            elif i < 36:
                accel = "&%c " % chr(i + ord("@") - 9)
            action = menu.addAction("%s%s" % (accel, title))
            action.setCheckable(True)
            action.triggered.connect(
                functools.partial(self.mdi.setActiveSubWindow, window))
            if window == self.mdi.activeSubWindow():
                action.setChecked(True)

    def windowRestoreAll(self):
        for subwindow in self.mdi.subWindowList():
            subwindow.showNormal()

    def windowMinimizeAll(self):
        for subwindow in self.mdi.subWindowList():
            subwindow.showMinimized()

    # stock only property
    @property
    def stock_only(self):
        return self.stockOnlyAction.isChecked()

    @stock_only.setter
    def stock_only(self, value):
        self.stockOnlyAction.setChecked(value)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main = MainWindow()
    main.show()
    app.exec_()
