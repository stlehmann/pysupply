#!/usr/bin/env python
"""
documentexplorer.py

copyright (c) 2015 by Stefan Lehmann
licensed under the MIT license

"""

import logging
import os
import sys
import subprocess
import shutil

from PyQt5.QtCore import QPoint, Qt, QSettings
from PyQt5.QtWidgets import QApplication, QWidget, QFileSystemModel, \
    QTreeView, QVBoxLayout, QAbstractItemView, QFileDialog, QMessageBox, \
    QMenu

from pysupply.gui.helper import create_action


S_GRP_DOCUMENTEXPLORER = "pysupply/gui/documentexplorer"
S_HEADER_STATE = "header_state"

logger = logging.getLogger("pysupply.gui.documentexplorer")


class DocumentExplorerWidget(QWidget):

    def __init__(self, root: str, parent=None):
        super().__init__(parent)
        self.root = root

        # filesystem model
        self.model = QFileSystemModel()
        self.model.setRootPath(self.root)
        self.model.setReadOnly(False)

        # treeview
        self.treeview = QTreeView()
        self.treeview.setModel(self.model)
        self.treeview.setRootIndex(self.model.index(self.root))
        self.treeview.setEditTriggers(QAbstractItemView.EditKeyPressed)
        self.treeview.setDragEnabled(True)
        self.treeview.setDragDropMode(QAbstractItemView.InternalMove)
        self.treeview.doubleClicked.connect(self.open)
        self.treeview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeview.customContextMenuRequested.connect(self.show_contextmenu)
        self.treeview.setSortingEnabled(True)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.treeview)
        self.setLayout(layout)

        self._init_actions()

        # load settings
        self.load_settings()

    def _init_actions(self):
        self.addAction = create_action(
            self,
            self.tr("Add..."),
            image=":icons/add.png",
            slot=self.add
        )
        self.deleteAction = create_action(
            self,
            self.tr("Delete..."),
            slot=self.delete,
            image=":icons/remove.png"
        )
        self.openFileAction = create_action(
            self,
            self.tr("Open"),
            slot=self.open_file,
            image=":icons/open.png"
        )
        self.openDirAction = create_action(
            self,
            self.tr("Open directory"),
            image=":icons/open_directory.png",
            slot=self.open_dir
        )
        self.newDirAction = create_action(
            self, self.tr("New directory"),
            slot=self.new_dir
        )

    def _current_dir(self):
        index = self.treeview.currentIndex()
        if index.isValid():
            d = self.model.filePath(index)
            if not os.path.isdir(d):
                d = os.path.dirname(d)
        else:
            d = self.root
        return d

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_DOCUMENTEXPLORER)

        # header state
        try:
            self.treeview.header().restoreState(
                settings.value(S_HEADER_STATE)
            )
        except TypeError as e:
            self.resize_columns_to_contents()
            logger.warning(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_DOCUMENTEXPLORER)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.treeview.header().saveState()
        )

    def open(self, index):
        filename = self.model.filePath(index)
        if os.path.isdir(filename):
            return
        try:
            os.startfile(filename)
        except AttributeError:
            subprocess.call(["open", filename])

    def open_file(self):
        for index in self.treeview.selectedIndexes():
            if index.isValid():
                self.open(index)

    def open_dir(self):
        path = self.root
        if sys.platform == "darwin":
            subprocess.call(['open', '--', path])
        elif sys.platform == "win32":
            subprocess.call(['explorer', path])
        elif sys.platform == "linux2":
            subprocess.call(['gnome-open', '--', path])

    def add(self):
        filenames, filter = QFileDialog.getOpenFileNames(
            self, self.tr("Add documents to order"),
            filter=self.tr(
                "Document files (*.pdf *.doc *.txt);; All files (*.*)")
        )

        for filename in filenames:
            shutil.copy(filename, self._current_dir())

    def delete(self):
        def remove(index):
            f = self.model.filePath(index)
            if os.path.isdir(f):
                self.model.rmdir(index)
            else:
                self.model.remove(index)

        removeall = False
        last_row = None

        for index in self.treeview.selectedIndexes():

            # get filename of index, only one index per row
            row = index.row()
            if row == last_row or not index.isValid():
                continue
            last_row = row

            if removeall:
                remove(index)
                continue

            filename = self.model.filePath(index)
            res = QMessageBox.question(
                self, QApplication.applicationName(),
                self.tr("Do you want to delete the document '%s'?") % filename,
                buttons=QMessageBox.Yes | QMessageBox.YesAll | QMessageBox.No |
                        QMessageBox.NoAll
            )

            if res == QMessageBox.Yes:
                remove(index)
            elif res == QMessageBox.YesAll:
                remove(index)
                removeall = True
            elif res == QMessageBox.No:
                continue
            elif res == QMessageBox.NoAll:
                return

    def new_dir(self):
        os.mkdir(os.path.join(self._current_dir(), self.tr("new directory")))

    def show_contextmenu(self, pos: QPoint):
        menu = QMenu(self)
        index = self.treeview.currentIndex()

        menu.addAction(self.newDirAction)

        if index.isValid():
            menu.addAction(self.openFileAction)
            menu.addSeparator()

        menu.addAction(self.addAction)

        if index.isValid():
            menu.setDefaultAction(self.openFileAction)
            menu.addAction(self.deleteAction)
        else:
            menu.setDefaultAction(self.addAction)

        menu.addSeparator()
        menu.addAction(self.openDirAction)

        globalpos = self.treeview.viewport().mapToGlobal(pos)
        menu.popup(globalpos)

    def resize_columns_to_contents(self):
        for i in range(self.model.columnCount()):
            self.treeview.resizeColumnToContents(i)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = DocumentExplorerWidget(".")
    w.show()
    app.exec_()
