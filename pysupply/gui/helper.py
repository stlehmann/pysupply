"""
helper.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
from collections import namedtuple

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction


CURRENCY_MIN = 0.
CURRENCY_MAX = 1E12
TAX_FACTOR = 1.19


Column = namedtuple("Column", "name label")


def create_column_actions(parent, columns, slot):
    """
    Create actions for hiding columns.

    """
    def iter_columns():
        for c in columns:
            action = create_action(parent, c.label, checkable=True,
                                   slot=slot)
            action.setChecked(True)
            action.setData(c)
            yield action
    return list(iter_columns())


def create_action(parent, title, shortcut=None, image=None, checkable=False,
                  tip=None, slot=None):
    """
    Conveniently create an Action.

    """
    action = QAction(title, parent)
    action.setCheckable(checkable)
    if shortcut:
        action.setShortcut(shortcut)
    if slot:
        action.triggered.connect(slot)
    if tip:
        action.setToolTip(tip)
        action.setStatusTip(tip)
    if image:
        action.setIcon(QIcon(image))
    return action


def sqlclass_keys(db_class):
    """
    Return all keys of a sqlalchemy class object as list.

    """
    def iter_keys():
        from sqlalchemy import inspect
        mapper = inspect(db_class)
        for column in mapper.attrs:
            yield column.key
    return list(iter_keys())


def debug_trace():
    """
    Set a tracepoint in the Python debugger that works with Qt.

    """
    from PyQt5.QtCore import pyqtRemoveInputHook
    from pdb import set_trace
    pyqtRemoveInputHook()
    set_trace()


def debug_stop():
    from PyQt5.QtCore import pyqtRestoreInputHook
    pyqtRestoreInputHook()
