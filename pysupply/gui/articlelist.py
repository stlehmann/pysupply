"""
articlelist.py

copyright (c) 2015 by Stefan Lehmann
licensed under the MIT license

"""
import os
import glob
import shutil
from functools import partial

from PyQt5.QtGui import QBrush
from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt, QSettings
from PyQt5.QtWidgets import QWidget, QTableView, QVBoxLayout, QMenu, QDialog, \
    QMessageBox, QApplication
from sqlalchemy.orm.exc import NoResultFound

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.helper import create_action, create_column_actions, Column
from pysupply.gui.documents import get_articledir

S_GRP_ARTICLELIST = "pysupply/gui/articlelist"
S_HEADER_STATE = "header_state"
S_HIDDEN_COLUMNS = "hidden_columns"


class ArticlesTableModel(QAbstractTableModel):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.columns = [
            Column("id", self.tr("ID")),
            Column("name", self.tr("name")),
            Column("category", self.tr("category")),
            Column("manufacturer", self.tr("manufacturer")),
            Column("article_nr", self.tr("article nr.")),
            Column("notes", self.tr("notes")),
            Column("stock_quantity", self.tr("stock quantity")),
            Column("stock_minimum", self.tr("stock minimum"))
        ]

        self.articles = [a for a in session.query(db.Article)]
        self.articles_unsorted = self.articles[:]

        # sorting
        self.sort_column = None
        self.sort_order = Qt.AscendingOrder

        # stock only
        self.stock_only = False

    def refresh(self, f_filter=None):
        # category filter
        if f_filter is None:
            articles = [a for a in session.query(db.Article)]
        else:
            articles = [a for a in session.query(db.Article)
                        if f_filter(a)]

        # stock only?
        if self.stock_only:
            self.articles = [a for a in articles if a.stock is not None]
        else:
            self.articles = articles

        self.articles_unsorted = self.articles[:]
        if self.sort_column is not None:
            self.sort(self.sort_column, self.sort_order)

    def rowCount(self, parent=QModelIndex()):
        return len(self.articles)

    def columnCount(self, parent=QModelIndex()):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        article = self.articles[index.row()]
        attr = self.columns[index.column()].name

        if role == Qt.DisplayRole:

            if attr == "category":
                if article.category is not None:
                    return article.category.name

            elif attr == "stock_quantity":
                if article.stock is not None:
                    return article.stock.quantity

            elif attr == "stock_minimum":
                if article.stock is not None:
                    return article.stock.minimum

            else:
                return getattr(article, attr)

        elif role == Qt.TextAlignmentRole:

            if attr in ("stock_quantity", "stock_minimum"):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.ForegroundRole:

            # highlight stock items with 0 or below minimum
            if article.stock is not None and article.stock.minimum is not None:

                if (article.stock.quantity < article.stock.minimum
                        or article.stock.quantity == 0):
                    return QBrush(Qt.red)

        elif role == Qt.UserRole:
            return article

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            else:
                return section + 1

    def sort(self, column, order=Qt.AscendingOrder):

        # save ordering info
        self.sort_column = column
        self.sort_order = order

        self.beginResetModel()
        if column == -1:
            self.articles = self.articles_unsorted
        else:
            attr = self.columns[column].name
            if attr == "category":
                key = lambda x: x.category.name.lower()

            elif attr == "stock_quantity":
                key = lambda x: 0 if x.stock is None else x.stock.quantity or 0

            elif attr == "stock_minimum":
                key = lambda x: x.stock.minimum or 0

            else:
                key = lambda x: getattr(x, attr)

            self.articles = sorted(
                self.articles_unsorted, key=key,
                reverse=order == Qt.DescendingOrder
            )
        self.endResetModel()


class ArticlelistWidget(QWidget):

    """
    Widget for listing all stock items.

    """

    def __init__(self, mainwindow, parent=None):
        super().__init__(parent)

        self.mainwindow = mainwindow
        self.model = ArticlesTableModel()

        self._init_actions()

        self.tableview = QTableView()
        self.tableview.setModel(self.model)
        self.tableview.setSortingEnabled(True)
        self.tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableview.customContextMenuRequested.connect(
            self.show_tableview_contextmenu)
        self.tableview.horizontalHeader().setContextMenuPolicy(
            Qt.ActionsContextMenu)
        self.tableview.doubleClicked.connect(self.open_article)

        # add all columns as actions to header
        for action in self.column_actions:
            self.tableview.horizontalHeader().addAction(action)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.tableview)
        self.setLayout(layout)

        self.setWindowTitle(self.tr("Articlelist"))

        # settings
        self.load_settings()

    def _init_actions(self):
        # open article
        self.openArticleAction = create_action(
            self, self.tr("Open..."),
            slot=self.open_article
        )
        # add article
        self.addArticleAction = create_action(
            self, self.tr("Add article..."), image=":icons/add.png",
            slot=self.add_article
        )
        # remove article
        self.removeArticleAction = create_action(
            self, self.tr("Remove article"), image=":icons/remove.png",
            slot=self.remove_article
        )
        # find article
        self.findArticleAction = create_action(
            self, self.tr("Navigate"),
            image=":icons/find.png", slot=self.navigate
        )

        self.column_actions = create_column_actions(
            self, self.model.columns, self.toggle_column)

    def closeEvent(self, event):
        self.save_settings()
        event.accept()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLELIST)

        # header state
        try:
            self.tableview.horizontalHeader().restoreState(
                settings.value(S_HEADER_STATE))
        except TypeError:
            self.tableview.resizeColumnsToContents()

        # hidden columns
        hidden_columns = settings.value(S_HIDDEN_COLUMNS)
        if hidden_columns is not None:
            for i, c in enumerate(self.model.columns):
                if c.name in hidden_columns:
                    self.column_actions[i].setChecked(False)
                    self.tableview.setColumnHidden(i, True)

        settings.endGroup()

    def filter_category(self, category: db.Category):
        def iter_categories(cat):
            yield cat.id
            child_categories = session.query(db.Category).filter(
                db.Category.parent_id == cat.id).all()

            for child_category in child_categories:
                yield from iter_categories(child_category)

        def filter_fct(article):
            return article.category_id in filter_categories

        self.model.beginResetModel()
        if category is not None:
            filter_categories = list(iter_categories(category))
            self.model.refresh(filter_fct)
        else:
            self.model.refresh()

        self.model.endResetModel()

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLELIST)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.tableview.horizontalHeader().saveState())

        # hidden columns
        settings.setValue(
            S_HIDDEN_COLUMNS,
            [c.data().name for c in self.column_actions if not c.isChecked()]
        )

        settings.endGroup()

    def show_tableview_contextmenu(self, pos):
        # check for valid index
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        # add actions
        menu = QMenu(self)
        menu.addAction(self.openArticleAction)
        menu.addAction(self.addArticleAction)
        menu.addAction(self.removeArticleAction)
        menu.addSeparator()
        menu.addAction(self.findArticleAction)
        menu.setDefaultAction(self.openArticleAction)

        # show menu
        globalpos = self.tableview.viewport().mapToGlobal(pos)
        menu.popup(globalpos)

    def open_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        article = self.model.data(index, Qt.UserRole)
        self.mainwindow.open_article(article)

    def add_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        node = self.model.data(index, Qt.UserRole)

        from pysupply.gui.article import ArticleDialog
        article = db.Article(category=node.category)
        articleDlg = ArticleDialog(article, self)
        session.commit()

        if articleDlg.exec_() == QDialog.Accepted:
            session.add(article)
            session.commit()

            # refresh catalog
            self.model.beginResetModel()
            self.model.refresh()
            self.model.endResetModel()

            # documents dir
            nonedir = get_articledir("None")
            newdir = get_articledir(str(article.id))
            documents = glob.glob(os.path.join(nonedir, "*"))

            if len(documents) > 0:
                os.mkdir(newdir)

            for document in documents:
                head, tail = os.path.split(document)
                shutil.move(document, os.path.join(newdir, tail))

            if os.path.exists(nonedir):
                os.rmdir(nonedir)

    def remove_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        article = self.model.data(index, Qt.UserRole)
        res = QMessageBox.question(
            self,
            QApplication.applicationName(),
            self.tr("Do you want to delete the article") +
            " '%s'?" % article.name
        )
        if res == QMessageBox.Yes:
            self.model.beginRemoveRows(
                index.row(), index.row(), QModelIndex)
            session.delete(article)
            session.commit()
            self.model.endRemoveRows()

            # clean document dir
            dir = get_articledir(str(article.id))
            if os.path.exists(dir):
                shutil.rmtree(dir)

    def refresh(self):
        self.model.refresh()

    def navigate(self):
        # check for valid index
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        article = self.model.data(index, Qt.UserRole)
        self.mainwindow.catalogWidget.find_article(article)

    def toggle_column(self):
        for action in self.column_actions:
            column = action.data()
            index = self.model.columns.index(column)
            self.tableview.setColumnHidden(index, not action.isChecked())

    # stock only property
    @property
    def stock_only(self):
        return self.model.stock_only

    @stock_only.setter
    def stock_only(self, value):
        self.model.stock_only = value
