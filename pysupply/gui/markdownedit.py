"""
markdownedit.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
import markdown2
from PyQt5.QtCore import QEvent
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QWidget, QTextEdit, QVBoxLayout, \
    QPlainTextEdit


class MarkdownEdit(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self._currentWidget = None

        # editor
        self.editor = QPlainTextEdit()
        self.editor.setTabChangesFocus(False)
        self.editor.installEventFilter(self)

        # viewer
        self.viewer = QTextEdit()
        self.viewer.setReadOnly(True)
        self.viewer.installEventFilter(self)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.viewer)
        layout.addWidget(self.editor)
        self.setLayout(layout)

        self.installEventFilter(self)
        self.currentWidget = self.viewer
        self.refresh_viewer()

    def refresh_viewer(self):
        if self.currentWidget == self.viewer:
            markdown = self.editor.toPlainText()
            html = markdown2.markdown(markdown)
            self.viewer.setHtml(html)

    def eventFilter(self, obj, event):
        if obj in (self.viewer,):
            if event.type() == QEvent.FocusIn:
                pos = QCursor.pos()
                pos = self.editor.mapFromGlobal(pos)

                self.currentWidget = self.editor
                self.editor.setFocus()
                cursor = self.editor.cursorForPosition(pos)
                self.editor.setTextCursor(cursor)

        elif obj in (self.editor,):
            if event.type() == QEvent.FocusOut:
                self.currentWidget = self.viewer

        return False

    @property
    def currentWidget(self):
        return self._currentWidget

    @currentWidget.setter
    def currentWidget(self, val):
        self._currentWidget = val
        self.editor.setVisible(val == self.editor)
        self.viewer.setVisible(val == self.viewer)

        if val == self.viewer:
            self.refresh_viewer()


class MyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.markdownedit = MarkdownEdit()

        layout = QVBoxLayout()
        layout.addWidget(self.markdownedit)
        self.setLayout(layout)
