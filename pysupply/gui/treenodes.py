#!/usr/bin/env python
"""
treenodes.py

copyright (c) 2015 by Stefan Lehmann
licensed under the MIT license

"""
from PyQt5.QtCore import QAbstractItemModel, QModelIndex


class Node():
    def __init__(self, item, parent=None):
        self.item = item
        self.children = []
        self.parent = parent

    def __len__(self):
        return len(self.children)

    def child_at_row(self, row: int):
        return self.children[row]

    def row_of_child(self, child):
        if child in self.children:
            return self.children.index(child)

    def has_leafs(self):
        for child in self.children:
            if isinstance(child, Leaf):
                return True

    def add_child(self, child):
        child.parent = self
        self.children.append(child)
        # self.children.sort(key=sort_nodes)

    def remove_child(self, child):
        self.children.remove(child)


class Leaf:
    def __init__(self, item, parent=None):
        self.item = item
        self.parent = parent

    def __len__(self):
        return 0


class TreeNodeItemModel(QAbstractItemModel):
    def index(self, row: int, column: int, parent: QModelIndex):
        parent_node = self.node_from_index(parent)
        return self.createIndex(row, column, parent_node.child_at_row(row))

    def parent(self, index: QModelIndex):
        node = self.node_from_index(index)
        if node is None:
            return QModelIndex()
        parent = node.parent
        if parent is None:
            return QModelIndex()
        grandparent = parent.parent
        if grandparent is None:
            return QModelIndex()
        row = grandparent.row_of_child(parent)
        return self.createIndex(row, 0, parent)

    def rowCount(self, parent=QModelIndex()):
        node = self.node_from_index(parent)
        return len(node)

    def columnCount(self, parent=QModelIndex()):
        return len(self.columns)

    def node_from_index(self, index: QModelIndex):
        return index.internalPointer() if index.isValid() else self.root
