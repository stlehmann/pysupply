<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="de_DE" sourcelanguage="">
<context>
    <name>ArticleDialog</name>
    <message>
        <location filename="article.py" line="96"/>
        <source>name:</source>
        <translation type="obsolete">Name:</translation>
    </message>
    <message>
        <location filename="article.py" line="100"/>
        <source>category:</source>
        <translation type="obsolete">Kategorie:</translation>
    </message>
    <message>
        <location filename="article.py" line="106"/>
        <source>manufacturer:</source>
        <translation type="obsolete">Hersteller:</translation>
    </message>
    <message>
        <location filename="article.py" line="110"/>
        <source>notes:</source>
        <translation type="obsolete">Notizen:</translation>
    </message>
</context>
<context>
    <name>ArticleDistributorModel</name>
    <message>
        <location filename="articledistr.py" line="105"/>
        <source>distributor</source>
        <translation>Lieferant</translation>
    </message>
    <message>
        <location filename="articledistr.py" line="106"/>
        <source>name</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location filename="articledistr.py" line="107"/>
        <source>order nr.</source>
        <translation>Bestellnr.</translation>
    </message>
    <message>
        <location filename="articledistr.py" line="108"/>
        <source>packing unit</source>
        <translation>VPE</translation>
    </message>
    <message>
        <location filename="articledistr.py" line="109"/>
        <source>price</source>
        <translation>Preis</translation>
    </message>
</context>
<context>
    <name>ArticleDistributorsWidget</name>
    <message>
        <location filename="articledistr.py" line="194"/>
        <source>Add distributor</source>
        <translation>Lieferant hinzufügen</translation>
    </message>
    <message>
        <location filename="articledistr.py" line="200"/>
        <source>Remove distributor</source>
        <translation>Lieferant entfernen</translation>
    </message>
</context>
<context>
    <name>ArticleModel</name>
    <message>
        <location filename="article.py" line="46"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="article.py" line="47"/>
        <source>category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="articles.py" line="71"/>
        <source>articlenr.</source>
        <translation type="obsolete">Artikelnr.</translation>
    </message>
    <message>
        <location filename="article.py" line="48"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="article.py" line="50"/>
        <source>notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="article.py" line="49"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="article.py" line="51"/>
        <source>properties</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location filename="article.py" line="52"/>
        <source>stock quantity</source>
        <translation>Lagerbestand</translation>
    </message>
    <message>
        <location filename="article.py" line="53"/>
        <source>stock minimum</source>
        <translation>min. Lagerbestand</translation>
    </message>
</context>
<context>
    <name>ArticleWidget</name>
    <message>
        <location filename="article.py" line="160"/>
        <source>name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="article.py" line="164"/>
        <source>category:</source>
        <translation>Kategorie:</translation>
    </message>
    <message>
        <location filename="article.py" line="168"/>
        <source>manufacturer:</source>
        <translation>Hersteller:</translation>
    </message>
    <message>
        <location filename="article.py" line="112"/>
        <source>notes:</source>
        <translation type="obsolete">Notizen:</translation>
    </message>
    <message>
        <location filename="article.py" line="215"/>
        <source>documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="article.py" line="217"/>
        <source>notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="article.py" line="218"/>
        <source>distributors</source>
        <translation>Lieferanten</translation>
    </message>
    <message>
        <location filename="article.py" line="172"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="article.py" line="216"/>
        <source>properties</source>
        <translation>Eigenschaften</translation>
    </message>
    <message>
        <location filename="article.py" line="138"/>
        <source>in stock:</source>
        <translation type="obsolete">Lagerbestand:</translation>
    </message>
    <message>
        <location filename="article.py" line="177"/>
        <source>stock item</source>
        <translation>Lagerware</translation>
    </message>
    <message>
        <location filename="article.py" line="181"/>
        <source>quantity:</source>
        <translation>Menge:</translation>
    </message>
    <message>
        <location filename="article.py" line="187"/>
        <source>minimum quantity:</source>
        <translation>Minimum:</translation>
    </message>
    <message>
        <location filename="article.py" line="271"/>
        <source>Remove article {0} from stock?</source>
        <translation>Artikel {0} aus dem Lagerbestand entfernen?</translation>
    </message>
</context>
<context>
    <name>ArticlelistModel</name>
    <message>
        <location filename="articlelist.py" line="79"/>
        <source>name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="80"/>
        <source>category</source>
        <translation type="obsolete">Kategorie</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="81"/>
        <source>articlenr.</source>
        <translation type="obsolete">Artikelnr.</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="82"/>
        <source>manufacturer</source>
        <translation type="obsolete">Hersteller</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="83"/>
        <source>notes</source>
        <translation type="obsolete">Notizen</translation>
    </message>
</context>
<context>
    <name>ArticlelistWidget</name>
    <message>
        <location filename="articlelist.py" line="212"/>
        <source>&amp;edit article...</source>
        <translation type="obsolete">Artikel &amp;bearbeiten...</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="218"/>
        <source>&amp;add article...</source>
        <translation type="obsolete">Artikel &amp;hinzufügen...</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="224"/>
        <source>&amp;remove article</source>
        <translation type="obsolete">Artikel &amp;entfernen</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="350"/>
        <source>Do you want to delete the article</source>
        <translation>Möchten Sie den Artikel entfernen?</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="164"/>
        <source>Stocklist</source>
        <translation type="obsolete">Lagerbestand</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="197"/>
        <source>Open...</source>
        <translation>Öffnen...</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="190"/>
        <source>Articlelist</source>
        <translation>Artikelliste</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="212"/>
        <source>Navigate</source>
        <translation>Navigieren</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="202"/>
        <source>Add article...</source>
        <translation>Artikel hinzufügen...</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="207"/>
        <source>Remove article</source>
        <translation>Artikel entfernen</translation>
    </message>
</context>
<context>
    <name>ArticlesTableModel</name>
    <message>
        <location filename="articlelist.py" line="34"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="35"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="36"/>
        <source>category</source>
        <translation>Kategorie</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="37"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="38"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="39"/>
        <source>notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="40"/>
        <source>stock quantity</source>
        <translation>Lagerbestand</translation>
    </message>
    <message>
        <location filename="articlelist.py" line="41"/>
        <source>stock minimum</source>
        <translation>min. Lagerbestand</translation>
    </message>
</context>
<context>
    <name>ArticlesWidget</name>
    <message>
        <location filename="articles.py" line="202"/>
        <source>&amp;edit article...</source>
        <translation type="obsolete">Artikel &amp;bearbeiten...</translation>
    </message>
    <message>
        <location filename="articles.py" line="207"/>
        <source>&amp;add article...</source>
        <translation type="obsolete">Artikel &amp;hinzufügen...</translation>
    </message>
    <message>
        <location filename="articles.py" line="212"/>
        <source>&amp;remove article</source>
        <translation type="obsolete">Artikel &amp;entfernen</translation>
    </message>
    <message>
        <location filename="articles.py" line="262"/>
        <source>Do you want to delete the article</source>
        <translation type="obsolete">Möchten Sie den Artikel entfernen?</translation>
    </message>
</context>
<context>
    <name>CatalogModel</name>
    <message>
        <location filename="catalog.py" line="177"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="catalog.py" line="178"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="catalog.py" line="179"/>
        <source>part nr.</source>
        <translation>Teilenummer</translation>
    </message>
    <message>
        <location filename="catalog.py" line="180"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="catalog.py" line="181"/>
        <source>unit</source>
        <translation>VPE</translation>
    </message>
    <message>
        <location filename="catalog.py" line="182"/>
        <source>price</source>
        <translation>Preis</translation>
    </message>
    <message>
        <location filename="catalog.py" line="183"/>
        <source>stock</source>
        <translation>Lagerbestand</translation>
    </message>
    <message>
        <location filename="catalog.py" line="223"/>
        <source>Catalog</source>
        <translation>Katalog</translation>
    </message>
</context>
<context>
    <name>CatalogWidget</name>
    <message>
        <location filename="catalog.py" line="611"/>
        <source>Add category...</source>
        <translation>Kategorie hinzufügen...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="617"/>
        <source>Remove category</source>
        <translation>Kategorie entfernen</translation>
    </message>
    <message>
        <location filename="catalog.py" line="623"/>
        <source>Add article...</source>
        <translation>Artikel hinzufügen...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="635"/>
        <source>Remove article</source>
        <translation>Artikel entfernen</translation>
    </message>
    <message>
        <location filename="catalog.py" line="886"/>
        <source>Category name:</source>
        <translation>Kategoriename:</translation>
    </message>
    <message>
        <location filename="catalog.py" line="967"/>
        <source>Do you want to delete the article</source>
        <translation>Möchten Sie den Artikel entfernen?</translation>
    </message>
    <message>
        <location filename="catalog.py" line="953"/>
        <source>Do you really want to delete the category &apos;%s&apos;?</source>
        <translation>Möchten Sie die Kategorie &apos;%s&apos; wirklich löschen?</translation>
    </message>
    <message>
        <location filename="catalog.py" line="641"/>
        <source>Edit article...</source>
        <translation>Artikel bearbeiten...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="570"/>
        <source>Search:</source>
        <translation>Suche:</translation>
    </message>
    <message>
        <location filename="catalog.py" line="647"/>
        <source>Remove from stock...</source>
        <translation>Aus dem Lagerbestand entfernen...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="665"/>
        <source>show stock only</source>
        <translation>Nur Lagerbestand anzeigen</translation>
    </message>
    <message>
        <location filename="catalog.py" line="1018"/>
        <source>Remove article &apos;{}&apos; from stock?</source>
        <translation>Artikel &apos;{}&apos; aus dem Lagerbestand entfernen?</translation>
    </message>
    <message>
        <location filename="catalog.py" line="675"/>
        <source>open in webbrowser</source>
        <translation>In Webbrowser öffnen</translation>
    </message>
    <message>
        <location filename="catalog.py" line="629"/>
        <source>Add article from URL...</source>
        <translation>Artikel von URL erzeugen...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="813"/>
        <source>Paste URL</source>
        <translation>URL einfügen</translation>
    </message>
    <message>
        <location filename="catalog.py" line="821"/>
        <source>Invalid url</source>
        <translation>Ungültige URL</translation>
    </message>
    <message>
        <location filename="catalog.py" line="821"/>
        <source>Please insert a valid url.</source>
        <translation>Bitte geben Sie eine gültige URL ein.</translation>
    </message>
    <message>
        <location filename="catalog.py" line="844"/>
        <source>Error importing article from url</source>
        <translation>Fehler beim Importieren des Artikels von der angegebenen URL</translation>
    </message>
    <message>
        <location filename="catalog.py" line="844"/>
        <source>Could not import article from the specified url.</source>
        <translation>Der Artikel konnte von der angegebenen URL nicht importiert werden.</translation>
    </message>
    <message>
        <location filename="catalog.py" line="653"/>
        <source>Change stock...</source>
        <translation>Bestand verändern...</translation>
    </message>
    <message>
        <location filename="catalog.py" line="659"/>
        <source>Show stock changes...</source>
        <translation>Bestandsänderungen zeigen...</translation>
    </message>
</context>
<context>
    <name>CategoriesModel</name>
    <message>
        <location filename="categories.py" line="70"/>
        <source>category</source>
        <translation type="obsolete">Kategorie</translation>
    </message>
    <message>
        <location filename="categories.py" line="138"/>
        <source>categories</source>
        <translation type="obsolete">Kategorien</translation>
    </message>
</context>
<context>
    <name>CategoriesWidget</name>
    <message>
        <location filename="categories.py" line="268"/>
        <source>Add category</source>
        <translation type="obsolete">Kategorie hinzufügen</translation>
    </message>
    <message>
        <location filename="categories.py" line="275"/>
        <source>Remove category</source>
        <translation type="obsolete">Kategorie entfernen</translation>
    </message>
    <message>
        <location filename="categories.py" line="293"/>
        <source>Category name:</source>
        <translation type="obsolete">Kategoriename:</translation>
    </message>
</context>
<context>
    <name>CategoryDialog</name>
    <message>
        <location filename="category.py" line="45"/>
        <source>name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="category.py" line="49"/>
        <source>parent category:</source>
        <translation>Übergeordnete Kategorie:</translation>
    </message>
</context>
<context>
    <name>DistributorWidget</name>
    <message>
        <location filename="distributor.py" line="80"/>
        <source>name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="distributor.py" line="96"/>
        <source>website:</source>
        <translation>Website:</translation>
    </message>
    <message>
        <location filename="distributor.py" line="101"/>
        <source>username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location filename="distributor.py" line="106"/>
        <source>password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="distributor.py" line="85"/>
        <source>address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="distributor.py" line="90"/>
        <source>contact person</source>
        <translation>Kontaktperson</translation>
    </message>
</context>
<context>
    <name>DistributorsWidget</name>
    <message>
        <location filename="distributors.py" line="120"/>
        <source>add distributor...</source>
        <translation>Lieferant hinzufügen...</translation>
    </message>
    <message>
        <location filename="distributors.py" line="126"/>
        <source>edit distributor...</source>
        <translation>Lieferant bearbeiten...</translation>
    </message>
    <message>
        <location filename="distributors.py" line="132"/>
        <source>remove distributor</source>
        <translation>Lieferant entfernen</translation>
    </message>
    <message>
        <location filename="distributors.py" line="165"/>
        <source>Do you want to delete the distributor</source>
        <translation>Möchten Sie den Lieferanten entfernen?</translation>
    </message>
</context>
<context>
    <name>DocumentExplorerWidget</name>
    <message>
        <location filename="documentexplorer.py" line="64"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="70"/>
        <source>Delete...</source>
        <translation>Entfernen...</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="76"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="82"/>
        <source>Open directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="88"/>
        <source>New directory</source>
        <translation>Neues Verzeichnis</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="149"/>
        <source>Add documents to order</source>
        <translation>Dokumente hinzufügen</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="149"/>
        <source>Document files (*.pdf *.doc *.txt);; All files (*.*)</source>
        <translation>Dokumente (*.pdf *.doc *.txt);;Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="182"/>
        <source>Do you want to delete the document &apos;%s&apos;?</source>
        <translation>Möchten Sie das Dokument &apos;%s&apos; löschen?</translation>
    </message>
    <message>
        <location filename="documentexplorer.py" line="200"/>
        <source>new directory</source>
        <translation>Neues Verzeichnis</translation>
    </message>
</context>
<context>
    <name>DocumentsWidget</name>
    <message>
        <location filename="documents.py" line="67"/>
        <source>open directory</source>
        <translation type="obsolete">Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="documents.py" line="109"/>
        <source>Open directory</source>
        <translation>Verzeichnis öffnen</translation>
    </message>
    <message>
        <location filename="documents.py" line="75"/>
        <source>Delete</source>
        <translation type="obsolete">Löschen</translation>
    </message>
    <message>
        <location filename="documents.py" line="226"/>
        <source>Do you want to delete the document &apos;%s&apos;?</source>
        <translation>Möchten Sie das Dokument &apos;%s&apos; löschen?</translation>
    </message>
    <message>
        <location filename="documents.py" line="100"/>
        <source>Add document...</source>
        <translation type="obsolete">Dokument hinzufügen...</translation>
    </message>
    <message>
        <location filename="documents.py" line="162"/>
        <source>Add documents to order</source>
        <translation>Dokumente zu der Bestellung hinzufügen</translation>
    </message>
    <message>
        <location filename="documents.py" line="124"/>
        <source>Document files (*.pdf, *.doc, *.txt);; All files (*.*)</source>
        <translation type="obsolete">Dokumente (*.pdf, *.doc, *.txt);; Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="documents.py" line="112"/>
        <source>Delete document...</source>
        <translation type="obsolete">Dokument löschen...</translation>
    </message>
    <message>
        <location filename="documents.py" line="126"/>
        <source>Open</source>
        <translation>Öffnen</translation>
    </message>
    <message>
        <location filename="documents.py" line="162"/>
        <source>Document files (*.pdf *.doc *.txt);; All files (*.*)</source>
        <translation>Dokumente (*.pdf *.doc *.txt);;Alle Dateien (*.*)</translation>
    </message>
    <message>
        <location filename="documents.py" line="101"/>
        <source>Add...</source>
        <translation>Hinzufügen...</translation>
    </message>
    <message>
        <location filename="documents.py" line="117"/>
        <source>Delete...</source>
        <translation>Entfernen...</translation>
    </message>
    <message>
        <location filename="documents.py" line="134"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="documents.py" line="140"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="documents.py" line="175"/>
        <source>The file {} exists already. Overwrite?</source>
        <translation>Die Datei {} existiert bereits. Überschreiben?</translation>
    </message>
    <message>
        <location filename="documents.py" line="204"/>
        <source>Error deleting file</source>
        <translation>Fehler beim Löschen der Datei</translation>
    </message>
    <message>
        <location filename="documents.py" line="204"/>
        <source>Could not delete file &quot;{}&quot;. Permission error.</source>
        <translation>Konnte die Datei &quot;{}&quot; nicht löschen. Der Zugriff wurde verweigert.</translation>
    </message>
</context>
<context>
    <name>ItemsTableView</name>
    <message>
        <location filename="order.py" line="787"/>
        <source>Reset sorting</source>
        <translation>Sortierung zurücksetzen</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.py" line="105"/>
        <source>Articlelist</source>
        <translation type="obsolete">Artikelliste</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="112"/>
        <source>Categories</source>
        <translation type="obsolete">Kategorien</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="86"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="112"/>
        <source>Distributors</source>
        <translation type="obsolete">Lieferanten</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="65"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="108"/>
        <source>New order</source>
        <translation>Neue Bestellung</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="148"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="114"/>
        <source>Open order...</source>
        <translation>Bestellung öffnen...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="99"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="175"/>
        <source>&amp;Next</source>
        <translation>&amp;Nächstes</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="181"/>
        <source>&amp;Previous</source>
        <translation>&amp;Vorheriges</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="558"/>
        <source>&amp;More</source>
        <translation>&amp;Mehr</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="120"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="154"/>
        <source>Show distributors...</source>
        <translation>Lieferanten anzeigen...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="230"/>
        <source>Catalog</source>
        <translation>Katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="236"/>
        <source>Projects</source>
        <translation>Projekte</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="187"/>
        <source>Casca&amp;de</source>
        <translation>Ü&amp;bereinander</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="193"/>
        <source>&amp;Tile</source>
        <translation>&amp;Anordnen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="199"/>
        <source>&amp;Restore All</source>
        <translation>Alle &amp;wiederherstellen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="205"/>
        <source>&amp;Iconize All</source>
        <translation>Alle &amp;minimieren</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="211"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="166"/>
        <source>Save changes of order &apos;%s&apos;?</source>
        <translation type="obsolete">Änderungen an der Bestellung &apos;%s&apos; speichern?</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="217"/>
        <source>&amp;Info</source>
        <translation>&amp;Info</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="126"/>
        <source>&amp;New project</source>
        <translation>&amp;Neues Projekt</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="130"/>
        <source>Show articlelist</source>
        <translation type="obsolete">Artikelliste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="132"/>
        <source>Open project...</source>
        <translation>Projekt öffnen...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="159"/>
        <source>articlelist</source>
        <translation>Artikelliste</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="169"/>
        <source>stock only</source>
        <translation>Nur Lagerbestand</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="164"/>
        <source>order online</source>
        <translation>Im Onlineshop bestellen</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="138"/>
        <source>User Information...</source>
        <translation>Benutzerinformation</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="143"/>
        <source>Export order to docx...</source>
        <translation>Bestellung als DOCX exportieren...</translation>
    </message>
    <message>
        <location filename="mainwindow.py" line="366"/>
        <source>Word Documents (*.docx);;All Files (*.*)</source>
        <translation>Word Dokument (*.docx);;Alle Dateien (*.*)</translation>
    </message>
</context>
<context>
    <name>MaterialItemsDelegate</name>
    <message>
        <location filename="material.py" line="82"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="material.py" line="83"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>MaterialItemsModel</name>
    <message>
        <location filename="material.py" line="123"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="material.py" line="124"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="material.py" line="125"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="material.py" line="126"/>
        <source>quantity</source>
        <translation>Menge</translation>
    </message>
    <message>
        <location filename="material.py" line="127"/>
        <source>stock</source>
        <translation>Lager</translation>
    </message>
    <message>
        <location filename="material.py" line="128"/>
        <source>removed from stock</source>
        <translation>Entnommen</translation>
    </message>
    <message>
        <location filename="material.py" line="164"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="material.py" line="164"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="material.py" line="129"/>
        <source>ordered</source>
        <translation>Bestellt</translation>
    </message>
    <message>
        <location filename="material.py" line="130"/>
        <source>comment</source>
        <translation>Kommentar</translation>
    </message>
</context>
<context>
    <name>MaterialListWidget</name>
    <message>
        <location filename="material.py" line="167"/>
        <source>add to order</source>
        <translation type="obsolete">Zu Bestellun hinzufügen...</translation>
    </message>
    <message>
        <location filename="material.py" line="171"/>
        <source>remove</source>
        <translation type="obsolete">entfernen</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="108"/>
        <source>Open...</source>
        <translation>Öffnen...</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="113"/>
        <source>Add Materiallist</source>
        <translation>Materialliste hinzufügen</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="118"/>
        <source>Delete Materiallist</source>
        <translation>Materialliste löschen</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="147"/>
        <source>new materiallist</source>
        <translation>Neue Materialliste</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="171"/>
        <source>Delete materiallist &quot;{}&quot; with all items?</source>
        <translation>Materiallistes &quot;{}&quot; mit allen Positionen entfernen?</translation>
    </message>
</context>
<context>
    <name>MaterialModel</name>
    <message>
        <location filename="material.py" line="33"/>
        <source>name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="material.py" line="34"/>
        <source>manufacturer</source>
        <translation type="obsolete">Hersteller</translation>
    </message>
    <message>
        <location filename="material.py" line="35"/>
        <source>article nr.</source>
        <translation type="obsolete">Artikelnr.</translation>
    </message>
    <message>
        <location filename="material.py" line="36"/>
        <source>quantity</source>
        <translation type="obsolete">Menge</translation>
    </message>
    <message>
        <location filename="material.py" line="37"/>
        <source>stock</source>
        <translation type="obsolete">Lagerbestand</translation>
    </message>
</context>
<context>
    <name>MaterialWidget</name>
    <message>
        <location filename="material.py" line="356"/>
        <source>name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="material.py" line="361"/>
        <source>comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="material.py" line="420"/>
        <source>add to order</source>
        <translation>Zu Bestellung hinzufügen</translation>
    </message>
    <message>
        <location filename="material.py" line="424"/>
        <source>remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="material.py" line="406"/>
        <source>Materialliste {}</source>
        <translation>Materialliste {}</translation>
    </message>
    <message>
        <location filename="material.py" line="412"/>
        <source>find article</source>
        <translation>Artikel finden</translation>
    </message>
    <message>
        <location filename="material.py" line="416"/>
        <source>open article...</source>
        <translation>Artikel öffnen...</translation>
    </message>
    <message>
        <location filename="material.py" line="428"/>
        <source>remove from stock</source>
        <translation>Aus Bestand entnehmen</translation>
    </message>
    <message>
        <location filename="material.py" line="432"/>
        <source>add to stock</source>
        <translation>Zu Bestand hinzufügen</translation>
    </message>
    <message>
        <location filename="material.py" line="579"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="material.py" line="579"/>
        <source>Can not remove article &quot;{}&quot; from stock. Article is not listed in stock.</source>
        <translation>Kann den Artikel &quot;{}&quot; nicht aus dem Bestand entnehmen. Der Artikel ist nich im Bestand gelistet.</translation>
    </message>
    <message>
        <location filename="material.py" line="436"/>
        <source>open order</source>
        <translation>Bestellung öffnen</translation>
    </message>
    <message>
        <location filename="material.py" line="440"/>
        <source>set ordered flag</source>
        <translation>&quot;Bestellt&quot; Flag setzen</translation>
    </message>
    <message>
        <location filename="material.py" line="444"/>
        <source>reset ordered flag</source>
        <translation>&quot;Bestellt&quot;-Flag zurücksetzen</translation>
    </message>
</context>
<context>
    <name>MateriallistModel</name>
    <message>
        <location filename="materiallists.py" line="31"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="32"/>
        <source>comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="33"/>
        <source>items</source>
        <translation>Positionen</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="34"/>
        <source>removed from stock</source>
        <translation>entnommen</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="62"/>
        <source>yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="materiallists.py" line="62"/>
        <source>no</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>OpenOrderDialog</name>
    <message>
        <location filename="orderlist.py" line="316"/>
        <source>Select an order to open.</source>
        <translation type="obsolete">Wählen Sie eine Bestellung aus.</translation>
    </message>
</context>
<context>
    <name>OrderItemsModel</name>
    <message>
        <location filename="order.py" line="277"/>
        <source>article</source>
        <translation>Artikel</translation>
    </message>
    <message>
        <location filename="order.py" line="278"/>
        <source>manufacturer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="order.py" line="279"/>
        <source>manufacturer&apos;s nr.</source>
        <translation>Herst.Teilenr.</translation>
    </message>
    <message>
        <location filename="order.py" line="280"/>
        <source>distributor</source>
        <translation>Lieferant</translation>
    </message>
    <message>
        <location filename="order.py" line="281"/>
        <source>article nr.</source>
        <translation>Artikelnr.</translation>
    </message>
    <message>
        <location filename="order.py" line="276"/>
        <source>count</source>
        <translation>Anzahl</translation>
    </message>
    <message>
        <location filename="order.py" line="284"/>
        <source>ppu</source>
        <translation>Einzelpreis</translation>
    </message>
    <message>
        <location filename="order.py" line="286"/>
        <source>total</source>
        <translation>Gesamt</translation>
    </message>
    <message>
        <location filename="order.py" line="309"/>
        <source>total net:</source>
        <translation>Netto:</translation>
    </message>
    <message>
        <location filename="order.py" line="327"/>
        <source>total gross:</source>
        <translation>Brutto:</translation>
    </message>
    <message>
        <location filename="order.py" line="318"/>
        <source>shipping costs:</source>
        <translation>Versandkosten:</translation>
    </message>
    <message>
        <location filename="order.py" line="287"/>
        <source>comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="order.py" line="282"/>
        <source>received</source>
        <translation>Ware erhalten</translation>
    </message>
    <message>
        <location filename="order.py" line="283"/>
        <source>added to stock</source>
        <translation>zum Bestand hinzugefügt</translation>
    </message>
    <message>
        <location filename="order.py" line="285"/>
        <source>packing unit</source>
        <translation>VPE</translation>
    </message>
    <message>
        <location filename="order.py" line="555"/>
        <source>Order difference to stock only?</source>
        <translation>Nur die Differenzmenge zum Bestand bestellen?</translation>
    </message>
    <message>
        <location filename="order.py" line="575"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
</context>
<context>
    <name>OrderModel</name>
    <message>
        <location filename="order.py" line="688"/>
        <source>date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="order.py" line="689"/>
        <source>number</source>
        <translation>Nummer</translation>
    </message>
    <message>
        <location filename="order.py" line="690"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="order.py" line="691"/>
        <source>state</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="order.py" line="692"/>
        <source>distributor</source>
        <translation>Lieferant</translation>
    </message>
    <message>
        <location filename="order.py" line="693"/>
        <source>notes</source>
        <translation>Notizen</translation>
    </message>
    <message>
        <location filename="order.py" line="694"/>
        <source>shipping costs</source>
        <translation>Versandkosten</translation>
    </message>
    <message>
        <location filename="order.py" line="695"/>
        <source>project</source>
        <translation>Projekt</translation>
    </message>
</context>
<context>
    <name>OrderWidget</name>
    <message>
        <location filename="order.py" line="845"/>
        <source>new order</source>
        <translation>Neue Bestellung</translation>
    </message>
    <message>
        <location filename="order.py" line="856"/>
        <source>date:</source>
        <translation>Datum:</translation>
    </message>
    <message>
        <location filename="order.py" line="863"/>
        <source>order number:</source>
        <translation>Bestellnr.:</translation>
    </message>
    <message>
        <location filename="order.py" line="868"/>
        <source>name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="order.py" line="891"/>
        <source>distributor:</source>
        <translation>Lieferant:</translation>
    </message>
    <message>
        <location filename="order.py" line="334"/>
        <source>article count:</source>
        <translation type="obsolete">Anzahl:</translation>
    </message>
    <message>
        <location filename="order.py" line="339"/>
        <source>total net:</source>
        <translation type="obsolete">Netto:</translation>
    </message>
    <message>
        <location filename="order.py" line="344"/>
        <source>total gross:</source>
        <translation type="obsolete">Brutto:</translation>
    </message>
    <message>
        <location filename="order.py" line="936"/>
        <source>details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="order.py" line="967"/>
        <source>articlelist</source>
        <translation>Artikelliste</translation>
    </message>
    <message>
        <location filename="order.py" line="371"/>
        <source>summary</source>
        <translation type="obsolete">Zusammenfassung</translation>
    </message>
    <message>
        <location filename="order.py" line="995"/>
        <source>add article</source>
        <translation>Artikel hinzufügen</translation>
    </message>
    <message>
        <location filename="order.py" line="1001"/>
        <source>remove article</source>
        <translation>Artikel entfernen</translation>
    </message>
    <message>
        <location filename="order.py" line="617"/>
        <source>Save changes made to this order?</source>
        <translation type="obsolete">Änderungen an dieser Bestellung speichern?</translation>
    </message>
    <message>
        <location filename="order.py" line="513"/>
        <source>notes</source>
        <translation type="obsolete">Notizen</translation>
    </message>
    <message>
        <location filename="order.py" line="883"/>
        <source>project:</source>
        <translation>Projekt:</translation>
    </message>
    <message>
        <location filename="order.py" line="730"/>
        <source>Save changes of order &apos;%s&apos;?</source>
        <translation type="obsolete">Änderungen an der Bestellung &apos;%s&apos; speichern?</translation>
    </message>
    <message>
        <location filename="order.py" line="902"/>
        <source>notes:</source>
        <translation>Notizen:</translation>
    </message>
    <message>
        <location filename="order.py" line="955"/>
        <source>documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="order.py" line="1013"/>
        <source>Find article in catalog</source>
        <translation>Artikel im Katalog suchen</translation>
    </message>
    <message>
        <location filename="order.py" line="1025"/>
        <source>Move up</source>
        <translation>Nach oben</translation>
    </message>
    <message>
        <location filename="order.py" line="1031"/>
        <source>Move down</source>
        <translation>Nach unten</translation>
    </message>
    <message>
        <location filename="order.py" line="1037"/>
        <source>copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="order.py" line="875"/>
        <source>state:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <location filename="order.py" line="878"/>
        <source>draft</source>
        <translation>Entwurf</translation>
    </message>
    <message>
        <location filename="order.py" line="879"/>
        <source>ordered</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="order.py" line="880"/>
        <source>received</source>
        <translation>erhalten</translation>
    </message>
    <message>
        <location filename="order.py" line="1306"/>
        <source>Do you want to set the order date to {:%d.%m.%Y}?</source>
        <translation>Möchten Sie das Datum der Bestellung auf den {:%d.%m.%Y} ändern?</translation>
    </message>
    <message>
        <location filename="order.py" line="1287"/>
        <source>Do you want to remove all articles from stock?</source>
        <translation>Möchten Sie alle Artikel der Bestellung vom Lagerbestand entfernen?</translation>
    </message>
    <message>
        <location filename="order.py" line="1324"/>
        <source>Do you want to add all articles to stock?</source>
        <translation>Möchten Sie alle Artikel der Bestellung dem Lagerbestand hinzufügen?</translation>
    </message>
    <message>
        <location filename="order.py" line="1007"/>
        <source>open article...</source>
        <translation>Artikel öffnen...</translation>
    </message>
    <message>
        <location filename="order.py" line="1019"/>
        <source>Update from catalog</source>
        <translation>Aus dem Katalog aktualisieren</translation>
    </message>
</context>
<context>
    <name>OrderlistModel</name>
    <message>
        <location filename="orderlist.py" line="32"/>
        <source>date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="33"/>
        <source>order nr.</source>
        <translation>Bestellnr.</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="34"/>
        <source>name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="35"/>
        <source>state</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="36"/>
        <source>distributor</source>
        <translation>Lieferant</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="38"/>
        <source>gross</source>
        <translation>Brutto</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="37"/>
        <source>net</source>
        <translation>Netto</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="82"/>
        <source>draft</source>
        <translation>Entwurf</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="84"/>
        <source>ordered</source>
        <translation>bestellt</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="86"/>
        <source>received</source>
        <translation>erhalten</translation>
    </message>
</context>
<context>
    <name>OrderlistWidget</name>
    <message>
        <location filename="orderlist.py" line="202"/>
        <source>add order</source>
        <translation>Bestellung hinzufügen</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="208"/>
        <source>remove order</source>
        <translation>Bestellung entfernen</translation>
    </message>
    <message>
        <location filename="orderlist.py" line="266"/>
        <source>Do you want to delete the order %s and all of its items?</source>
        <translation>Möchten Sie die Bestellung %s und die enthaltenen Positionen entfernen?</translation>
    </message>
</context>
<context>
    <name>OrdersModel</name>
    <message>
        <location filename="orders.py" line="26"/>
        <source>order nr.</source>
        <translation type="obsolete">Bestellnr.</translation>
    </message>
    <message>
        <location filename="orders.py" line="27"/>
        <source>name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="orders.py" line="25"/>
        <source>date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <location filename="orders.py" line="29"/>
        <source>distributor</source>
        <translation type="obsolete">Lieferant</translation>
    </message>
    <message>
        <location filename="orders.py" line="30"/>
        <source>gross</source>
        <translation type="obsolete">Brutto</translation>
    </message>
    <message>
        <location filename="orders.py" line="28"/>
        <source>state</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <location filename="orders.py" line="73"/>
        <source>draft</source>
        <translation type="obsolete">Entwurf</translation>
    </message>
    <message>
        <location filename="orders.py" line="73"/>
        <source>ordered</source>
        <translation type="obsolete">Bestellung ausgelöst</translation>
    </message>
    <message>
        <location filename="orders.py" line="73"/>
        <source>received</source>
        <translation type="obsolete">Bestellung erhalten</translation>
    </message>
</context>
<context>
    <name>OrdersWidget</name>
    <message>
        <location filename="orders.py" line="169"/>
        <source>add order</source>
        <translation type="obsolete">Bestellung hinzufügen</translation>
    </message>
    <message>
        <location filename="orders.py" line="173"/>
        <source>remove order</source>
        <translation type="obsolete">Bestellung entfernen</translation>
    </message>
    <message>
        <location filename="orders.py" line="187"/>
        <source>Do you want to delete the order %s and all of its items?</source>
        <translation type="obsolete">Möchten Sie die Bestellung %s und die enthaltenen Positionen entfernen?</translation>
    </message>
</context>
<context>
    <name>ProjectDialog</name>
    <message>
        <location filename="project.py" line="71"/>
        <source>project name:</source>
        <translation type="obsolete">Projektname:</translation>
    </message>
    <message>
        <location filename="project.py" line="76"/>
        <source>project number:</source>
        <translation type="obsolete">Projektnummer:</translation>
    </message>
    <message>
        <location filename="project.py" line="81"/>
        <source>notes:</source>
        <translation type="obsolete">Notizen:</translation>
    </message>
    <message>
        <location filename="project.py" line="102"/>
        <source>Project %s</source>
        <translation type="obsolete">Projekt %s</translation>
    </message>
</context>
<context>
    <name>ProjectExplorer</name>
    <message>
        <location filename="projectexplorer.py" line="258"/>
        <source>add project...</source>
        <translation>Projekt hinzufügen...</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="262"/>
        <source>remove project</source>
        <translation>Projekt entfernen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="266"/>
        <source>edit project...</source>
        <translation>Projekt bearbeiten...</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="432"/>
        <source>open order</source>
        <translation type="obsolete">Bestellung öffnen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="371"/>
        <source>Do you really want to delete the project &apos;%s&apos;? (All contained orders will not be deleted.)</source>
        <translation>Möchten Sie das Projekt &apos;%s&apos; wirklich löschen? (Enthaltene Bestellungen werden nicht gelöscht)</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="436"/>
        <source>add order</source>
        <translation type="obsolete">Bestellung hinzufügen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="440"/>
        <source>add directory</source>
        <translation type="obsolete">Verzeichnis hinzufügen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="516"/>
        <source>new directory</source>
        <translation type="obsolete">Neues Verzeichnis</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="444"/>
        <source>remove directory</source>
        <translation type="obsolete">Verzeichnis entfernen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="448"/>
        <source>remove order</source>
        <translation type="obsolete">Bestellung entfernen</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="607"/>
        <source>Do you want to delete the order &apos;%s&apos;?</source>
        <translation type="obsolete">Möchten Sie die Bestellung &apos;%s&apos; wirklich löschen?</translation>
    </message>
    <message>
        <location filename="projects.py" line="531"/>
        <source>The project contains orders. Please delte the orders or move them to another project.</source>
        <translation type="obsolete">Das Projekt enthält Bestellungen. Bitte löschen Sie die Bestellungen oder verschieben Sie sie in ein anderes Projekt.</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="477"/>
        <source>lock order</source>
        <translation type="obsolete">Bestellung sperren</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="477"/>
        <source>unlock order</source>
        <translation type="obsolete">Bestellung entsperren</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="362"/>
        <source>The project contains orders. Please delete the orders or move them to another project.</source>
        <translation>Das Projekt enthält Bestellungen. Bitte löschen Sie die Bestellungen oder verschieben Sie sie in ein anderes Projekt.</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="628"/>
        <source>New Order</source>
        <translation type="obsolete">Neue Bestellung</translation>
    </message>
</context>
<context>
    <name>ProjectModel</name>
    <message>
        <location filename="projectexplorer.py" line="45"/>
        <source>number</source>
        <translation type="obsolete">Nummer</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="46"/>
        <source>net</source>
        <translation>Netto</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="47"/>
        <source>gross</source>
        <translation>Brutto</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="80"/>
        <source>distributor</source>
        <translation type="obsolete">Lieferant</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="77"/>
        <source>order number</source>
        <translation type="obsolete">Auftragsnummer</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="79"/>
        <source>date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="44"/>
        <source>projectnumber</source>
        <translation>Projektnummer</translation>
    </message>
    <message>
        <location filename="projectexplorer.py" line="45"/>
        <source>projectname</source>
        <translation>Projektname</translation>
    </message>
</context>
<context>
    <name>ProjectWidget</name>
    <message>
        <location filename="project.py" line="81"/>
        <source>project number:</source>
        <translation>Projektnummer:</translation>
    </message>
    <message>
        <location filename="project.py" line="86"/>
        <source>project name:</source>
        <translation>Projektname:</translation>
    </message>
    <message>
        <location filename="project.py" line="91"/>
        <source>notes:</source>
        <translation>Notizen:</translation>
    </message>
    <message>
        <location filename="project.py" line="128"/>
        <source>documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="project.py" line="129"/>
        <source>orders</source>
        <translation>Bestellungen</translation>
    </message>
    <message>
        <location filename="project.py" line="157"/>
        <source>Project %s</source>
        <translation>Projekt %s</translation>
    </message>
    <message>
        <location filename="project.py" line="208"/>
        <source>The number {} is used by another project. Please use another number</source>
        <translation>Die Projektnummer {} wird bereits verwendet. Bitte wählen Sie eine ander Nummer.</translation>
    </message>
    <message>
        <location filename="project.py" line="217"/>
        <source>The directory with the number {} already exists. Do you want to overwrite?</source>
        <translation>Das Verzeichnis mit der Projektnummer {} existiert bereits. Soll es überschrieben werden?</translation>
    </message>
    <message>
        <location filename="project.py" line="130"/>
        <source>material</source>
        <translation>Material</translation>
    </message>
</context>
<context>
    <name>PropertyDialog</name>
    <message>
        <location filename="properties.py" line="31"/>
        <source>key:</source>
        <translation>Schlüssel:</translation>
    </message>
    <message>
        <location filename="properties.py" line="37"/>
        <source>value:</source>
        <translation>Wert:</translation>
    </message>
</context>
<context>
    <name>PropertyModel</name>
    <message>
        <location filename="properties.py" line="103"/>
        <source>property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <location filename="properties.py" line="103"/>
        <source>value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>PropertyWidget</name>
    <message>
        <location filename="properties.py" line="163"/>
        <source>add property</source>
        <translation>Eigenschaft hinzufügen</translation>
    </message>
    <message>
        <location filename="properties.py" line="167"/>
        <source>remove property</source>
        <translation>Eigenschaft entfernen</translation>
    </message>
    <message>
        <location filename="properties.py" line="171"/>
        <source>paste</source>
        <translation>Einfügen</translation>
    </message>
</context>
<context>
    <name>StockChangeDialog</name>
    <message>
        <location filename="stockchange.py" line="37"/>
        <source>Make a stock change</source>
        <translation>Bestandsänderung</translation>
    </message>
    <message>
        <location filename="stockchange.py" line="40"/>
        <source>article name:</source>
        <translation>Artikelname:</translation>
    </message>
    <message>
        <location filename="stockchange.py" line="46"/>
        <source>stock change:</source>
        <translation>Bestandsänderung:</translation>
    </message>
    <message>
        <location filename="stockchange.py" line="40"/>
        <source>resuling stock:</source>
        <translation type="obsolete">Restbestand:</translation>
    </message>
    <message>
        <location filename="stockchange.py" line="57"/>
        <source>comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="stockchange.py" line="53"/>
        <source>stock:</source>
        <translation>Bestand:</translation>
    </message>
</context>
<context>
    <name>StockHistoryTableModel</name>
    <message>
        <location filename="stockhistory.py" line="28"/>
        <source>date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="stockhistory.py" line="29"/>
        <source>quantity</source>
        <translation>Menge</translation>
    </message>
    <message>
        <location filename="stockhistory.py" line="30"/>
        <source>comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="stockhistory.py" line="31"/>
        <source>order</source>
        <translation>Bestellung</translation>
    </message>
</context>
<context>
    <name>StockTableModel</name>
    <message>
        <location filename="stocklist.py" line="28"/>
        <source>name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="29"/>
        <source>category</source>
        <translation type="obsolete">Kategorie</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="30"/>
        <source>manufacturer</source>
        <translation type="obsolete">Hersteller</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="31"/>
        <source>article nr.</source>
        <translation type="obsolete">Artikelnr.</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="32"/>
        <source>notes</source>
        <translation type="obsolete">Notizen</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="36"/>
        <source>in stock</source>
        <translation type="obsolete">Lagerbestand</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="27"/>
        <source>ID</source>
        <translation type="obsolete">ID</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="33"/>
        <source>stock quantity</source>
        <translation type="obsolete">Lagerbestand</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="34"/>
        <source>stock minimum</source>
        <translation type="obsolete">min. Lagerbestand</translation>
    </message>
</context>
<context>
    <name>StocklistWidget</name>
    <message>
        <location filename="stocklist.py" line="156"/>
        <source>Open...</source>
        <translation type="obsolete">Öffnen...</translation>
    </message>
    <message>
        <location filename="stocklist.py" line="150"/>
        <source>Stocklist</source>
        <translation type="obsolete">Lagerbestand</translation>
    </message>
</context>
<context>
    <name>self.self</name>
    <message>
        <location filename="order.py" line="690"/>
        <source>Move down</source>
        <translation type="obsolete">Nach unten</translation>
    </message>
</context>
</TS>
