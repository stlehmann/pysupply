"""
distributors.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
from PyQt5.QtCore import QAbstractTableModel, Qt, QModelIndex
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QDialog, QMessageBox, \
    QApplication, QDialogButtonBox, QTreeView, QMenu

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.helper import create_action
from pysupply.gui.distributor import DistributorDialog
import pysupply.gui.resources_rc


class Column(object):

    def __init__(self, name, label=None):
        self.name = name
        self.label = name if label is None else label


class DistributorsModel(QAbstractTableModel):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.distributors = []
        self.refresh()

        self.columns = [
            Column('name'),
            Column('website'),
            Column('username'),
            Column('password')
        ]

    def rowCount(self, parent=None):
        return len(self.distributors)

    def columnCount(self, parent=None):
        return len(self.columns)

    def data(self, index, role):
        if not index.isValid():
            return
        distributor = self.distributors[index.row()]
        attr = self.columns[index.column()].name
        if role in (Qt.DisplayRole, Qt.EditRole):
            if attr == "password" and role == Qt.DisplayRole:
                if distributor.password is not None:
                    return "*" * len(distributor.password)
            else:
                column = self.columns[index.column()].name
                return getattr(distributor, column)
        if role == Qt.UserRole:
            return distributor

    def setData(self, index: QModelIndex, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if role == Qt.EditRole:
            distributor = self.data(index, Qt.UserRole)
            attr = self.columns[index.column()].name
            setattr(distributor, attr, value)
            return True
        return False

    def refresh(self):
        self.distributors = list(session.query(db.Distributor).
                                 order_by(db.Distributor.name))

    def flags(self, index: QModelIndex):
        return Qt.ItemIsEditable | Qt.ItemIsSelectable | Qt.ItemIsEnabled

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label


class DistributorsWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.init_actions()

        self.model = DistributorsModel(self)
        self.treeview = QTreeView()
        self.treeview.setModel(self.model)
        self.treeview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeview.customContextMenuRequested.connect(self.show_contexmenu)
        self.treeview.doubleClicked.connect(self.edit_distributor)
        self.treeview.setRootIsDecorated(False)

        layout = QVBoxLayout()
        layout.addWidget(self.treeview)
        self.setLayout(layout)

        for c in range(self.model.columnCount()):
            self.treeview.resizeColumnToContents(c)

    def show_contexmenu(self, pos):
        menu = QMenu(self)
        menu.addAction(self.addDistributorAction)
        if self.treeview.currentIndex().isValid():
            menu.addAction(self.editDistributorAction)
            menu.addAction(self.removeDistributorAction)
            menu.setDefaultAction(self.editDistributorAction)
        else:
            menu.setDefaultAction(self.addDistributorAction)
        globalpos = self.treeview.viewport().mapToGlobal(pos)
        menu.popup(globalpos)

    def init_actions(self):
        self.addDistributorAction = create_action(
            self,
            self.tr("add distributor..."),
            image=":icons/add.png",
            slot=self.add_distributor
        )
        self.editDistributorAction = create_action(
            self,
            self.tr("edit distributor..."),
            image=":icons/edit.png",
            slot=self.edit_distributor
        )
        self.removeDistributorAction = create_action(
            self,
            self.tr("remove distributor"),
            image=":icons/remove.png",
            slot=self.remove_distributor
        )

    def add_distributor(self):
        distributor = db.Distributor()
        dlg = DistributorDialog(distributor, self)
        if dlg.exec_() == QDialog.Accepted:
            session.add(distributor)
            self.model.beginResetModel()
            self.model.refresh()
            self.model.endResetModel()

    def edit_distributor(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        distributor = self.model.data(index, Qt.UserRole)
        dlg = DistributorDialog(distributor, self)
        if dlg.exec_() == QDialog.Accepted:
            self.model.beginResetModel()
            self.model.refresh()
            self.model.endResetModel()

    def remove_distributor(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        distributor = self.model.data(index, Qt.UserRole)

        res = QMessageBox.question(
            self,
            QApplication.applicationName(),
            self.tr("Do you want to delete the distributor") +
                    " '%s'?" % distributor.name,
            buttons=QMessageBox.Yes | QMessageBox.No
        )

        if res == QMessageBox.Yes:
            self.model.beginResetModel()
            session.delete(distributor)
            self.model.refresh()
            self.model.endResetModel()


class DistributorsDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.distributorsWidget = DistributorsWidget()
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal
        )
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        layout = QVBoxLayout()
        layout.addWidget(self.distributorsWidget)
        layout.addWidget(self.buttons)
        self.setLayout(layout)
        self.resize(600, 400)
