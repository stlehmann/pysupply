"""
pysupply.gui.properties.py,

Copyright (c) 2015 by Stefan Lehmann,
licensed under the MIT license

"""
import logging

from PyQt5.QtCore import QAbstractTableModel, Qt, QModelIndex, QPoint, \
    QSettings
from PyQt5.QtWidgets import QWidget, QTableView, QGridLayout, QDialog, \
    QLabel, QLineEdit, QDialogButtonBox, QMenu, QApplication
import json
import pysupply.db as db
from pysupply.gui.helper import create_action


S_GRP_PROPERTIES = "pysupply/gui/properties"
S_HEADER_STATE = "header_state"

logger = logging.getLogger("pysupply.gui.properties")


class PropertyDialog(QDialog):

    def __init__(self, key, value, parent=None):
        super().__init__(parent)

        # keylabel
        self.keyLabel = QLabel(self.tr("key:"))
        self.keyLineEdit = QLineEdit()
        self.keyLabel.setBuddy(self.keyLineEdit)
        self.keyLineEdit.setText(key)

        # valuelabel
        self.valueLabel = QLabel(self.tr("value:"))
        self.valueLineEdit = QLineEdit()
        self.valueLabel.setBuddy(self.valueLineEdit)
        self.valueLineEdit.setText(value)

        # buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal
        )
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        # layout
        layout = QGridLayout()
        layout.addWidget(self.keyLabel, 0, 0)
        layout.addWidget(self.keyLineEdit, 0, 1)
        layout.addWidget(self.valueLabel, 1, 0)
        layout.addWidget(self.valueLineEdit, 1, 1)
        layout.addWidget(self.buttons, 2, 0, 1, 2)
        self.setLayout(layout)

    def key(self):
        return self.keyLineEdit.text()

    def value(self):
        return self.valueLineEdit.text()


class PropertyModel(QAbstractTableModel):

    def __init__(self, article: db.Article, parent=None):
        super().__init__(parent)
        self.article = article
        self.keys = []
        self.values = []
        self.refresh_data()

    def rowCount(self, QModelIndex_parent=None, *args, **kwargs):
        return len(self.keys)

    def columnCount(self, QModelIndex_parent=None, *args, **kwargs):
        return 2

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        if not index.isValid():
            return
        if role in (Qt.DisplayRole, Qt.EditRole):
            if index.column() == 0:
                return self.keys[index.row()]
            elif index.column() == 1:
                return self.values[index.row()]

    def setData(self, index: QModelIndex, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if role == Qt.EditRole:
            if index.column() == 0:
                self.keys[index.row()] = value
            elif index.column() == 1:
                self.values[index.row()] = value
            self.save_to_article()
            return True
        return False

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            header = [self.tr("property"), self.tr("value")]
            if orientation == Qt.Horizontal:
                return header[section]

    def flags(self, index: QModelIndex):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable

    def refresh_data(self):
        properties = {}
        if self.article.properties is not None:
            properties = json.loads(self.article.properties)
        self.keys = sorted(list(properties.keys()))
        self.values = [properties[key] for key in self.keys]

    def add(self, key, value):
        self.beginResetModel()
        if key in self.keys:
            i = self.keys.index(key)
            self.values[i] = value
        else:
            self.keys.append(key)
            self.values.append(value)
        self.save_to_article()
        self.refresh_data()
        self.endResetModel()

    def remove(self, index):
        self.beginRemoveRows(QModelIndex(), index.row(), index.row())
        self.keys.pop(index.row())
        self.values.pop(index.row())
        self.save_to_article()
        self.endRemoveRows()

    def save_to_article(self):
        properties = dict(zip(self.keys, self.values))
        self.article.properties = json.dumps(properties)


class PropertyWidget(QWidget):

    def __init__(self, article: db.Article, parent=None):
        super().__init__(parent)
        self._init_actions()

        # table
        self.model = PropertyModel(article)
        self.table = QTableView()
        self.table.setModel(self.model)
        self.table.setContextMenuPolicy(Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.show_contextmenu)

        # layout
        layout = QGridLayout()
        layout.addWidget(self.table, 0, 0)
        self.setLayout(layout)

        # load settings
        self.load_settings()

    def _init_actions(self):
        self.addpropertyAction = create_action(
            self, self.tr("add property"), image=":icons/add.png",
            slot=self.add
        )
        self.removepropertyAction = create_action(
            self, self.tr("remove property"), image=":icons/remove.png",
            slot=self.remove
        )
        self.pasteAction = create_action(
            self, self.tr("paste"), image=":icons/paste.png",
            slot=self.paste
        )

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_PROPERTIES)

        # header state
        try:
            self.table.horizontalHeader().restoreState(
                settings.value(S_HEADER_STATE))
        except TypeError as e:
            logger.warning(e)
            self.table.resizeColumnsToContents()

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_PROPERTIES)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.table.horizontalHeader().saveState())

    def add(self):
        dlg = PropertyDialog("", "", self)
        res = dlg.exec()
        if res == QDialog.Accepted:
            self.model.add(dlg.key(), dlg.value())

    def paste(self):
        text = QApplication.clipboard().text()
        properties_dict = db.Article.properties_str_to_dict(text)
        for key, value in properties_dict.items():
            self.model.add(key, value)

    def remove(self):
        index = self.table.currentIndex()
        self.model.remove(index)

    def show_contextmenu(self, point: QPoint):
        clipboard = QApplication.clipboard()
        menu = QMenu(self)
        menu.addAction(self.addpropertyAction)
        menu.addAction(self.removepropertyAction)
        if clipboard.text():
            menu.addAction(self.pasteAction)
        menu.popup(self.table.viewport().mapToGlobal(point))
