SOURCES = \
    article.py \
    articledistr.py \
    articlelist.py \
    catalog.py \
    category.py \
    distributors.py \
    distributor.py \
    documents.py \
    documentexplorer.py \
    mainwindow.py \
    material.py \
    materiallists.py \
    order.py \
    orderlist.py \
    properties.py \
    project.py \
    projectexplorer.py \
    stockchange.py \
    stockhistory.py
TRANSLATIONS = pysupply_de.ts