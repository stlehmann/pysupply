import sys
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QDialogButtonBox, \
    QGridLayout, QApplication


USER_NAME_SETTING = 'user/name'
USER_SIGN_SETTING = 'user/sign'


class UserDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        # username
        self.usernameLabel = QLabel(self.tr('Username'))
        self.usernameLineEdit = QLineEdit()
        self.usernameLineEdit.setMaxLength(64)

        # user sign
        self.userSignLabel = QLabel(self.tr('User Sign'))
        self.userSignLineEdit = QLineEdit()
        self.userSignLineEdit.setMaxLength(64)

        # buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Abort)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        layout = QGridLayout()
        layout.addWidget(self.usernameLabel, 0, 0)
        layout.addWidget(self.usernameLineEdit, 0, 1)
        layout.addWidget(self.userSignLabel, 1, 0)
        layout.addWidget(self.userSignLineEdit, 1, 1)
        layout.addWidget(self.buttons, 2, 0, 1, 2)
        self.setLayout(layout)

        self.load_settings()

    def closeEvent(self, event):
        self.save_settings()
        super().closeEvent(event)

    def accept(self):
        self.save_settings()
        super().accept()

    def load_settings(self):
        settings = QSettings()

        try:
            self.usernameLineEdit.setText(
                settings.value(USER_NAME_SETTING, '', type=str))
            self.userSignLineEdit.setText(
                settings.value(USER_SIGN_SETTING, '', type=str))
        except TypeError:
            return

    def save_settings(self):
        settings = QSettings()
        settings.setValue(USER_NAME_SETTING, self.usernameLineEdit.text())
        settings.setValue(USER_SIGN_SETTING, self.userSignLineEdit.text())


if __name__ == '__main__':
    app = QApplication(sys.argv)
    dlg = UserDialog()
    dlg.show()
    app.exec_()
