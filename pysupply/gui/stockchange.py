"""
pyads.gui.stockchange
---------------------

:author: Stefan Lehmann <stefan.st.lehmann@gmail.com
:license: MIT license, see license.txt for details

:created on 2017-02-03 13:37:47
:last modified by:   Stefan Lehmann
:last modified time: 2017-10-06 09:55:54

"""
from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QGridLayout, \
    QSpinBox, QDialogButtonBox, QWidget
from ..db import StockChangeItem


class StockSpinBox(QSpinBox):
    """ Customized SpinBox for stock changes """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setRange(-1E6, 1E6)


class StockChangeDialog(QDialog):
    """
    Dialog for adding or removing stock for a specific article

    """

    def __init__(self, stockchange_item: StockChangeItem,
                 parent: QWidget=None) -> None:

        super().__init__(parent)
        self.stockchange_item = stockchange_item
        self.setWindowTitle(self.tr('Make a stock change'))

        # Article Name
        self.article_name_label = QLabel(self.tr('article name:'))
        self.article_name_lineedit = QLineEdit()
        self.article_name_lineedit.setReadOnly(True)
        self.article_name_label.setBuddy(self.article_name_lineedit)

        # stock change
        self.stock_change_label = QLabel(self.tr('stock change:'))
        self.stock_change_spinbox = StockSpinBox()
        self.stock_change_spinbox.valueChanged.connect(
            self.handle_value_changed
        )

        # current and new stock
        self.stock_label = QLabel(self.tr('stock:'))
        self.stock_value_label = QLabel()

        # comment
        self.comment_label = QLabel(self.tr('comment:'))
        self.comment_lineedit = QLineEdit()
        self.comment_label.setBuddy(self.comment_lineedit)

        # buttons
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok |
                                        QDialogButtonBox.Cancel)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        # layout
        layout = QGridLayout()
        layout.addWidget(self.article_name_label, 0, 0)
        layout.addWidget(self.article_name_lineedit, 0, 1)
        layout.addWidget(self.stock_change_label, 2, 0)
        layout.addWidget(self.stock_change_spinbox, 2, 1)
        layout.addWidget(self.stock_label, 3, 0)
        layout.addWidget(self.stock_value_label, 3, 1)
        layout.addWidget(self.comment_label, 4, 0)
        layout.addWidget(self.comment_lineedit, 4, 1)
        layout.addWidget(self.buttons, 5, 0, 1, 2)
        self.setLayout(layout)

        # insert data
        item = self.stockchange_item
        self.article_name_lineedit.setText(item.article.name)
        self.stock_value_label.setText(
            '{0}'.format(item.article.stock.quantity)
        )

        # formatting
        size = self.size()
        self.resize(480, size.height())

    def accept(self):
        item = self.stockchange_item
        item.quantity_change = self.stock_change_spinbox.value()
        item.comment = self.comment_lineedit.text()
        return super().accept()

    def handle_value_changed(self, value: int) -> None:
        """ Handle value changes of stock_change_spinbox and refresh the
            displayed stock value. """

        curr_stock = self.stockchange_item.article.stock.quantity
        new_stock = curr_stock + value
        self.stock_value_label.setText(
            '{0} -> {1}'.format(curr_stock, new_stock)
        )


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    app = QApplication(sys.argv)
    dlg = StockChangeDialog(None, None)
    dlg.show()
    app.exec_()
