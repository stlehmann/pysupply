"""
project.py,

copyright (c) 2015 by Stefan Lehmann,
licensed under the MIT license

"""
import os
import logging
from datetime import datetime

from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt, QEvent, \
    QSettings
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QDataWidgetMapper, \
    QGridLayout, QTabWidget, QSplitter, QVBoxLayout, QMessageBox, QApplication

import pysupply.db as db
from pysupply.db import session
from pysupply.config import PROJECTSDIR
from pysupply.gui.documentexplorer import DocumentExplorerWidget
from pysupply.gui.orderlist import OrderlistWidget
from pysupply.gui.materiallists import MaterialListWidget
from pysupply.gui.markdownedit import MarkdownEdit


COLUMNS = ["name", "number", "notes"]

S_GRP_PROJECT = "pysupply/gui/project"
S_SPLITTER_STATE = "splitter_state"

logger = logging.getLogger("pysupply.gui.project")


class ProjectModel(QAbstractTableModel):

    """
    Model for Project data.

    """

    def __init__(self, project: db.Project, parent=None):
        super().__init__(parent)
        self.project = project

    def rowCount(self, QModelIndex_parent=None, *args, **kwargs):
        return 1

    def columnCount(self, QModelIndex_parent=None, *args, **kwargs):
        return len(COLUMNS)

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        if not index.isValid():
            return
        if role in (Qt.DisplayRole, Qt.EditRole):
            return getattr(self.project, COLUMNS[index.column()])

    def setData(self, index: QModelIndex, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if role == Qt.EditRole:
            setattr(self.project, COLUMNS[index.column()], value)
            return True
        return False


class ProjectWidget(QWidget):

    """
    Widget for editing project in MainWindow.

    """

    def __init__(self, project: db.Project, mainwindow, parent=None):
        super().__init__(parent)

        self.project = project
        self.model = ProjectModel(self.project)
        self.mainwindow = mainwindow

        # number
        self.numberLabel = QLabel(self.tr("project number:"))
        self.numberLineEdit = QLineEdit()
        self.numberLabel.setBuddy(self.numberLineEdit)

        # name
        self.nameLabel = QLabel(self.tr("project name:"))
        self.nameLineEdit = QLineEdit()
        self.nameLabel.setBuddy(self.nameLineEdit)

        # notes
        self.notesLabel = QLabel(self.tr("notes:"))
        self.notesMarkdownEdit = MarkdownEdit()
        self.notesLabel.setBuddy(self.notesMarkdownEdit)

        # header widget
        self.headerWidget = QWidget()
        # self.headerWidget.setMaximumHeight(200)

        # tabulator widget
        # documents
        # if project has no number us id
        if not self.project.number:
            documents_dir = os.path.join("id" + str(self.project.id))
        else:
            documents_dir = os.path.join(PROJECTSDIR, self.project.number)

        if not os.path.exists(PROJECTSDIR):
            os.mkdir(PROJECTSDIR)
        if not os.path.exists(documents_dir):
            os.mkdir(documents_dir)

        self.documentsWidget = DocumentExplorerWidget(documents_dir)

        # ordersTableView
        self.orderlist = OrderlistWidget(
            self,
            filter_=lambda x: x.project == self.project
        )
        self.orderlist.treeview.viewport().installEventFilter(self)

        # materialTableView
        self.materiallist = MaterialListWidget(
            self.project, self.mainwindow, self)
        # self.materiallist = QWidget()

        # tab widget
        self.tabWidget = QTabWidget()
        self.tabWidget.addTab(self.documentsWidget, self.tr("documents"))
        self.tabWidget.addTab(self.orderlist, self.tr("orders"))
        self.tabWidget.addTab(self.materiallist, self.tr("material"))

        # splitter
        self.splitter = QSplitter()
        self.splitter.setOrientation(Qt.Vertical)
        self.splitter.setChildrenCollapsible(False)

        # layout
        # headerWidget
        layout = QGridLayout()
        layout.addWidget(self.numberLabel, 0, 0)
        layout.addWidget(self.numberLineEdit, 0, 1)
        layout.addWidget(self.nameLabel, 1, 0)
        layout.addWidget(self.nameLineEdit, 1, 1)
        layout.addWidget(self.notesLabel, 2, 0)
        layout.addWidget(self.notesMarkdownEdit, 2, 1)
        self.headerWidget.setLayout(layout)

        # splitter
        self.splitter.addWidget(self.headerWidget)
        self.splitter.addWidget(self.tabWidget)

        # self
        layout = QVBoxLayout()
        layout.addWidget(self.splitter)
        self.setLayout(layout)

        self.setWindowTitle(self.tr("Project %s") % self.project.name)

        # data mapper
        self.numberLineEdit.setText(self.project.number)

        self.mapper = QDataWidgetMapper()
        self.mapper.setModel(self.model)
        self.mapper.addMapping(self.nameLineEdit, COLUMNS.index("name"))
        self.mapper.addMapping(self.notesMarkdownEdit.editor,
                               COLUMNS.index("notes"))
        self.mapper.toFirst()
        self.notesMarkdownEdit.refresh_viewer()

        self.numberLineEdit.editingFinished.connect(self.number_changed)
        self.load_settings()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_PROJECT)

        # splitter state
        try:
            self.splitter.restoreState(settings.value(S_SPLITTER_STATE))
        except TypeError as e:
            logger.debug(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_PROJECT)

        # splitter state
        settings.setValue(S_SPLITTER_STATE, self.splitter.saveState())

    def number_changed(self):
        # original project number
        org_number = self.project.number
        if org_number:
            org_project_dir = os.path.join(PROJECTSDIR, org_number)

        # new project number
        new_number = self.numberLineEdit.text()
        if new_number:
            new_project_dir = os.path.join(PROJECTSDIR, new_number)
        else:
            return

        if org_number == new_number:
            return

        numbers = [p.number for p in session.query(db.Project)]
        if new_number in numbers:
            QMessageBox.warning(
                self, QApplication.applicationName(),
                self.tr("The number {} is used by another project. Please "
                        "use another number".format(new_number))
            )
            self.numberLineEdit.setText(org_number)
            return

        if os.path.exists(new_project_dir):
            res = QMessageBox.question(
                self, QApplication.applicationName(),
                self.tr("The directory with the number {} already "
                        "exists. Do you want to overwrite?".format(new_number))
            )

            if res == QMessageBox.No:
                self.numberLineEdit.setText(self.project.number)
                return
        else:
            if org_number:
                os.rename(org_project_dir, new_project_dir)
            else:
                os.mkdir(new_project_dir)

        self.project.number = new_number
        session.commit()

    def eventFilter(self, obj, event):
        if obj == self.orderlist.treeview.viewport():
            if event.type() == QEvent.MouseButtonDblClick:
                try:
                    order = self.orderlist.current_order
                except AttributeError:
                    pass
                else:
                    if order is not None:
                        self.mainwindow.open_order(order)
                        return True
        return False

    def closeEvent(self, event):
        self.orderlist.save_settings()
        self.documentsWidget.save_settings()
        self.materiallist.save_settings()
        session.commit()
        self.save_settings()
        event.accept()

    def add_order(self):
        order = db.Order()
        order.project = self.project
        order.date = datetime.today()
        session.add(order)
        session.commit()

        self.mainwindow.open_order(order)
        self.orderlist.refresh()
