"""
distributor.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
import pkgutil

from PyQt5.QtCore import QVariant, Qt, QAbstractTableModel
from PyQt5.QtWidgets import QWidget, QDialog, QDialogButtonBox, QVBoxLayout, \
    QDataWidgetMapper, QLabel, QLineEdit, QGridLayout, QPlainTextEdit, \
    QComboBox, QItemDelegate

import pysupply.db as db
from ..plugins import distributors as dist_plugins
from pysupply.db import session


def get_column(name):
    return db.Distributor.__table__.columns.keys().index(name)


class PluginDelegate(QItemDelegate):

    def setEditorData(self, editor, index):
        if isinstance(editor, QComboBox):
            data_row = editor.findData(index.model().data(index))
            editor.setCurrentIndex(data_row)
            return
        return QItemDelegate.setEditorData(self, editor, index)

    def setModelData(self, editor, model, index):
        if isinstance(editor, QComboBox):
            model.setData(index, editor.currentData())
            return
        return QItemDelegate.setModelData(self, editor, model, index)


class DistributorModel(QAbstractTableModel):

    def __init__(self, distributor: db.Distributor, parent=None):
        super().__init__(parent)
        self.distributor = distributor

    def rowCount(self, parent):
        return 1

    def columnCount(self, parent):
        return len(db.Distributor.__table__.columns.keys())

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()

        key = db.Distributor.__table__.columns.keys()[index.column()]
        if role in (Qt.DisplayRole, Qt.EditRole):
            return getattr(self.distributor, key)

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False

        if role == Qt.EditRole:
            key = db.Distributor.__table__.columns.keys()[index.column()]
            setattr(self.distributor, key, value)
            session.commit()
            return True
        return False


class DistributorWidget(QWidget):

    def __init__(self, distributor, parent=None):
        super().__init__(parent)

        self.model = DistributorModel(distributor)

        # name
        self.nameLabel = QLabel(self.tr("name:"))
        self.nameLineEdit = QLineEdit()
        self.nameLabel.setBuddy(self.nameLineEdit)

        # address
        self.addressLabel = QLabel(self.tr("address:"))
        self.addressTextEdit = QPlainTextEdit()
        self.addressLabel.setBuddy(self.addressTextEdit)

        # contact person
        self.contactPersonLabel = QLabel(self.tr('contact person'))
        self.contactPersonLineEdit = QLineEdit()
        self.contactPersonLineEdit.setMaxLength(64)
        self.contactPersonLabel.setBuddy(self.contactPersonLineEdit)

        # website
        self.websiteLabel = QLabel(self.tr("website:"))
        self.websiteLineEdit = QLineEdit()
        self.websiteLabel.setBuddy(self.websiteLineEdit)

        # username
        self.usernameLabel = QLabel(self.tr("username:"))
        self.usernameLineEdit = QLineEdit()
        self.usernameLabel.setBuddy(self.usernameLineEdit)

        # password
        self.passwordLabel = QLabel(self.tr("password:"))
        self.passwordLineEdit = QLineEdit()
        self.passwordLabel.setBuddy(self.passwordLineEdit)

        # plugin
        self.pluginLabel = QLabel("Plugin:")
        self.pluginComboBox = QComboBox()
        self.pluginLabel.setBuddy(self.pluginComboBox)

        self.pluginComboBox.addItem('-', '')
        for plugin_name in dist_plugins.get_plugin_list():
            self.pluginComboBox.addItem(plugin_name, plugin_name)

        # mapper
        self.mapper = QDataWidgetMapper()
        self.mapper.setItemDelegate(PluginDelegate())
        self.mapper.setModel(self.model)
        self.mapper.addMapping(self.nameLineEdit, get_column("name"))
        self.mapper.addMapping(self.contactPersonLineEdit, get_column('contact_person'))
        self.mapper.addMapping(self.websiteLineEdit, get_column("website"))
        self.mapper.addMapping(self.usernameLineEdit, get_column("username"))
        self.mapper.addMapping(self.passwordLineEdit, get_column("password"))
        self.mapper.addMapping(self.addressTextEdit, get_column("address"))
        self.mapper.addMapping(self.pluginComboBox, get_column("plugin_name"))
        self.mapper.setSubmitPolicy(self.mapper.AutoSubmit)
        self.mapper.toFirst()

        # layout
        layout = QGridLayout()
        layout.addWidget(self.nameLabel, 0, 0)
        layout.addWidget(self.nameLineEdit, 0, 1)
        layout.addWidget(self.addressLabel, 1, 0)
        layout.addWidget(self.addressTextEdit, 1, 1, 2, 1)
        layout.addWidget(self.contactPersonLabel, 2, 0)
        layout.addWidget(self.contactPersonLineEdit, 2, 1)
        layout.addWidget(self.websiteLabel, 3, 0)
        layout.addWidget(self.websiteLineEdit, 3, 1)
        layout.addWidget(self.usernameLabel, 4, 0)
        layout.addWidget(self.usernameLineEdit, 4, 1)
        layout.addWidget(self.passwordLabel, 5, 0)
        layout.addWidget(self.passwordLineEdit, 5, 1)
        layout.addWidget(self.pluginLabel, 6, 0)
        layout.addWidget(self.pluginComboBox, 6, 1)
        self.setLayout(layout)


class DistributorDialog(QDialog):

    def __init__(self, session: db.Session,
                 distributor: db.Distributor, parent=None):
        super().__init__(parent)

        self.distributorWidget = DistributorWidget(session, distributor)
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Abort)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        layout = QVBoxLayout()
        layout.addWidget(self.distributorWidget)
        layout.addWidget(self.buttons)
        self.setLayout(layout)
