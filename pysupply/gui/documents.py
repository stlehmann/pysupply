"""
documents.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
import os
import logging
import sys
import subprocess
import shutil

from PyQt5.QtCore import Qt, QPoint, QModelIndex, QEvent, QMimeData, QUrl
from PyQt5.QtGui import QFocusEvent
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout, \
    QListView, QFileSystemModel, QAbstractItemView, QMenu, QMessageBox, \
    QFileDialog, QItemDelegate, QLineEdit, QStyleOptionViewItem

from pysupply.config import ARTICLEDIR, ORDERDIR
from pysupply.gui.helper import create_action
from pysupply.gui import resources_rc


logger = logging.getLogger("pysupply.gui.documents")


def get_articledir(article_id: str):
    """
    Get directory where the documents for the article with the given id are
    stored.

    :param article_id: unique id of the article
    :return: directory path

    """
    return os.path.join(ARTICLEDIR, article_id)


def get_orderdir(orderid: str) -> str:
    """
    Get directory of order documents.

    """
    return os.path.join(ORDERDIR, orderid)


class Editor(QLineEdit):

    def focusInEvent(self, event: QFocusEvent):
        root, ext = os.path.splitext(self.text())
        self.setSelection(0, len(root))


class MyItemDelegate(QItemDelegate):

    def createEditor(self, parent: QWidget, style: QStyleOptionViewItem,
                     index: QModelIndex):
        editor = Editor(parent)
        return editor

    def setEditorData(self, editor=QWidget, index=QModelIndex):
        model = index.model()
        editor.setText(model.data(index))


class DocumentsWidget(QWidget):

    def __init__(self, root: str, dirname: str, parent=None):
        super().__init__(parent)
        self.init_actions()

        self.rootdir = os.path.join(root, dirname)
        if not os.path.exists(root):
            os.mkdir(root)
        if not os.path.exists(self.rootdir):
            os.mkdir(self.rootdir)

        self.model = QFileSystemModel()
        self.model.setRootPath(self.rootdir)
        self.model.setReadOnly(False)

        self.documentlist = QListView()
        self.documentlist.setModel(self.model)
        self.documentlist.setRootIndex(self.model.index(self.rootdir))
        self.documentlist.setEditTriggers(QAbstractItemView.EditKeyPressed)
        self.documentlist.doubleClicked.connect(self.open_file)
        self.documentlist.setAcceptDrops(True)
        self.documentlist.setContextMenuPolicy(Qt.CustomContextMenu)
        self.documentlist.customContextMenuRequested.connect(
            self.show_contextmenu)
        self.documentlist.setItemDelegate(MyItemDelegate())
        self.documentlist.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.documentlist.installEventFilter(self)
        layout = QVBoxLayout()
        layout.addWidget(self.documentlist)
        self.setLayout(layout)

    def init_actions(self):
        # add document
        self.addDocumentAction = create_action(
            self,
            self.tr("Add..."),
            image=":icons/add.png",
            slot=self.add_document
        )

        # open directory
        self.openDirAction = create_action(
            self,
            self.tr("Open directory"),
            image=":icons/open_directory.png",
            slot=self.open_dir
        )

        # delete document
        self.deleteDocumentAction = create_action(
            self,
            self.tr("Delete..."),
            slot=self.delete_documents,
            shortcut=Qt.Key_Delete,
            image=":icons/remove.png"
        )

        # open document
        self.openDocumentAction = create_action(
            self,
            self.tr("Open"),
            slot=self.open_document,
            image=":icons/open.png"
        )

        # copy
        self.copyAction = create_action(
            self, self.tr("Copy"), slot=self.copy_document,
            image=":icons/editcopy.png", shortcut=Qt.Key_Copy
        )

        # paste
        self.pasteAction = create_action(
            self, self.tr("Paste"), slot=self.paste_document,
            image=":icons/paste.png", shortcut=Qt.Key_Paste
        )

    def open_file(self, index):
        filename = self.model.filePath(index)
        try:
            os.startfile(filename)
        except AttributeError:
            subprocess.call(["open", filename])

    def open_dir(self):
        path = self.rootdir
        if sys.platform == "darwin":
            subprocess.call(['open', '--', path])
        elif sys.platform == "win32":
            subprocess.call(['explorer', path])
        elif sys.platform == "linux2":
            subprocess.call(['gnome-open', '--', path])

    def add_document(self):
        filenames, filter = QFileDialog.getOpenFileNames(
            self,
            self.tr("Add documents to order"),
            filter=self.tr(
                "Document files (*.pdf *.doc *.txt);; All files (*.*)")
        )

        for src in filenames:
            dst = os.path.join(self.rootdir, os.path.basename(src))
            ow = False

            # check if file exists
            if os.path.exists(dst):
                res = QMessageBox.question(
                    self, QApplication.applicationName(),
                    self.tr("The file {} exists already. Overwrite?")
                    .format(os.path.basename(dst)),
                    QMessageBox.Yes | QMessageBox.YesAll |
                    QMessageBox.No | QMessageBox.Abort
                )

                if res == QMessageBox.YesAll:
                    ow = True

                if res == QMessageBox.Yes or ow:
                    shutil.copy(src, dst)

                elif res == QMessageBox.Abort:
                    return

            else:
                shutil.copy(src, dst)

            logger.debug("Added document '%s' to directory '%s'" %
                         (src, self.rootdir))

    def delete_documents(self):

        def remove():
            try:
                os.remove(filename)
            except PermissionError:
                res = QMessageBox.critical(
                    self, self.tr('Error deleting file'),
                    self.tr('Could not delete file "{}". Permission error.')
                    .format(filename),
                    buttons=QMessageBox.Retry | QMessageBox.Ignore |
                            QMessageBox.Cancel
                )
                if res == QMessageBox.Retry:
                    remove()
                elif res == QMessageBox.Cancel:
                    return False
            return True

        removeall = False
        for index in self.documentlist.selectedIndexes():
            if not index.isValid():
                continue
            filename = self.model.filePath(index)
            if removeall:
                if not remove():
                    return
                continue
            res = QMessageBox.question(
                self,
                QApplication.applicationName(),
                self.tr(
                    "Do you want to delete the document '%s'?") % filename,
                buttons=QMessageBox.Yes | QMessageBox.YesAll | QMessageBox.No |
                        QMessageBox.NoAll
            )
            if res == QMessageBox.Yes:
                if not remove():
                    return
            elif res == QMessageBox.YesAll:
                if not remove():
                    return
                removeall = True
            elif res == QMessageBox.No:
                continue
            elif res == QMessageBox.NoAll:
                return

    def eventFilter(self, obj, event):
        # documentlist
        if obj == self.documentlist:
            # key event
            if event.type() == QEvent.KeyPress:
                if event.key() == Qt.Key_Delete:
                    self.delete_documents()
                    return True

        return False

    def open_document(self):
        for index in self.documentlist.selectedIndexes():
            if index.isValid():
                self.open_file(index)

    def copy_document(self):
        urls = []
        for index in self.documentlist.selectedIndexes():
            if index.isValid():
                filename = self.model.filePath(index)
                urls.append(QUrl.fromLocalFile(filename))

        data = QMimeData()
        data.setUrls(urls)
        QApplication.clipboard().setMimeData(data)

    def paste_document(self):
        index = self.documentlist.currentIndex()
        data = QApplication.clipboard().mimeData()
        urls = data.urls()

        dest = (
            os.path.split(self.model.filePath(index))[0] if index.isValid()
            else self.rootdir
        )

        for url in urls:
            shutil.copy(url.toLocalFile(), dest)

    def show_contextmenu(self, pos: QPoint):
        menu = QMenu(self)
        index = self.documentlist.currentIndex()

        if index.isValid():
            menu.addAction(self.openDocumentAction)
            menu.addSeparator()

        clipboard = QApplication.clipboard()
        self.pasteAction.setEnabled(clipboard.mimeData().hasUrls())

        menu.addAction(self.addDocumentAction)
        menu.addAction(self.pasteAction)

        if index.isValid():
            menu.setDefaultAction(self.openDocumentAction)
            menu.addAction(self.copyAction)
            menu.addAction(self.deleteDocumentAction)
        else:
            menu.setDefaultAction(self.addDocumentAction)

        menu.addSeparator()
        menu.addAction(self.openDirAction)

        globalpos = self.documentlist.viewport().mapToGlobal(pos)
        menu.popup(globalpos)
