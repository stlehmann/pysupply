"""
pysupply.gui.articledistr.py - GUI objects for article distributors,

copyright (c) 2014 by Stefan Lehmann,
licensed under the MIT license

"""
import logging

from PyQt5.QtCore import QAbstractTableModel, Qt, QSettings
from PyQt5.QtWidgets import QWidget, QTableView, QVBoxLayout, QComboBox, \
    QItemDelegate, QDoubleSpinBox, QSpinBox

from pysupply.gui.helper import create_action
import pysupply.db as db
from pysupply.db import session


S_GRP_ARTICLEDISTR = "pysupply/gui/articledistr"
S_HEADER_STATE = "header_state"

logger = logging.getLogger("pysupply.gui.articledistr")


class ItemDelegate(QItemDelegate):

    def __init__(self, parent=None):
        super().__init__(parent)

    def createEditor(self, parent, option, index):
        model = index.model()
        attr = model.columns[index.column()].name

        if attr == "distributor":
            editor = QComboBox(parent)
            distributors = session.query(db.Distributor)\
                .order_by(db.Distributor.name)
            for distributor in distributors:
                editor.addItem(distributor.name, distributor)
            editor.showPopup()
            return editor
        elif attr == "price":
            editor = QDoubleSpinBox(parent)
            editor.setRange(0.0, 1e12)
            editor.setDecimals(2)
            editor.setSuffix(" €")
            return editor
        elif attr == "packaging_unit":
            editor = QSpinBox()
            return editor
        else:
            return super().createEditor(parent, option, index)

    def setEditorData(self, editor, index):
        if not index.isValid():
            return
        model = index.model()
        attr = model.columns[index.column()].name
        item = model.data(index, Qt.UserRole)

        if attr == "distributor":
            if item.distributor is None:
                editor.setCurrentIndex(0)
            else:
                editor.setCurrentText(item.distributor.name)
        elif attr == "price":
            if item.price is not None:
                editor.setValue(item.price)
        elif attr == "packaging_unit":
            editor.setValue(item.packaging_unit
                            if item.packaging_unit is not None else 1)
        else:
            super().setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        if not index.isValid():
            return
        attr = model.columns[index.column()].name
        item = model.data(index, Qt.UserRole)

        if attr == "distributor":
            item.distributor = editor.currentData()
        elif attr == "price":
            item.price = editor.value()
        elif attr == "packaging_unit":
            item.packaging_unit = editor.value()
        else:
            super().setModelData(editor, model, index)


class Column():

    def __init__(self, name, label=None):
        self.name = name
        self.label = label if label is not None else self.name


class ArticleDistributorModel(QAbstractTableModel):

    def __init__(self, items: [db.CatalogItem], parent=None):
        super().__init__(parent)
        self.items = items

        self.columns = [
            Column("distributor", self.tr("distributor")),
            Column("name", self.tr("name")),
            Column("order_number", self.tr("order nr.")),
            Column("packing_unit", self.tr("packing unit")),
            Column("price", self.tr("price"))
        ]

    def rowCount(self, parent):
        return len(self.items)

    def columnCount(self, parent):
        return len(self.columns)

    def data(self, index, role):
        if not index.isValid():
            return
        attr = self.columns[index.column()].name
        item = self.items[index.row()]

        if role in (Qt.DisplayRole, Qt.EditRole):
            if attr == "distributor":
                if item.distributor is None:
                    return
                else:
                    return item.distributor.name
            elif attr == "price":
                return "%.2f €" % item.price if item.price is not None else "-"
            else:
                return getattr(item, attr)

        elif role == Qt.TextAlignmentRole:
            if attr in ("price", "packing_unit"):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.UserRole:
            return self.items[index.row()]

    def setData(self, index, value, role):
        if not index.isValid:
            return False
        if role == Qt.EditRole:
            attr = self.columns[index.column()].name
            item = self.items[index.row()]
            setattr(item, attr, value)
            self.dataChanged.emit(index, index, [Qt.DisplayRole, Qt.EditRole])
            return True
        return False

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            elif orientation == Qt.Vertical:
                return section + 1

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsEditable


class ArticleDistributorsWidget(QWidget):

    def __init__(self, article: db.Article, parent=None):
        super().__init__(parent)

        self._init_actions()
        self.article = article

        # table
        self.tableview = QTableView()
        self.model = ArticleDistributorModel(list(self.article.catalog_items))
        self.model.dataChanged.connect(self.tableview.resizeColumnsToContents)
        self.tableview.setModel(self.model)
        self.tableview.setItemDelegate(ItemDelegate())
        self.tableview.addActions([
            self.addItemAction,
            self.removeItemAction
        ])
        self.tableview.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.tableview.horizontalHeader().setHighlightSections(False)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.tableview)
        self.setLayout(layout)

        # load settings
        self.load_settings()

    def _init_actions(self):
        self.addItemAction = create_action(
            self,
            self.tr("Add distributor"),
            image=":icons/add.png",
            slot=self.add_item
        )
        self.removeItemAction = create_action(
            self,
            self.tr("Remove distributor"),
            image=":icons/remove.png",
            slot=self.remove_item
        )

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLEDISTR)

        # header state
        try:
            self.tableview.horizontalHeader().restoreState(
                settings.value(S_HEADER_STATE))
        except TypeError as e:
            logger.warning(e)
            self.tableview.resizeColumnsToContents()

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLEDISTR)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.tableview.horizontalHeader().saveState()
        )

    def add_item(self):
        db.CatalogItem(article=self.article, name=self.article.name)
        self.refresh()

    def remove_item(self):
        index = self.tableview.currentIndex()
        item = self.model.data(index, Qt.UserRole)

        session.delete(item)
        session.commit()
        self.refresh()

    def refresh(self):
        self.model.beginResetModel()
        self.model.items = list(self.article.catalog_items)
        if len(self.model.items):
            self.tableview.resizeColumnsToContents()
        self.model.endResetModel()
