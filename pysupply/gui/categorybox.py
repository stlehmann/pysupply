"""
categorybox.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
from PyQt5.QtCore import QModelIndex, QObject, QEvent
from PyQt5.QtWidgets import QComboBox, QTreeView, QWidget, \
    QLabel, QHBoxLayout, QSizePolicy

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.catalog import CatalogModel, CategoryNode


class CategoryModel(CatalogModel):

    def init_root(self):
        def _find_children(category_id):
            categories = session.query(db.Category).\
                filter(db.Category.parent_id == category_id)

            for category in categories:
                branch = CategoryNode(category)
                children = _find_children(category.id)
                for child in children:
                    branch.insert_child(child)
                yield branch

        root = CategoryNode(None)
        for child in _find_children(0):
            root.insert_child(child)
        return root

    def columnCount(self, parent=QModelIndex()):
        return 1


class CategoryTreeView(QTreeView):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setHeaderHidden(True)


class CategoryComboBox(QComboBox):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setView(CategoryTreeView(self))
        self.setModel(CategoryModel(self))
        self.view().viewport().installEventFilter(self)
        self.skip_next_hide = False

    def eventFilter(self, obj: QObject, event: QEvent):
        if (obj == self.view().viewport() and
                event.type() == QEvent.MouseButtonPress):
            index = self.view().indexAt(event.pos())
            if not self.view().visualRect(index).contains(event.pos()):
                self.skip_next_hide = True
        return False

    def showPopup(self):
        policy = self.view().sizePolicy()
        policy.setHorizontalPolicy(QSizePolicy.MinimumExpanding)
        self.view().setSizePolicy(policy)
        self.setRootModelIndex(QModelIndex())
        QComboBox.showPopup(self)

    def hidePopup(self):
        if (self.skip_next_hide):
            self.skip_next_hide = False
        else:
            QComboBox.hidePopup(self)

    def setCurrentModelIndex(self, index: QModelIndex):
        """
        Set the current index of the ComboBox to the item specified by index.

        :param index: index of the item that shall be selected

        """
        if not index.isValid():
            return
        self.view().setCurrentIndex(index)
        self.setRootModelIndex(index.parent())
        self.setCurrentIndex(index.row())


class MainWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.label = QLabel(self.tr("category:"))
        self.categoryComboBox = CategoryComboBox()

        layout = QHBoxLayout()
        layout.addWidget(self.label)
        layout.addWidget(self.categoryComboBox)
        self.setLayout(layout)
