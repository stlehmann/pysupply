"""
order.py

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
import logging
from datetime import datetime

from PyQt5.QtCore import QAbstractTableModel, Qt, QDate, QSize, \
    QModelIndex, QDataStream, QIODevice, QAbstractItemModel, \
    QLocale, QByteArray, QMimeData, QPoint, QObject, QEvent, QSettings
from PyQt5.QtGui import QFont, QFontMetrics
from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QDateEdit, \
    QGridLayout, QTableView, QDataWidgetMapper, QComboBox, QGroupBox, \
    QVBoxLayout, QItemDelegate, QStyleOptionViewItem, QDoubleSpinBox, \
    QPlainTextEdit, QSplitter, QSizePolicy, QMenu, QCompleter, QApplication, \
    QAbstractItemView, QMessageBox, QSpinBox
from sqlalchemy.orm.exc import NoResultFound

import pysupply.db as db
from pysupply.db import session, ORDERSTATE_DRAFT, ORDERSTATE_ORDERED, \
    ORDERSTATE_RECEIVED
from pysupply.gui.documents import DocumentsWidget, ORDERDIR
from pysupply.gui.helper import create_action, CURRENCY_MIN, CURRENCY_MAX, \
    Column, create_column_actions
from pysupply.config import TAX
import pysupply.gui.resources_rc


locale = QLocale()
logger = logging.getLogger("pysupply.gui.order")

ROW_SHIPPING = 0
ROW_NETTO = 1
ROW_BRUTTO = 2

S_GRP_ORDER = "pysupply/gui/order"
S_HEADER_STATE = "headerstate"
S_HIDDEN_COLUMNS = "hidden_columns"
S_SPLITTER1_STATE = "splitter1_state"
S_SPLITTER2_STATE = "splitter2_state"


def project_label(project):
    if project.number:
        return "{} - {}".format(project.number, project.name)
    else:
        return project.name


def add_to_stock(item: db.OrderItem) -> bool:
    article = item.article

    if article is None:
        logger.debug(
            "Could not add order item {} to stock. Not in catalog."
            .format(item.name)
        )
        return False

    if not item.added_to_stock:
        if article.stock is None:
            stock = db.StockItem()
            stock.quantity = 0
            stock.minimum = 0
            article.stock = stock
            session.commit()
        item.add_to_stock()
        return True
    return False


def remove_from_stock(item: db.OrderItem) -> bool:
    article = item.article
    if article is None:
        logger.debug(
            "Could not remove order item {} from stock. Not in catalog."
            .format(item.name)
        )
        return False

    if article.stock is None:
        logger.debug(
            "Could not remove article {} from stock. "
            "Not in stock.".format(article.name))
        return False

    if item.added_to_stock:
        article.stock.quantity -= item.count * item.packing_unit
        if article.stock.quantity < 0:
            logger.debug(
                "Invalid stock count of article {0}: {1}. "
                "Stock count will be set to 0."
                .format(article.name, article.stock.quantity)
            )
            article.stock.quantity = 0

        item.added_to_stock = False
        session.commit()
        return True
    return False


class OrderDelegate(QItemDelegate):

    def setEditorData(self, editor, index: QModelIndex):
        model = index.model()
        attr = model.columns[index.column()].name
        order = index.model().data(index, Qt.UserRole)
        if attr == "distributor":
            if order.distributor is None:
                return
            i = editor.findData(order.distributor)
            editor.setCurrentIndex(i)
        elif attr == "project":
            if order.project is None:
                return
            i = editor.findData(order.project)
            editor.setCurrentIndex(i)
        elif attr == "state":
            if order.state is None:
                return
            editor.setCurrentIndex(editor.findData(order.state))
        else:
            super().setEditorData(editor, index)

    def setModelData(self, editor, model, index: QModelIndex):
        attr = model.columns[index.column()].name
        if attr in ("distributor", "project", "state"):
            model.setData(index, editor.currentData())
        else:
            super().setModelData(editor, model, index)


class OrderItemDelegate(QItemDelegate):

    def __init__(self):
        super().__init__()

    def createEditor(self, parent: QWidget, option: QStyleOptionViewItem,
                     index: QModelIndex):
        model = index.model()
        attr = model.columns[index.column()].name
        if index.row() < len(model.items):
            if attr == "manufacturer":
                editor = QLineEdit(parent)
                manufacturers = session.query(db.Article.manufacturer)\
                    .group_by(db.Article.manufacturer)\
                    .filter(db.Article.manufacturer.isnot(None))
                completer = QCompleter([m[0] for m in manufacturers])
                editor.setCompleter(completer)
                editor.setGeometry(option.rect)
                return editor
            elif attr == "distributor":
                editor = QComboBox(parent)
                editor.setEditable(True)
                model = index.model()
                order_item = model.data(index, Qt.UserRole)
                if order_item.article is not None:
                    article = order_item.article
                    for catalog_item in article.catalog_items:
                        editor.addItem(
                            catalog_item.distributor.name, catalog_item
                        )
                else:
                    for distr in session.query(db.Distributor)\
                            .order_by(db.Distributor.name):
                        editor.addItem(distr.name, distr)
                editor.setGeometry(option.rect)
                editor.currentIndexChanged.connect(editor.close)
                return editor
            elif attr == "ppu":
                editor = QDoubleSpinBox(parent)
                editor.setSuffix(" €")
                editor.setRange(CURRENCY_MIN, CURRENCY_MAX)
                editor.setDecimals(2)
                editor.setButtonSymbols(QDoubleSpinBox.NoButtons)
                editor.setGeometry(option.rect)
                return editor
            elif attr == 'packing_unit':
                editor = QSpinBox(parent)
                editor.setRange(1, 1000)
                editor.setGeometry(option.rect)
                return editor

        # shipping costs
        elif index.row() == len(model.items) + ROW_SHIPPING \
                and model.columns[index.column()].name == "total":
            editor = QDoubleSpinBox(parent)
            editor.setSuffix(" €")
            editor.setRange(CURRENCY_MIN, CURRENCY_MAX)
            editor.setDecimals(2)
            editor.setButtonSymbols(QDoubleSpinBox.NoButtons)
            editor.setGeometry(option.rect)
            return editor
        return super().createEditor(parent, option, index)

    def setEditorData(self, editor: QWidget, index: QModelIndex):
        model = index.model()
        item = model.data(index, Qt.UserRole)
        attr = model.columns[index.column()].name
        if index.row() < len(model.items):
            if attr == "manufacturer":
                editor.setText(item.manufacturer)
            elif attr == "distributor":
                if item.distributor is None:
                    return
                i = editor.findData(item.catalog_item)
                if i > -1:
                    editor.setCurrentIndex(i)
            elif attr == "ppu":
                if item.ppu is not None:
                    editor.setValue(item.ppu)
            elif attr == "packing_unit":
                if item.packing_unit is not None:
                    editor.setValue(item.packing_unit)
                else:
                    editor.setValue(1)
            else:
                super().setEditorData(editor, index)
        elif index.row() == len(model.items) + ROW_SHIPPING \
                and model.columns[index.column()].name == "total":
            if model.order.shipping_costs is not None:
                editor.setValue(model.order.shipping_costs)

    def setModelData(self, editor: QWidget, model: QAbstractItemModel,
                     index: QModelIndex):
        attr = model.columns[index.column()].name
        if index.row() < len(model.items):
            if attr == "manufacturer":
                model.setData(index, editor.text())
            elif attr == "distributor":
                orderitem = model.data(index, Qt.UserRole)
                if isinstance(editor.currentData(), db.CatalogItem):
                    catalog_item = editor.currentData()
                    articlenr_index = model.index(
                        index.row(), model.column_by_name("articlenr")
                    )
                    ppu_index = model.index(index.row(),
                                            model.column_by_name("ppu"))
                    packing_unit_index = model.index(
                        index.row(), model.column_by_name('packing_unit'))
                    model.setData(index, catalog_item.distributor)
                    model.setData(articlenr_index, catalog_item.order_number)
                    model.setData(ppu_index, catalog_item.price)
                    model.setData(packing_unit_index,
                                  catalog_item.packing_unit)
                    orderitem.catalog_item = catalog_item
                elif isinstance(editor.currentData(), db.Distributor):
                    distributor = editor.currentData()
                    model.setData(index, distributor.id)
            elif attr == "ppu":
                model.setData(index, editor.value())
            elif attr == 'packing_unit':
                model.setData(index, editor.value())
            else:
                super().setModelData(editor, model, index)
        # shipping costs
        elif index.row() == len(model.items) + ROW_SHIPPING \
                and model.columns[index.column()].name == "total":
            model.setData(index, editor.value())


class OrderItemsModel(QAbstractTableModel):

    def __init__(self, order: db.Order, parent=None):
        super().__init__(parent)
        self.order = order
        self.dirty = False
        self.items = []
        self.columns = [
            Column("count", self.tr("count")),
            Column("name", self.tr("article")),
            Column("manufacturer", self.tr("manufacturer")),
            Column("manufacturer_nr", self.tr("manufacturer's nr.")),
            Column("distributor", self.tr("distributor")),
            Column("articlenr", self.tr("article nr.")),
            Column("received", self.tr("received")),
            Column("added_to_stock", self.tr("added to stock")),
            Column("ppu", self.tr("ppu")),
            Column('packing_unit', self.tr('packing unit')),
            Column("total", self.tr("total")),
            Column("comment", self.tr("comment"))
        ]
        self.refresh()

    def rowCount(self, parent=QModelIndex()):
        return len(self.items) + 3

    def columnCount(self, parent=QModelIndex()):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):

        if not index.isValid():
            return

        attr = self.columns[index.column()].name
        total_column = [c.name for c in self.columns].index("total")

        if role in (Qt.DisplayRole, Qt.EditRole):
            # netto total
            if index.row() == len(self.items) + ROW_NETTO:
                if index.column() == total_column - 1:
                    return self.tr("total net:")
                elif index.column() == total_column:
                    netto = sum(item.total() or 0 for item in self.items) \
                        + (self.order.shipping_costs or 0)
                    return locale.toCurrencyString(netto)

            # shipping costs
            elif index.row() == len(self.items) + ROW_SHIPPING:
                if index.column() == total_column - 1:
                    return self.tr("shipping costs:")
                elif index.column() == total_column:
                    return locale.toCurrencyString(
                        self.order.shipping_costs or 0
                    )

            # brutto total
            elif index.row() == len(self.items) + ROW_BRUTTO:
                if index.column() == total_column - 1:
                    return self.tr("total gross:")
                elif index.column() == total_column:
                    brutto = (sum(item.total() or 0 for item in self.items) +
                              (self.order.shipping_costs or 0)) * TAX
                    return locale.toCurrencyString(brutto)

            # table cells
            elif index.row() < len(self.items):
                item = self.items[index.row()]
                if attr == "distributor":
                    if item.distributor is None:
                        return "-"
                    return item.distributor.name
                elif attr == "total":
                    if item.count is None or item.ppu is None:
                        return "-"
                    else:
                        return locale.toCurrencyString(item.count * item.ppu)
                elif attr == "ppu":
                    if item.ppu is None:
                        return "-"
                    return locale.toCurrencyString(item.ppu)
                elif attr in ("added_to_stock", "received"):
                    return ""
                else:
                    return getattr(item, attr)

        elif role == Qt.CheckStateRole:
            if index.row() < len(self.items):
                item = self.items[index.row()]
                if attr == "added_to_stock":
                    return Qt.Checked if item.added_to_stock else Qt.Unchecked
                elif attr == "received":
                    return Qt.Checked if item.received else Qt.Unchecked

        elif role == Qt.TextAlignmentRole:
            if attr in ("ppu", "total", "packing_unit"):
                return Qt.AlignVCenter | Qt.AlignRight
            elif attr in ("count", "added_to_stock"):
                return Qt.AlignVCenter | Qt.AlignCenter
            else:
                return Qt.AlignVCenter | Qt.AlignLeft

        elif role == Qt.FontRole:
            font = QFont()
            if attr in ("ppu", "total") and index.row() >= len(self.items):
                font.setBold(True)
            return font

        elif role == Qt.UserRole:
            if index.row() < len(self.items):
                return self.items[index.row()]

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False

        attr = self.columns[index.column()].name

        if role == Qt.EditRole:
            if index.row() < len(self.items):
                item = self.items[index.row()]
                if attr == "distributor":
                    if isinstance(value, int):
                        try:
                            item.distributor = (
                                session.query(db.Distributor)
                                .filter(db.Distributor.id == value).one()
                            )
                        except NoResultFound:
                            return False
                    elif isinstance(value, db.Distributor):
                        item.distributor = value
                else:
                    setattr(item, attr, value)

                self.dirty = True
                self.dataChanged.emit(index, index, [Qt.DisplayRole])
                session.commit()
                return True

            elif index.row() == len(self.items) + ROW_SHIPPING:
                self.order.shipping_costs = float(value)
                session.commit()
                return True

        elif role == Qt.CheckStateRole:
            if index.row() < len(self.items):
                item = self.items[index.row()]

                if attr == "received":
                    b = (value == Qt.Checked)
                    item.received = b
                    if b:
                        add_to_stock(item)
                    else:
                        remove_from_stock(item)
                    return True

                elif attr == "added_to_stock":
                    if value == Qt.Checked:
                        add_to_stock(item)
                    else:
                        remove_from_stock(item)
                    return True

        return False

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            elif orientation == Qt.Vertical:
                return section + 1

    def flags(self, index):
        flags = (Qt.ItemIsSelectable | Qt.ItemIsDropEnabled |
                 Qt.ItemIsEnabled)

        attr = self.columns[index.column()].name

        if index.row() < len(self.items):
            if (attr not in ("total", "added_to_stock", "received") and not self.order.locked):
                flags |= Qt.ItemIsEditable

            if attr in ("received", "added_to_stock"):
                flags |= Qt.ItemIsUserCheckable

        if (index.row() == len(self.items) + ROW_SHIPPING and
                attr == "total" and not self.order.locked):
            flags |= Qt.ItemIsEditable

        return flags

    def dropMimeData(self, data, action, row, column, parent=QModelIndex()):
        if action == Qt.IgnoreAction:
            return True

        # check mime format
        if data.hasFormat("application/x-list-of-articlenr"):
            # decode mimedata articles
            enc_data = data.data("application/x-list-of-articlenr")
            stream = QDataStream(enc_data, QIODevice.ReadOnly)
            articlenumbers = []
            while not stream.atEnd():
                articlenumbers.append(stream.readInt())
            last_position = len(self.items)

            # filter articles from database
            articles = session.query(db.Article)\
                .filter(db.Article.id.in_(articlenumbers))

            # add dropped articles to order
            for i, article in enumerate(articles):
                catalogitem = None
                for item in article.catalog_items:
                    if item.distributor is None:
                        break
                    if self.order.distributor is None:
                        self.order.distributor_id = item.distributor.id
                        break
                    if item.distributor.id == self.order.distributor.id:
                        catalogitem = item
                        break

                orderitem = db.OrderItem(
                    order=self.order,
                    article=article,
                    name=article.name,
                    manufacturer=article.manufacturer,
                    manufacturer_nr=article.article_nr,
                    position=last_position + i,
                )

                if catalogitem is not None:
                    orderitem.catalog_item = catalogitem
                    orderitem.articlenr = catalogitem.order_number
                    orderitem.distributor = catalogitem.distributor
                    orderitem.ppu = catalogitem.price
                    orderitem.packing_unit = catalogitem.packing_unit

                logger.debug("%s position: %i" %
                             (orderitem.name, orderitem.position))

                session.add(orderitem)

            session.commit()
            self.beginResetModel()
            self.items = self.order.items
            self.endResetModel()
            return True

        # dropped material items
        if data.hasFormat('application/x-list-of-material'):
            enc_data = data.data("application/x-list-of-material")
            stream = QDataStream(enc_data, QIODevice.ReadOnly)
            last_pos = len(self.items)

            # get all ids
            material_ids = []
            while not stream.atEnd():
                material_ids.append(stream.readInt())

            # create list of materials
            materiallist = (session.query(db.MaterialItem)
                            .filter(db.MaterialItem.id.in_(material_ids)))

            # for each material add an order item
            order_difference_to_stock = None
            for i, material in enumerate(materiallist):

                if material.ordered:
                    QMessageBox.information(
                        self.parent(), QApplication.applicationName(),
                        self.tr(f'The item "{material.article.name}" has '
                                'already been ordered and will not be added.'),
                        buttons=QMessageBox.Ok
                    )
                    continue

                # check if article is already in order
                article = material.article
                quantity = material.quantity

                try:
                    if article.stock.quantity > 0:
                        if order_difference_to_stock is None:
                            res = QMessageBox.question(
                                self.parent(), QApplication.applicationName(),
                                self.tr('Order difference to stock only?'),
                                buttons=QMessageBox.Yes | QMessageBox.No
                            )
                            order_difference_to_stock = res == QMessageBox.Yes

                        if order_difference_to_stock:
                            quantity = max(0, quantity - article.stock.quantity)
                            if quantity == 0:
                                continue

                except AttributeError:
                    pass


                order_item = next((x for x in self.order.items
                                   if x.article == article), None)

                if order_item is not None:
                    res = QMessageBox.question(
                        self.parent(), self.tr('Warning'),
                        self.tr(f'There is already an article "{article.name}" in '
                                f'the order "{self.order.name}". Do you want to add '
                                f'{quantity} items to this order?'),
                        buttons=QMessageBox.Yes | QMessageBox.No
                    )

                    if res == QMessageBox.Yes:
                        order_item.count += quantity
                        material.ordered = True
                        session.add(order_item)
                        session.add(material)

                    continue

                order_item = db.OrderItem(
                    order=self.order,
                    article=article,
                    name=article.name,
                    manufacturer=article.manufacturer,
                    manufacturer_nr=article.article_nr,
                    position=last_pos + i,
                    count=quantity
                )

                # Try to find the distributor for the given article
                # and set the properties according
                try:
                    catalog_item = next(item for item in article.catalog_items
                                        if item.distributor is self.order.distributor)
                    order_item.distributor = catalog_item.distributor
                    order_item.articlenr = catalog_item.order_number
                    order_item.ppu = catalog_item.price
                    order_item.packing_unit = catalog_item.packing_unit
                except StopIteration:
                    pass

                session.add(order_item)
                material.order_item = order_item
                material.ordered = True
                session.add(material)

            session.commit()
            self.beginResetModel()
            self.items = self.order.items
            self.endResetModel()
            return True

        return False

    def supportedDropActions(self):
        return Qt.CopyAction if not self.order.locked else None

    def mimeTypes(self):
        return [
            "application/x-list-of-articlenr",
            "application/x-list-of-material"
        ]

    def mimeData(self, indexes):
        mimedata = QMimeData()
        enc_data = QByteArray()
        stream = QDataStream(enc_data, QIODevice.WriteOnly)
        rows = []
        for index in indexes:
            if index.isValid():
                if index.row() not in rows:
                    rows.append(index.row())
                    orderitem = self.data(index, Qt.UserRole)
                    stream.writeInt(orderitem.id)
        mimedata.setData("application/x-list-of-orderitems", enc_data)
        return mimedata

    def column_by_name(self, name):
        names = [c.name for c in self.columns]
        return names.index(name)

    def refresh(self):
        session.refresh(self.order)
        res = session.query(db.OrderItem) \
            .filter(db.OrderItem.order == self.order) \
            .order_by(db.OrderItem.position)
        self.items = list(res)

    def sort(self, column: int, order: Qt.SortOrder=Qt.AscendingOrder):
        self.beginResetModel()
        if column == -1:
            key = lambda item: item.position
        else:
            attr = self.columns[column].name
            if attr == "distributor":
                key = lambda item: item.distributor.name
            elif attr == "total":
                key = lambda item: item.total()
            else:
                key = lambda item: getattr(item, attr)
        self.items = sorted(self.items, key=key,
                            reverse=order == Qt.DescendingOrder)
        self.endResetModel()

    def supportedDragActions(self):
        return Qt.MoveAction


class OrderModel(QAbstractTableModel):

    def __init__(self, order: db.Order, parent=None):
        super().__init__(parent)
        self.order = order
        self.dirty = False

        self.columns = [
            Column("date", self.tr("date")),
            Column("number", self.tr("number")),
            Column("name", self.tr("name")),
            Column("state", self.tr("state")),
            Column("distributor", self.tr("distributor")),
            Column("notes", self.tr("notes")),
            Column("shipping_costs", self.tr("shipping costs")),
            Column("project", self.tr("project"))
        ]

    def rowCount(self, parent):
        return 1

    def columnCount(self, parent):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        attr = self.columns[index.column()].name

        if role in (Qt.DisplayRole, Qt.EditRole):
            if attr == "date":
                if self.order.date is None:
                    return
                s = self.order.date.strftime("%d.%m.%Y")
                return QDate.fromString(s, "dd.MM.yyyy")

            elif attr == "distributor":
                if self.order.distributor is None:
                    return
                return self.order.distributor.name

            elif attr == "project":
                if self.order.project is None:
                    return None
                return project_label(self.order.project)

            elif attr == "state":
                state = self.order.state
                if state is None:
                    return -1
                else:
                    return state

            else:
                return getattr(self.order, attr)

        elif role == Qt.UserRole:
            return self.order

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        attr = self.columns[index.column()].name

        if role == Qt.EditRole:
            if attr == "date":
                self.order.date = value.toPyDate()

            elif attr == "distributor":
                self.order.distributor = value

            elif attr == "state":
                self.order.state = value

            elif attr == "project":
                self.order.project = value

            else:
                setattr(self.order, attr, value)

            self.dirty = True
            return True

        return False

    def headerData(self, p_int, Qt_Orientation, int_role=None):
        pass

    def flags(self, QModelIndex):
        pass

    def set_orderitems(self, items: [db.OrderItem]):
        self.beginResetModel()
        self.items = items
        self.endResetModel()

    def supportedDragActions(self):
        return Qt.MoveAction


class ItemsTableView(QTableView):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.orderWidget = parent

        self.disableSortAction = create_action(
            self, self.tr("Reset sorting"), slot=self.disable_sort
        )

        self.column_actions = []

        self.setDragEnabled(True)
        self.horizontalHeader().sectionClicked.connect(self.enable_sort)
        self.horizontalHeader().setContextMenuPolicy(Qt.CustomContextMenu)
        self.horizontalHeader().customContextMenuRequested.connect(
            self.show_contextmenu)

    def enable_sort(self, section):
        self.setSortingEnabled(True)

    def disable_sort(self):
        self.setSortingEnabled(False)
        self.model().sort(-1)

    def show_contextmenu(self, pos: QPoint):
        menu = QMenu(self)

        if self.isSortingEnabled():

            # disable sorting
            menu.addAction(self.disableSortAction)

            if self.column_actions:
                menu.addSeparator()

        # actions for hiding columns
        for action in self.column_actions:
            menu.addAction(action)

        globalpos = self.horizontalHeader().mapToGlobal(pos)
        menu.popup(globalpos)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_C and event.modifiers() & Qt.ControlModifier:
            self.orderWidget.copy()


class NotesLineEdit(QPlainTextEdit):

    def sizeHint(self):
        metrics = QFontMetrics(self.font())
        row_height = metrics.lineSpacing()
        return QSize(0, 5 * row_height)


class OrderWidget(QWidget):

    def __init__(self, mainwindow, order_id: int=None, parent=None):
        super().__init__(parent)
        self.mainwindow = mainwindow

        # set window title
        if order_id is None:
            self.order = db.Order(
                date=datetime.now(), name=self.tr("new order"))
            session.add(self.order)
            self.setWindowTitle(self.order.name)
        else:
            orders = session.query(
                db.Order).filter(db.Order.id == order_id)
            self.order = orders.one()
            self.setWindowTitle(self.order.name)

        # date
        self.dateLabel = QLabel(self.tr("date:"))
        self.dateEdit = QDateEdit()
        self.dateEdit.setSizePolicy(QSizePolicy.MinimumExpanding,
                                    QSizePolicy.Preferred)
        self.dateLabel.setBuddy(self.dateEdit)

        # number
        self.numberLabel = QLabel(self.tr("order number:"))
        self.numberLineEdit = QLineEdit()
        self.numberLabel.setBuddy(self.numberLineEdit)

        # name
        self.nameLabel = QLabel(self.tr("name:"))
        self.nameLineEdit = QLineEdit()
        self.nameLabel.setBuddy(self.nameLineEdit)
        self.nameLineEdit.setAcceptDrops(True)
        self.nameLineEdit.installEventFilter(self)

        # state
        self.stateLabel = QLabel(self.tr("state:"))
        self.stateComboBox = QComboBox()
        self.stateLabel.setBuddy(self.stateComboBox)
        self.stateComboBox.addItem(self.tr("draft"), ORDERSTATE_DRAFT)
        self.stateComboBox.addItem(self.tr("ordered"), ORDERSTATE_ORDERED)
        self.stateComboBox.addItem(self.tr("received"), ORDERSTATE_RECEIVED)

        # project
        self.projectLabel = QLabel(self.tr("project:"))
        self.projectComboBox = QComboBox()
        self.projectLabel.setBuddy(self.projectComboBox)
        self.projectComboBox.addItem("-", None)
        for p in session.query(db.Project).order_by(db.Project.number):
            self.projectComboBox.addItem(project_label(p), p)

        # distributor
        self.distributorLabel = QLabel(self.tr("distributor:"))
        self.distributorComboBox = QComboBox()
        self.distributorLabel.setBuddy(self.distributorComboBox)
        self.distributorComboBox.setEditable(True)

        for distributor in session.query(
                db.Distributor).order_by(db.Distributor.name):
            self.distributorComboBox.addItem(distributor.name, distributor)

        # notes
        self.notesTextEdit = NotesLineEdit()
        self.notesLabel = QLabel(self.tr("notes:"))
        self.notesLabel.setBuddy(self.notesTextEdit)

        # documents
        self.documentsListView = DocumentsWidget(ORDERDIR, str(self.order.id))

        # mapper and order model
        self.orderMapper = QDataWidgetMapper()
        self.orderModel = OrderModel(self.order, self)
        self.orderMapper.setModel(self.orderModel)
        self.orderMapper.setItemDelegate(OrderDelegate())
        self.orderMapper.addMapping(self.dateEdit, 0)
        self.orderMapper.addMapping(self.numberLineEdit, 1)
        self.orderMapper.addMapping(self.nameLineEdit, 2)
        self.orderMapper.addMapping(self.stateComboBox, 3)
        self.orderMapper.addMapping(self.distributorComboBox, 4)
        self.orderMapper.addMapping(self.notesTextEdit, 5)
        self.orderMapper.addMapping(self.projectComboBox, 7)
        self.orderMapper.toFirst()

        # tableview and itemsModel
        self.tableview = ItemsTableView(self)
        self.itemsModel = OrderItemsModel(self.order)
        self.tableview.setModel(self.itemsModel)
        self.tableview.setSelectionMode(QAbstractItemView.ContiguousSelection)
        self.tableview.setItemDelegate(OrderItemDelegate())
        self.tableview.viewport().setAcceptDrops(True)
        self.tableview.setSortingEnabled(False)
        self.tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableview.customContextMenuRequested.connect(
            self.show_contextmenu)
        self.tableview.installEventFilter(self)

        # details groupbox
        detailsGroupBox = QGroupBox(self.tr("details"))
        layout = QGridLayout()
        layout.addWidget(self.nameLabel, 0, 0)
        layout.addWidget(self.nameLineEdit, 0, 1, 1, 3)
        layout.addWidget(self.projectLabel, 0, 4)
        layout.addWidget(self.projectComboBox, 0, 5, 1, 3)
        layout.addWidget(self.numberLabel, 1, 0, alignment=Qt.AlignRight)
        layout.addWidget(self.numberLineEdit, 1, 1, alignment=Qt.AlignRight)
        layout.addWidget(self.distributorLabel, 1, 2)
        layout.addWidget(self.distributorComboBox, 1, 3)
        layout.addWidget(self.dateLabel, 1, 4)
        layout.addWidget(self.dateEdit, 1, 5)
        layout.addWidget(self.stateLabel, 1, 6)
        layout.addWidget(self.stateComboBox, 1, 7)
        layout.addWidget(self.notesLabel, 2, 0)
        layout.addWidget(self.notesTextEdit, 2, 1, 1, 7)
        detailsGroupBox.setLayout(layout)

        # documents groupbox
        documentsGroupBox = QGroupBox(self.tr("documents"))
        layout = QVBoxLayout()
        layout.addWidget(self.documentsListView)
        documentsGroupBox.setLayout(layout)

        # splitter1
        self.splitter1 = QSplitter(Qt.Horizontal)
        self.splitter1.addWidget(detailsGroupBox)
        self.splitter1.addWidget(documentsGroupBox)
        self.splitter1.setChildrenCollapsible(False)

        # articlelist Groupbox
        articlelistGroupBox = QGroupBox(self.tr("articlelist"))
        layout = QGridLayout()
        layout.addWidget(self.tableview, 0, 0, 1, 5)
        layout.setColumnStretch(0, 1)
        articlelistGroupBox.setLayout(layout)

        # splitter2
        self.splitter2 = QSplitter(Qt.Vertical)
        self.splitter2.addWidget(self.splitter1)
        self.splitter2.addWidget(articlelistGroupBox)
        self.splitter2.setStretchFactor(1, 5)

        # Layout
        layout = QVBoxLayout()
        layout.addWidget(self.splitter2)
        self.setLayout(layout)

        self._init_actions()
        self.tableview.resizeColumnsToContents()
        self.set_lockstate(self.order.locked)
        self.load_settings()
        self.nameLineEdit.selectAll()
        self.stateComboBox.currentIndexChanged.connect(self.state_changed)
        self.prev_order_state = self.order.state

    def _init_actions(self):

        # add article
        self.addArticleAction = create_action(
            self, self.tr("add article"), shortcut="Ctrl++",
            image=":icons/add.png", slot=self.add_article
        )

        # remove article
        self.removeArticleAction = create_action(
            self, self.tr("remove article"), shortcut="Ctrl+-",
            image=":icons/remove.png", slot=self.remove_article
        )

        # open article
        self.openArticleAction = create_action(
            self, self.tr('open article...'), image=":icons/edit.png",
            slot=self.open_article
        )

        # find article
        self.findArticleAction = create_action(
            self, self.tr("Find article in catalog"),
            image=":icons/find.png", slot=self.find_article
        )

        # update from catalog
        self.updateArticleAction = create_action(
            self, self.tr('Update from catalog'),
            image=":icons/reload.png", slot=self.update_article_from_catalog
        )

        # move up
        self.moveUpAction = create_action(
            self, self.tr("Move up"),
            image=":icons/go-up.png", slot=self.move_up
        )

        # move down
        self.moveDownAction = create_action(
            self, self.tr("Move down"),
            image=":icons/go-down.png", slot=self.move_down
        )

        # copy
        self.copyAction = create_action(
            self, self.tr("copy"), shortcut="Ctrl+C",
            image=":icons/editcopy.png", slot=self.copy
        )

        # columns
        self.tableview.column_actions = create_column_actions(
            self, self.itemsModel.columns, self.toggle_column)

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDER)

        # header state
        try:
            self.tableview.horizontalHeader().restoreState(
                settings.value(S_HEADER_STATE))
        except TypeError as e:
            self.tableview.resizeColumnsToContents()
            logger.warning(e)

        # hidden columns
        hidden_columns = settings.value(S_HIDDEN_COLUMNS)
        try:
            if hidden_columns is not None:
                for i, column in enumerate(self.itemsModel.columns):
                    if column.name in hidden_columns:
                        self.tableview.column_actions[i].setChecked(False)
                        self.tableview.setColumnHidden(i, True)
        except TypeError as e:
            logger.warning(e)

        # splitter1 state
        try:
            self.splitter1.restoreState(settings.value(S_SPLITTER1_STATE))
        except TypeError as e:
            logger.warning(e)

        # splitter2 state
        try:
            self.splitter2.restoreState(settings.value(S_SPLITTER2_STATE))
        except TypeError as e:
            logger.warning(e)

        # splitter2 state
        settings.setValue(S_SPLITTER2_STATE, self.splitter2.saveState())

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDER)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.tableview.horizontalHeader().saveState())

        # hidden columns
        settings.setValue(
            S_HIDDEN_COLUMNS,
            [c.data().name for c in self.tableview.column_actions
             if not c.isChecked()]
        )

        # splitter1 state
        settings.setValue(S_SPLITTER1_STATE, self.splitter1.saveState())

        # splitter2 state
        settings.setValue(S_SPLITTER2_STATE, self.splitter2.saveState())

    def add_article(self):
        self.itemsModel.beginResetModel()
        count = len(self.itemsModel.items)
        orderitem = db.OrderItem(order=self.order, position=count)
        logger.debug("item position: %i" % orderitem.position)
        session.add(orderitem)
        self.itemsModel.refresh()
        self.itemsModel.endResetModel()

    def remove_article(self):
        items = set()
        for index in self.tableview.selectedIndexes():
            if not index.isValid():
                continue
            item = self.itemsModel.items[index.row()]
            if isinstance(item, db.OrderItem):
                items.add(item)

        self.itemsModel.beginResetModel()
        for item in items:
            session.delete(item)
        session.commit()
        self.itemsModel.refresh()
        self.itemsModel.endResetModel()

    def eventFilter(self, obj: QObject, event: QEvent):
        # nameLineEdit
        if obj == self.nameLineEdit:
            if event.type() == QEvent.FocusIn:
                self.nameLineEdit.selectAll()
                return True

        # tableview
        elif obj == self.tableview:
            if event.type() == QEvent.KeyPress:
                if (event.key() == Qt.Key_Up and event.modifiers() == Qt.ControlModifier):
                    self.move_up()
                    return True

                if (event.key() == Qt.Key_Down and event.modifiers() == Qt.ControlModifier):
                    self.move_down()
                    return True

        return False

    def copy(self):

        SEPARATOR = "\t"
        NEWLINE = "\n"

        def iter_data():
            previous = selected[0]
            for index in selected:
                if index != previous:
                    yield (SEPARATOR if index.row() == previous.row()
                           else NEWLINE)
                yield str(model.data(index) or "")
                previous = index

        model = self.tableview.model()
        selected = self.tableview.selectedIndexes()

        # copy headers if full column is selected
        columns = [True] * model.columnCount()
        for i in range(model.rowCount()):
            for j in range(model.columnCount()):
                index = model.index(i, j, QModelIndex())
                if index not in selected:
                    columns[j] = False

        # add column names
        text = ""
        if any(columns):
            text += (SEPARATOR.join(
                [model.headerData(i, Qt.Horizontal) if c else ""
                 for i, c in enumerate(columns)]) + NEWLINE
            )

        text += "".join(iter_data())
        QApplication.clipboard().setText(text)

    @property
    def dirty(self):
        return self.orderModel.dirty or self.itemsModel.dirty

    @dirty.setter
    def dirty(self, value):
        self.orderModel.dirty = value
        self.itemsModel.dirty = value

    def closeEvent(self, event):
        self.save_settings()
        # session.commit()

    def find_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        item = self.itemsModel.data(index, Qt.UserRole)
        if not isinstance(item, db.OrderItem):
            return
        article = item.article
        if article is None:
            return
        self.mainwindow.catalogWidget.find_article(article)

    def move_up(self):
        # allow moving only if sorting is not enabled
        if self.tableview.isSortingEnabled():
            return

        index = self.tableview.currentIndex()
        if index.isValid():

            item = self.itemsModel.data(index, Qt.UserRole)
            prev_index = self.itemsModel.index(
                index.row() - 1, index.column(), index.parent())

            # if no item above leave
            if not prev_index.isValid():
                return

            prev_item = self.itemsModel.data(prev_index, Qt.UserRole)

            # swap positions
            prev_item.position += 1
            item.position -= 1

            # refresh model
            self.itemsModel.beginMoveRows(
                index.parent(), index.row(), index.row(),
                prev_index.parent(), prev_index.row()
            )
            self.itemsModel.refresh()
            self.itemsModel.endMoveRows()

    def move_down(self):
        # allow moving only if sorting is not enabled
        if self.tableview.isSortingEnabled():
            return

        index = self.tableview.currentIndex()
        if index.isValid():

            item = self.itemsModel.data(index, Qt.UserRole)
            next_index = self.itemsModel.index(
                index.row() + 1, index.column(), index.parent())

            # return if no next item
            if not next_index.isValid():
                return
            next_item = self.itemsModel.data(next_index, Qt.UserRole)

            # swap positions
            next_item.position -= 1
            item.position += 1

            # refresh model
            self.itemsModel.beginMoveRows(
                index.parent(), index.row(), index.row(),
                next_index.parent(), next_index.row() + 1
            )
            self.itemsModel.refresh()
            self.itemsModel.endMoveRows()

    def open_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        order_item = self.itemsModel.items[index.row()]
        self.mainwindow.open_article(order_item.article)

    def set_lockstate(self, locked: bool=False):
        self.addArticleAction.setEnabled(not locked)
        self.removeArticleAction.setEnabled(not locked)
        self.tableview.setAcceptDrops(not locked)
        # self.tableview.setDragEnabled(not locked)

    def state_changed(self, index):
        if (self.prev_order_state == ORDERSTATE_RECEIVED and index != ORDERSTATE_RECEIVED):
            res = QMessageBox.question(
                self.parent(), QApplication.applicationName(),
                self.tr("Do you want to remove all articles from stock?")
            )

            if res == QMessageBox.Yes:
                items = self.itemsModel.items
                i = 0
                for item in items:
                    item.received = False
                    remove_from_stock(item)
                    i += 1

                session.commit()
                logger.debug("Removed {} items from stock.".format(i))

        if index == ORDERSTATE_ORDERED:
            d = datetime.now()

            res = QMessageBox.question(
                self.parent(), QApplication.applicationName(),
                self.tr("Do you want to set the order date to {:%d.%m.%Y}?")
                .format(d)
            )

            if res == QMessageBox.Yes:
                column = [c[0] for c in self.orderModel.columns].index("date")
                index = self.orderModel.index(
                    self.orderMapper.currentIndex(),
                    column
                )
                self.orderModel.setData(index, QDate(d))
                self.orderModel.dataChanged.emit(
                    index, index, [Qt.DisplayRole])

        elif index == ORDERSTATE_RECEIVED:

            res = QMessageBox.question(
                self.parent(), QApplication.applicationName(),
                self.tr("Do you want to add all articles to stock?")
            )

            if res == QMessageBox.Yes:
                items = self.itemsModel.items
                added = 0
                for i, item in enumerate(items):
                    item.received = True
                    if add_to_stock(item):
                        added += 1

                session.commit()
                logger.debug("Added {} items to stock.".format(added))
                if i - added:
                    logger.debug("Missed {} items:".format(i - added))

        self.prev_order_state = self.order.state

    def save(self):
        self.orderMapper.submit()
        session.commit()
        self.dirty = False

    def show_contextmenu(self, pos: QPoint):
        menu = QMenu(self)
        index = self.tableview.currentIndex()

        # copy to clipboard
        if index.isValid():
            menu.addAction(self.copyAction)
            menu.addSeparator()

        # add article
        menu.addAction(self.addArticleAction)

        if index.isValid():
            # remove article
            menu.addAction(self.removeArticleAction)
            menu.addSeparator()

            try:
                order_item = self.itemsModel.items[index.row()]
            except IndexError:
                order_item = None

            b = order_item is not None and order_item.article is not None
            self.openArticleAction.setEnabled(b)
            self.findArticleAction.setEnabled(b)

            # open article
            menu.addAction(self.openArticleAction)
            # find article
            menu.addAction(self.findArticleAction)

            # update article
            if order_item is not None and order_item.catalog_item is not None:
                menu.addAction(self.updateArticleAction)

            menu.addSeparator()

            item = self.itemsModel.data(index, Qt.UserRole)
            if not self.tableview.isSortingEnabled():
                # move up
                menu.addAction(self.moveUpAction)
                # move down
                menu.addAction(self.moveDownAction)
                try:
                    if item.position is not None:
                        self.moveUpAction.setEnabled(item.position > 0)
                        self.moveDownAction.setEnabled(
                            item.position < len(self.itemsModel.items) - 1)
                except AttributeError:
                    pass
            menu.setDefaultAction(self.openArticleAction)

        globalpos = self.tableview.viewport().mapToGlobal(pos)
        menu.popup(globalpos)

    def toggle_column(self):
        for action in self.tableview.column_actions:
            column = action.data()
            index = self.itemsModel.columns.index(column)
            self.tableview.setColumnHidden(index, not action.isChecked())

    def refresh(self):
        self.itemsModel.refresh()

    def update_article_from_catalog(self):
        indexes = self.tableview.selectedIndexes()
        rows = set()
        for index in indexes:
            if index.isValid():
                rows.add(index.row())

        for row in rows:
            order_item = self.itemsModel.items[row]
            article = order_item.article
            catalog_item = order_item.catalog_item

            if article is None:
                order_item.manufacturer = article.manufacturer
                order_item.manufacturer_nr = article.article_nr

            if catalog_item is not None:
                order_item.articlenr = catalog_item.order_number
                order_item.distributor = catalog_item.distributor
                order_item.ppu = catalog_item.price
                order_item.packing_unit = catalog_item.packing_unit

        db.session.commit()
