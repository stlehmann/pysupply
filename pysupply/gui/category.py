from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
from PyQt5.QtWidgets import QDialog, QGridLayout, QLabel, QLineEdit, \
    QComboBox, QDialogButtonBox, QDataWidgetMapper

from pysupply.db import Session, Category


session = Session()


def get_column(name):
    return Category.__table__.columns.keys().index(name)


class CategoryModel(QAbstractTableModel):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.categories = [c for c in session.query(Category)]

    def rowCount(self, parent=QModelIndex()):
        return len(self.categories)

    def columnCount(self, parent=QModelIndex()):
        return len(Category.__table__.columns.keys())

    def data(self, index=QModelIndex(), role=Qt.DisplayRole):
        if not index.isValid():
            return
        category = self.categories[index.row()]
        key = Category.__table__.columns.keys()[index.column()]
        if role == Qt.EditRole:
            return getattr(category, key)

    def setData(self, index=QModelIndex(), value=None, role=Qt.EditRole):
        pass


class CategoryDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)

        # category name
        self.nameLabel = QLabel(self.tr("name:"))
        self.nameLineEdit = QLineEdit()

        # parent category
        self.parentLabel = QLabel(self.tr("parent category:"))
        self.parentComboBox = QComboBox()

        # button box
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok |
            QDialogButtonBox.Cancel)

        # mapper
        self.mapper = QDataWidgetMapper()
        self.mapper.setModel(CategoryModel())
        self.mapper.addMapping(self.nameLineEdit, get_column("name"))
        self.mapper.addMapping(self.parentComboBox, get_column("parent_id"))
        self.mapper.toFirst()

        # layout
        layout = QGridLayout()
        layout.addWidget(self.nameLabel, 0, 0)
        layout.addWidget(self.nameLineEdit, 0, 1)
        layout.addWidget(self.parentLabel, 1, 0)
        layout.addWidget(self.parentComboBox, 1, 1)
        self.setLayout(layout)
