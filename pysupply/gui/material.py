"""
material.py,

copyright (c) 2015 by Stefan Lehmann,
licensed under the MIT license

"""
import sys
import logging

from PyQt5.QtGui import QColor, QBrush
from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex, QDataStream, \
    QIODevice, QSettings, QMimeData, QByteArray
from PyQt5.QtWidgets import QWidget, QTableView, QVBoxLayout, QMenu, QLabel, \
    QLineEdit, QPlainTextEdit, QGridLayout, QSplitter, QDataWidgetMapper, \
    QItemDelegate, QComboBox, QSpinBox, QMessageBox

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.helper import Column, create_action
from .dialogs import OrderDialog


S_GRP_MATERIAL = "pysupply/gui/material"
S_HEADER_STATE = "header_state"
S_SPLITTER_STATE = "splitter_state"


logger = logging.getLogger("pysupply.gui.material")


class MaterialListModel(QAbstractTableModel):

    columns = [
        'name',
        'comment'
    ]

    def __init__(self, materiallist, parent=None):
        super().__init__(parent)
        self.materiallist = materiallist

    def rowCount(self, index):
        return 1

    def columnCount(self, index):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        attr = self.columns[index.column()]

        if role in (Qt.DisplayRole, Qt.EditRole):
            return getattr(self.materiallist, attr)

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return

        attr = self.columns[index.column()]

        if role == Qt.EditRole:
            setattr(self.materiallist, attr, value)
            session.commit()
            return True

        return False


class MaterialItemsDelegate(QItemDelegate):

    def createEditor(self, parent, option, index):
        if not index.isValid():
            return

        attr = index.model().columns[index.column()].name

        if attr in ('removed_from_stock', 'ordered'):
            editor = QComboBox(parent)
            editor.addItem(self.tr('Yes'), True)
            editor.addItem(self.tr('No'), False)
            editor.setFocusPolicy(Qt.StrongFocus)
            editor.setAutoFillBackground(True)
            return editor
        elif attr == 'quantity':
            editor = QSpinBox(parent)
            editor.setRange(1, sys.maxsize)
            editor.lineEdit().setAlignment(Qt.AlignRight)
            return editor

        return super().createEditor(parent, option, index)

    def setEditorData(self, editor, index):
        attr = index.model().columns[index.column()].name
        if attr in ('removed_from_stock', 'ordered'):
            value = index.model().data(index, Qt.DisplayRole)
            index = editor.findText(value)
            editor.setCurrentIndex(index)
        elif attr == 'quantity':
            value = index.model().data(index, Qt.DisplayRole)
            editor.setValue(value)
        else:
            return super().setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        attr = index.model().columns[index.column()].name
        if attr in ('removed_from_stock', 'ordered'):
            model.setData(index, editor.currentData(), Qt.EditRole)
        elif attr == 'quantity':
            model.setData(index, editor.value(), Qt.EditRole)
        else:
            return super().setModelData(editor, model, index)


class MaterialItemsModel(QAbstractTableModel):

    def __init__(self, materiallist, parent=None):
        super().__init__(parent)
        self.materiallist = materiallist
        self.columns = [
            Column("name", self.tr("name")),
            Column("manufacturer", self.tr("manufacturer")),
            Column("article_nr", self.tr("article nr.")),
            Column("quantity", self.tr("quantity")),
            Column("stock", self.tr("stock")),
            Column("removed_from_stock", self.tr("removed from stock")),
            Column("ordered", self.tr("ordered")),
            Column("comment", self.tr("comment"))
        ]
        self.refresh()

    def rowCount(self, index):
        return len(self.items)

    def columnCount(self, index):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        item = self.items[index.row()]
        attr = self.columns[index.column()].name

        if role in (Qt.DisplayRole, Qt.EditRole):
            try:
                if attr == "name":
                    return item.article.name
                elif attr == "manufacturer":
                    return item.article.manufacturer
                elif attr == "article_nr":
                    return item.article.article_nr
                elif attr == "stock":
                    try:
                        return item.article.stock.quantity
                    except AttributeError:
                        return "-"
                elif attr == "removed_from_stock":
                    return self.tr('Yes') if item.removed_from_stock else self.tr('No')

                elif attr == "ordered":
                    return self.tr('Yes') if item.ordered else self.tr('No')
            except AttributeError as e:
                logger.error(e)
                return ''

            return getattr(item, attr)

        elif role == Qt.TextAlignmentRole:
            if attr in ("quantity", "stock"):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.ForegroundRole:
            try:
                if item.article.stock is None:
                    return QBrush(QColor(Qt.red))

                if item.article.stock.quantity < item.article.stock.minimum:
                    if attr == 'stock':
                        return QBrush(QColor(Qt.red))

                if (item.quantity > item.article.stock.quantity and not
                        item.removed_from_stock):
                    return QBrush(QColor(Qt.red))

                elif item.removed_from_stock:
                    return QBrush(QColor(Qt.darkGreen))
            except (AttributeError, TypeError) as e:
                logger.error(e)


        elif role == Qt.UserRole:
            return item

    def setData(self, index, value, role=Qt.EditRole):
        if not index.isValid():
            return False

        attr = self.columns[index.column()].name
        item = self.items[index.row()]

        if role == Qt.EditRole:
            if attr == 'comment':
                item.comment = value
                session.commit()
                return True

            if attr == 'quantity':
                item.quantity = value
                session.commit()
                return True

            if attr == 'removed_from_stock':
                if item.removed_from_stock == value:
                    return

                stock = item.article.stock
                if stock is None:
                    raise ValueError(
                        "Can not remove item from stock. Item not in stock"
                    )

                if value:
                    if item.quantity <= stock.quantity:
                        stock.quantity -= item.quantity
                        item.removed_from_stock = True
                    else:
                        raise ValueError(
                            "Not enough material in stock."
                        )
                else:
                    stock.quantity += item.quantity
                    item.removed_from_stock = False

                session.commit()

                update_column = [x.name for x in self.columns].index("stock")
                update_index = self.index(index.row(), update_column)
                self.dataChanged.emit(
                    update_index, update_index, [Qt.DisplayRole]
                )
                return True

            if attr == 'ordered':
                item.ordered = value
                session.commit()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            else:
                return section + 1

    def supportedDropActions(self):
        return Qt.CopyAction

    def mimeTypes(self):
        return ["application/x-list-of-articlenr"]

    def mimeData(self, indexes):
        mimedata = QMimeData()
        enc_data = QByteArray()
        stream = QDataStream(enc_data, QIODevice.WriteOnly)

        # get all selected items
        items = set()
        for index in indexes:
            if not index.isValid():
                continue
            items.add(self.items[index.row()])

        for item in items:
            stream.writeInt(item.id)
        mimedata.setData('application/x-list-of-material', enc_data)
        return mimedata

    def dropMimeData(self, data, action, row, column, parent=QModelIndex()):
        if action == Qt.IgnoreAction:
            return True
        if not data.hasFormat("application/x-list-of-articlenr"):
            return False

        enc = data.data("application/x-list-of-articlenr")
        stream = QDataStream(enc, QIODevice.ReadOnly)

        # decode article numbers
        article_ids = []
        while not stream.atEnd():
            article_ids.append(stream.readInt())

        # get articles from database
        articles = (session.query(db.Article)
                    .filter(db.Article.id.in_(article_ids)))

        for i, article in enumerate(articles):
            material = next((item for item in self.items
                             if item.article == article), None)
            if material is None:
                material = db.MaterialItem()
                material.quantity = 1
                material.article = article
                material.materiallist = self.materiallist
                session.add(material)
                session.commit()
            else:
                material.quantity += 1
                session.commit()

        self.refresh()
        return True

    def flags(self, index):
        flags = Qt.ItemIsEnabled | Qt.ItemIsDropEnabled | Qt.ItemIsSelectable

        if not index.isValid():
            return flags

        flags |= Qt.ItemIsDragEnabled
        item = self.items[index.row()]
        attr = self.columns[index.column()].name

        if (
                attr in ('removed_from_stock', 'comment', 'ordered')
                or (attr == 'quantity' and not item.removed_from_stock)
        ):
            flags |= Qt.ItemIsEditable
        return flags

    def supportedDragActions(self):
        return Qt.CopyAction

    def refresh(self):
        self.beginResetModel()
        self.items = list(
            session.query(db.MaterialItem).filter(
                db.MaterialItem.materiallist == self.materiallist)
        )
        self.endResetModel()


class MaterialWidget(QWidget):

    def __init__(self, materiallist, parent=None):
        super().__init__(parent)
        self.materiallist = materiallist
        self.mainwindow = parent
        self.items_model = MaterialItemsModel(materiallist, self)

        self._init_actions()

        # name
        self.nameLabel = QLabel(self.tr('name:'))
        self.nameLineEdit = QLineEdit()
        self.nameLabel.setBuddy(self.nameLineEdit)

        # comment
        self.commentLabel = QLabel(self.tr('comment:'))
        self.commentTextEdit = QPlainTextEdit()
        self.commentLabel.setBuddy(self.commentTextEdit)

        # headerWidget
        self.headerWidget = QWidget(self)
        layout = QGridLayout()
        layout.addWidget(self.nameLabel, 0, 0)
        layout.addWidget(self.nameLineEdit, 0, 1)
        layout.addWidget(self.commentLabel, 1, 0)
        layout.addWidget(self.commentTextEdit, 1, 1)
        self.headerWidget.setLayout(layout)

        # tableview
        self.tableview = QTableView()
        self.tableview.setModel(self.items_model)
        self.tableview.setItemDelegate(MaterialItemsDelegate())
        self.tableview.viewport().setAcceptDrops(True)
        self.tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableview.customContextMenuRequested.connect(
            self.show_contextmenu)
        self.tableview.setEditTriggers(QTableView.AllEditTriggers)
        self.tableview.doubleClicked.connect(self.open_article)
        self.tableview.setDragEnabled(True)

        # splitter
        self.splitter = QSplitter(self, orientation=Qt.Vertical)
        self.splitter.addWidget(self.headerWidget)
        self.splitter.addWidget(self.tableview)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.splitter)
        self.setLayout(layout)

        # data mapper
        self.mapper = QDataWidgetMapper(self)
        model = MaterialListModel(self.materiallist)
        self.mapper.setModel(model)
        self.mapper.addMapping(self.nameLineEdit, model.columns.index('name'))
        self.mapper.addMapping(
            self.commentTextEdit, model.columns.index('comment'))
        self.mapper.toFirst()

        # window title
        self.setWindowTitle(self.tr('Materialliste {}').format(
            self.materiallist.name))
        self.load_settings()

    def _init_actions(self):
        # find article
        self.findArticleAction = create_action(
            self, self.tr('find article'), slot=self.find_article)

        # open article
        self.openArticleAction = create_action(
            self, self.tr('open article...'), slot=self.open_article)

        # order
        self.orderItemAction = create_action(
            self, self.tr("add to order"), slot=self.order_items)

        # remove
        self.removeMaterialAction = create_action(
            self, self.tr("remove"), slot=self.remove_item)

        # remove from stock
        self.removeFromStockAction = create_action(
            self, self.tr('remove from stock'), slot=self.remove_from_stock)

        # add to stock
        self.addToStockAction = create_action(
            self, self.tr('add to stock'), slot=self.add_to_stock)

        # go to order item
        self.gotoOrderItemAction = create_action(
            self, self.tr('open order'), slot=self.open_order)

        # set ordered flag
        self.setOrderedFlagAction = create_action(
            self, self.tr('set ordered flag'), slot=self.set_ordered_flag)

        # reset ordered flag
        self.resetOrderedFlagAction = create_action(
            self, self.tr('reset ordered flag'), slot=self.reset_ordered_flag)

    def closeEvent(self, event):
        self.save_settings()
        super().closeEvent(event)

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_MATERIAL)

        # header state
        try:
            state = settings.value(S_HEADER_STATE, None)
            if state is not None:
                self.tableview.horizontalHeader().restoreState(state)
        except TypeError as e:
            logger.debug(e)

        # splitter state
        try:
            state = settings.value(S_SPLITTER_STATE, None)
            if state is not None:
                self.splitter.restoreState(state)
        except TypeError as e:
            logger.debug(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_MATERIAL)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.tableview.horizontalHeader().saveState())

        # splitter state
        settings.setValue(
            S_SPLITTER_STATE, self.splitter.saveState()
        )

    def show_contextmenu(self, pos):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        item = self.items_model.items[index.row()]

        menu = QMenu(self)
        menu.addAction(self.openArticleAction)
        menu.addAction(self.findArticleAction)
        menu.addSeparator()

        if not item.ordered:
            menu.addAction(self.orderItemAction)
        else:
            menu.addAction(self.gotoOrderItemAction)

        # check if item has been removed
        if not item.removed_from_stock:
            menu.addAction(self.removeFromStockAction)
        else:
            menu.addAction(self.addToStockAction)

        # chek if item has been ordered
        if item.ordered:
            menu.addAction(self.resetOrderedFlagAction)
        else:
            menu.addAction(self.setOrderedFlagAction)

        menu.addSeparator()
        menu.addAction(self.removeMaterialAction)
        menu.setDefaultAction(self.openArticleAction)

        global_pos = self.tableview.viewport().mapToGlobal(pos)
        menu.popup(global_pos)

    def order_items(self):
        items = self.get_selected_items()

        # open order with dialog
        dlg = OrderDialog()
        if dlg.exec_() == OrderDialog.Rejected:
            return
        order = dlg.current_order()

        for item in items:
            order.add_material(item)

        widget = self.mainwindow.find_orderWidget(order)
        if widget is not None:
            widget.refresh()
            self.mainwindow.mdi.setActiveSubWindow(widget.parent())

    def remove_item(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        material = self.items_model.data(index, Qt.UserRole)
        session.delete(material)
        self.items_model.refresh()

    def open_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        item = self.items_model.items[index.row()]
        self.mainwindow.open_article(item.article)

    def find_article(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        item = self.items_model.items[index.row()]
        self.mainwindow.catalogWidget.find_article(item.article)

    def get_selected_items(self):
        indexes = self.tableview.selectedIndexes()
        items = set()

        for index in indexes:
            item = self.items_model.items[index.row()]
            items.add(item)

        return list(items)

    def add_to_stock(self):
        for item in self.get_selected_items():
            item.return_to_stock()
        session.commit()

    def remove_from_stock(self):
        for item in self.get_selected_items():
            stock = item.article.stock
            if stock is None:
                QMessageBox.warning(
                    self, self.tr('Warning'),
                    self.tr('Can not remove article "{}" from stock. Article '
                            'is not listed in stock.')
                    .format(item.article.name)
                )
                return
            item.remove_from_stock()
        session.commit()

    def open_order(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return
        item = self.items_model.items[index.row()]
        if item is None:
            logger.warning('no material item found at current position')
            return
        order_item = item.order_item
        if order_item is None:
            logger.warning(
                'no order item is linked to material item {}'.format(item.id))
            return
        order = order_item.order
        if order is None:
            logger.warning(
                'no order found for order item {}'.format(order_item.id))
        self.mainwindow.open_order(order)

    def set_ordered_flag(self):
        for item in self.get_selected_items():
            item.ordered = True
        session.commit()

    def reset_ordered_flag(self):
        for item in self.get_selected_items():
            item.ordered = False
        session.commit()


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)
    materiallist = session.query(db.MaterialList).get(0)
    wgt = MaterialWidget(materiallist)
    wgt.show()
    app.exec_()
