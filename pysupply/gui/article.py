"""
article.py - GUI objects for article properties

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""
import logging

from PyQt5.QtCore import Qt, QModelIndex, QAbstractTableModel, QVariant, \
    QSettings
from PyQt5.QtWidgets import QDialog, QLabel, QLineEdit, QDataWidgetMapper, \
     QGridLayout, QPlainTextEdit, QComboBox, QItemDelegate, QDialogButtonBox, \
     QWidget, QVBoxLayout, QTabWidget, QSpinBox, QCheckBox, QGroupBox, \
     QMessageBox, QApplication
from sqlalchemy.exc import InvalidRequestError

from pysupply.gui.articledistr import ArticleDistributorsWidget
from pysupply.gui.categorybox import CategoryComboBox
from pysupply.gui.documents import DocumentsWidget, ARTICLEDIR
from pysupply.gui.properties import PropertyWidget
from pysupply.gui.helper import Column
from pysupply.config import MAX_INT
import pysupply.db as db
from pysupply.db import session


S_GRP_ARTICLE = "pysupply/gui/article"
S_GEOMETRY = "geometry"

logger = logging.getLogger("pysupply.gui.article")


def article_keys():
        from sqlalchemy import inspect
        mapper = inspect(db.Article)
        for column in mapper.attrs:
            yield column.key


class ArticleModel(QAbstractTableModel):

    def __init__(self, article, parent=None):
        super().__init__(parent)
        self.columns = [
            Column("name", self.tr("name")),
            Column("category", self.tr("category")),
            Column("manufacturer", self.tr("manufacturer")),
            Column("article_nr", self.tr("article nr.")),
            Column("notes", self.tr("notes")),
            Column("properties", self.tr("properties")),
            Column("stock_quantity", self.tr("stock quantity")),
            Column("stock_minimum", self.tr("stock minimum"))
        ]
        self.article = article

    def get_column(self, name):
        column_names = [c.name for c in self.columns]
        return column_names.index(name)

    def rowCount(self, parent=QModelIndex):
        return 1

    def columnCount(self, parent=QModelIndex):
        return len(self.columns)

    def data(self, index=QModelIndex, role=Qt.DisplayRole):

        if not index.isValid():
            return

        attr = self.columns[index.column()].name

        if role in (Qt.DisplayRole, Qt.EditRole):

            if attr == "category":
                if self.article.category is None:
                    return QVariant()
                else:
                    return self.article.category.name

            elif attr == "stock_quantity":
                if self.article.stock is None:
                    return QVariant()
                else:
                    return self.article.stock.quantity

            elif attr == "stock_minimum":
                if self.article.stock is None:
                    return QVariant()
                else:
                    return self.article.stock.minimum

            else:
                return getattr(self.article, attr)

        elif role == Qt.UserRole + 1:
            if attr == "category":
                return self.article.category.id

    def setData(self, index=QModelIndex, value=None, role=Qt.EditRole):

        if not index.isValid():
            return

        attr = self.columns[index.column()].name

        if role == Qt.EditRole:

            if attr == "stock_quantity":
                self.article.stock.quantity = value
                return True
            elif attr == "stock_minimum":
                self.article.stock.minimum = value
                return True
            else:
                setattr(self.article, attr, value)
                return True

        return False


class ArticleItemDelegate(QItemDelegate):

    def setEditorData(self, editor, index):

        if not index.isValid():
            return

        if isinstance(editor, QComboBox):
            category = index.data(Qt.UserRole + 1)
            model = editor.view().model()
            indexes = model.match(model.index(0, 0, QModelIndex()),
                                  Qt.UserRole + 1, category,
                                  1, Qt.MatchRecursive)
            if not indexes:
                return
            index = indexes[0]
            editor.view().setCurrentIndex(index)
            editor.setRootModelIndex(index.parent())
            editor.setCurrentIndex(index.row())
        else:
            QItemDelegate.setEditorData(self, editor, index)

    def setModelData(self, editor, model, index):
        if isinstance(editor, QComboBox):
            node = editor.currentData(Qt.UserRole)
            model.setData(index, node.category)
        else:
            QItemDelegate.setModelData(self, editor, model, index)


class ArticleWidget(QWidget):

    def __init__(self, article: db.Article, parent=None):
        super().__init__(parent)
        self.article = article

        # article name
        self.nameLabel = QLabel(self.tr("name:"))
        self.nameLineEdit = QLineEdit()

        # category
        self.categoryLabel = QLabel(self.tr("category:"))
        self.categoryComboBox = CategoryComboBox()

        # manufacturer
        self.manufacturerLabel = QLabel(self.tr("manufacturer:"))
        self.manufacturerLineEdit = QLineEdit()

        # article number
        self.articlenrLabel = QLabel(self.tr("article nr."))
        self.articlenrLineEdit = QLineEdit()
        self.articlenrLabel.setBuddy(self.articlenrLineEdit)

        # stock
        self.stockCheckBox = QCheckBox(self.tr("stock item"))
        self.stockCheckBox.stateChanged.connect(self.change_stock_state)

        # stock count
        self.stockLabel = QLabel(self.tr("quantity:"))
        self.stockSpinBox = QSpinBox()
        self.stockSpinBox.setRange(0, MAX_INT)
        self.stockLabel.setBuddy(self.stockSpinBox)

        # minimum stock count
        self.minstockLabel = QLabel(self.tr("minimum quantity:"))
        self.minstockSpinBox = QSpinBox()
        self.minstockSpinBox.setRange(0, MAX_INT)
        self.minstockLabel.setBuddy(self.minstockSpinBox)

        # stock widget
        self.stockGroupBox = QGroupBox(self.tr(""))
        layout = QGridLayout()
        layout.addWidget(self.stockLabel, 0, 1,)
        layout.addWidget(self.stockSpinBox, 0, 2)
        layout.addWidget(self.minstockLabel, 0, 3)
        layout.addWidget(self.minstockSpinBox, 0, 4)
        self.stockGroupBox.setLayout(layout)

        # notes
        self.notesTextEdit = QPlainTextEdit()

        # properties
        self.propertyWidget = PropertyWidget(article)

        # documents
        self.documentlist = DocumentsWidget(ARTICLEDIR, str(article.id))

        # suppliers
        self.distributors = ArticleDistributorsWidget(article)

        # tabwidget
        self.tabWidget = QTabWidget()
        self.tabWidget.addTab(self.documentlist, self.tr("documents"))
        self.tabWidget.addTab(self.propertyWidget, self.tr("properties"))
        self.tabWidget.addTab(self.notesTextEdit, self.tr("notes"))
        self.tabWidget.addTab(self.distributors, self.tr("distributors"))

        # mapper
        self.model = ArticleModel((article))
        self.mapper = QDataWidgetMapper()
        self.mapper.setModel(self.model)
        self.mapper.setItemDelegate(ArticleItemDelegate())
        self.mapper.addMapping(self.nameLineEdit,
                               self.model.get_column("name"))
        self.mapper.addMapping(self.categoryComboBox,
                               self.model.get_column("category"))
        self.mapper.addMapping(self.manufacturerLineEdit,
                               self.model.get_column("manufacturer"))
        self.mapper.addMapping(self.articlenrLineEdit,
                               self.model.get_column("article_nr"))
        self.mapper.addMapping(self.notesTextEdit,
                               self.model.get_column("notes"))
        self.mapper.addMapping(self.stockSpinBox,
                               self.model.get_column("stock_quantity"))
        self.mapper.addMapping(self.minstockSpinBox,
                               self.model.get_column("stock_minimum"))
        self.mapper.toFirst()

        # layout
        layout = QGridLayout()
        layout.addWidget(self.nameLabel, 0, 0)
        layout.addWidget(self.nameLineEdit, 0, 1)
        layout.addWidget(self.categoryLabel, 1, 0)
        layout.addWidget(self.categoryComboBox, 1, 1)
        layout.addWidget(self.manufacturerLabel, 2, 0)
        layout.addWidget(self.manufacturerLineEdit, 2, 1)
        layout.addWidget(self.articlenrLabel, 3, 0)
        layout.addWidget(self.articlenrLineEdit, 3, 1)
        layout.addWidget(self.stockCheckBox, 4, 0)
        layout.addWidget(self.stockGroupBox, 5, 0, 1, 2)
        layout.addWidget(self.tabWidget, 6, 0, 1, 2)
        self.setLayout(layout)

        # set stock group visibility
        self.stockCheckBox.setChecked(self.article.stock is not None)
        self.stockGroupBox.setVisible(self.stockCheckBox.isChecked())

    def change_stock_state(self, state):
        self.stockGroupBox.setVisible(state == Qt.Checked)

        if state == Qt.Checked:
            if self.article.stock is None:
                stock = db.StockItem()
                stock.quantity = 0
                stock.minimum = 0
                self.article.stock = stock

        elif state == Qt.Unchecked:
            res = QMessageBox.question(
                self, QApplication.applicationName(),
                self.tr("Remove article {0} from stock?")
                    .format(self.article.name),
            )
            if res == QMessageBox.Yes:
                stock_item = self.article.stock
                try:
                    session.delete(stock_item)
                except InvalidRequestError:
                    pass
                self.article.stock = None


class ArticleDialog(QDialog):

    def __init__(self, article: db.Article, parent=None):
        super().__init__(parent)

        self.centralWidget = ArticleWidget(article)

        # buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok |
            QDialogButtonBox.Cancel)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.centralWidget)
        layout.addWidget(self.buttons)
        self.setLayout(layout)

        self.accepted.connect(self.save_settings)
        self.rejected.connect(self.save_settings)

        # load user settings
        self.load_settings()

    def accept(self):
        session.commit()
        super().accept()

    def reject(self):
        session.rollback()
        super().reject()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLE)

        # restore geometry
        try:
            self.restoreGeometry(settings.value(S_GEOMETRY))
        except TypeError as e:
            self.resize(500, 450)
            logger.warning(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ARTICLE)

        # save geometry
        settings.setValue(S_GEOMETRY, self.saveGeometry())

        # save settings of widgets
        self.centralWidget.propertyWidget.save_settings()
        self.centralWidget.distributors.save_settings()
