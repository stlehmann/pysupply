"""
catalog.py - catalog GUI objects

copyright (c) 2014 by Stefan Lehmann
licensed under the MIT license

"""

import os
import shutil
import bisect
import glob
import logging
from urllib.parse import urlparse

from PyQt5.QtCore import QAbstractItemModel, Qt, QModelIndex, QLocale, \
    QObject, QEvent, QVariant, QPoint, QMimeData, QByteArray, QDataStream, \
    QIODevice, QCoreApplication, QSettings, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QPixmap, QFont, QMouseEvent, QDropEvent, \
    QDragMoveEvent, QIcon, QBrush, QColor
from PyQt5.QtWidgets import QWidget, QTreeView, QApplication, \
    QDialog, QMessageBox, QMenu, QInputDialog, QAbstractItemView, \
    QLabel, QLineEdit, QGridLayout, QPushButton
import validators

import pysupply.db as db
from pysupply.db import session, CatalogItem
from ..plugins import distributors as dist_plugins
from pysupply.gui.documents import get_articledir
from pysupply.gui.helper import create_action, Column, create_column_actions
from pysupply.gui.stockchange import StockChangeDialog
from .stockhistory import StockHistoryDialog
import pysupply.gui.resources_rc

logger = logging.getLogger("pysupply.catalog")

KEY, NODE = range(2)

# settings
S_GRP_CATALOG = "pysupply/gui/catalog"
S_HEADER_STATE = "header_state"
S_HIDDEN_COLUMNS = "hidden_columns"
S_STOCK_ONLY = "stock_only"
S_EXPANDED_ITEMS = "expanded_items"


locale = QLocale()

ItemRole = Qt.UserRole + 1
ArticleIdRole = Qt.UserRole + 2


class BaseNode():

    def __init__(self):
        self.children = []
        self.name = ""

    def __len__(self):
        return len(self.children)

    def child_at_row(self, row: int):
        if 0 <= row < len(self.children):
            return self.children[row][NODE]
        return None

    def row_of_child(self, child):
        for i, item in enumerate(self.children):
            if item[NODE] == child:
                return i
        return -1

    def child_with_key(self, key):
        if not self.children:
            return
        i = bisect.bisect_left(self.children(key, None))
        if i < 0 or i >= len(self.children):
            return
        if self.children[i][KEY] == key:
            return self.children[i][NODE]

    def insert_child(self, child):
        child.parent = self
        try:
            bisect.insort(self.children, (child.order_key(), child))
        except TypeError as e:
            logger.exception(e)

    def order_key(self):
        return "{0}".format(self.name.lower())


class CategoryNode(BaseNode):

    def __init__(self, category: db.Category, parent=None):
        if category is not None:
            self.name = category.name
        self.category = category
        self.parent = parent
        self.children = []

    def order_key(self):
        return "%s%i" % (self.name.lower(), self.category.id)

    def has_articles(self):
        if not self.children():
            return False
        for child in self.children:
            if isinstance(child[NODE], ArticleNode):
                return True
        return False


class ArticleNode(BaseNode):

    def __init__(self, article: db.Article, parent=None):
        self.parent = parent
        self.article = article
        self.children = []

        for catalog_item in article.catalog_items:
            self.insert_child(CatalogItemNode(catalog_item))

    def order_key(self):
        try:
            return "%s%i" % (self.article.name.lower(), self.article.id)
        except AttributeError:
            logger.exception(
                'Name of article {article.id} is None'
                .format(article=self.article)
            )
            return ""

    def __len__(self):
        return len(self.children)

    def child_at_row(self, row):
        assert 0 <= row < len(self.children)
        return self.children[row][NODE]

    def row_of_child(self, child):
        for i, item in enumerate(self.children):
            if item[NODE] == child:
                return i
        return -1

    def has_catalogitems(self):
        return len(self.children) > 0


class CatalogItemNode():

    def __init__(self, catalogitem: db.CatalogItem, parent=None):
        self.parent = parent
        self.catalog_item = catalogitem

    def order_key(self):
        if self.catalog_item.name is None:
            return ""
        return self.catalog_item.id

    def __len__(self):
        return 0


class CatalogModel(QAbstractItemModel):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.filter = None
        self.stock_only = False
        self.root = self.init_root()
        self.columns = [
            Column("name", self.tr("name")),
            Column("manufacturer", self.tr("manufacturer")),
            Column("partnr", self.tr("part nr.")),
            Column("articlenr", self.tr("article nr.")),
            Column("unit", self.tr("unit")),
            Column("price", self.tr("price")),
            Column("stock", self.tr("stock"))
        ]

    def init_root(self):
        def _find_children(category_id):
            # Query all categories for given parent
            categories = session.query(db.Category).\
                filter(db.Category.parent_id == category_id)

            # Add children to each category
            for category in categories:
                branch = CategoryNode(category)
                children = _find_children(category.id)
                branch_has_children = False

                for child in children:
                    branch.insert_child(child)
                    branch_has_children = True

                for article in category.articles:
                    try:
                        if (
                            self.filter is None or
                            self.filter.lower() in article.name.lower() and
                            ((not self.stock_only) or (article.stock is not None))
                        ):
                            branch.insert_child(ArticleNode(article))
                            branch_has_children = True
                    except AttributeError:
                        logger.error(
                            f'Attribute Error for article id:{article.id}'
                        )

                if ((self.filter is None and not self.stock_only) or
                        branch_has_children):
                    yield branch

        self.beginResetModel()
        root = CategoryNode(None)
        catalog_node = BaseNode()
        catalog_node.name = self.tr("Catalog")
        root.insert_child(catalog_node)
        for child in _find_children(0):
            catalog_node.insert_child(child)
        self.endResetModel()
        return root

    def index(self, row: int, column: int, parent: QModelIndex()):
        assert self.root
        parent_node = self.node_from_index(parent)
        assert parent_node is not None
        return self.createIndex(row, column, parent_node.child_at_row(row))

    def parent(self, index=QModelIndex()):
        node = self.node_from_index(index)
        if node is None:
            return QModelIndex()
        parent = node.parent
        if parent is None:
            return QModelIndex()
        grandparent = parent.parent
        if grandparent is None:
            return QModelIndex()
        row = grandparent.row_of_child(parent)
        assert row != -1
        return self.createIndex(row, 0, parent)

    def rowCount(self, parent=QModelIndex()):
        node = self.node_from_index(parent)
        return len(node)

    def columnCount(self, parent=QModelIndex()):
        return len(self.columns)

    def data(self, index=QModelIndex, role=Qt.DisplayRole):
        if not index.isValid():
            return

        column = self.columns[index.column()]
        item = self.node_from_index(index)

        if item is None:
            return

        if role in (Qt.DisplayRole, Qt.EditRole):
            if column.name == "name":
                if isinstance(item, CategoryNode):
                    return item.category.name
                elif isinstance(item, ArticleNode):
                    return item.article.name
                elif isinstance(item, CatalogItemNode):
                    return item.catalog_item.distributor.name
                else:
                    return item.name

            elif (column.name == "manufacturer" and
                    isinstance(item, ArticleNode)):
                return item.article.manufacturer

            elif column.name == "partnr" and isinstance(item, ArticleNode):
                return item.article.article_nr

            elif (column.name == "articlenr" and
                    isinstance(item, CatalogItemNode)):
                return item.catalog_item.order_number

            elif column.name == "unit" and isinstance(item, CatalogItemNode):
                return item.catalog_item.packing_unit

            elif column.name == "price":
                if isinstance(item, ArticleNode):
                    article = item.article
                    if not len(article.catalog_items):
                        return
                    try:
                        min_price = min([item.price / item.packing_unit
                                         for item in article.catalog_items
                                         if item.price is not None])
                    except ValueError:
                        return "-"
                    return locale.toCurrencyString(min_price)
                if isinstance(item, CatalogItemNode):
                    return locale.toCurrencyString(item.catalog_item.price) \
                        if item.catalog_item.price is not None else "-"

            elif column.name == "stock" and isinstance(item, ArticleNode):
                article = item.article
                if article.stock is None:
                    return "-"
                else:
                    return article.stock.quantity

        elif role == Qt.TextAlignmentRole:
            if column.name in ("stock", "price"):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.FontRole:
            font = QFont()
            if isinstance(item, CatalogItemNode):
                font.setItalic(True)
            return font

        elif role == Qt.ForegroundRole:
            if isinstance(item, ArticleNode):
                article = item.article
                stock_item = article.stock
                color = Qt.gray

                if stock_item is not None and stock_item.minimum is not None:
                    color = Qt.black
                    if stock_item.quantity > stock_item.minimum:
                        color = Qt.darkGreen
                    elif stock_item.quantity < stock_item.minimum:
                        color = Qt.red

                return QBrush(QColor(color))

        elif role == Qt.DecorationRole:
            if isinstance(item, CategoryNode) and index.column() == 0:
                return QVariant(QPixmap(":icons/category.png"))

        elif role == Qt.UserRole:
            return item

        elif role == ItemRole:
            if isinstance(item, CategoryNode):
                return item.category.id
            elif isinstance(item, ArticleNode):
                return item.article

        elif role == ArticleIdRole:
            if isinstance(item, ArticleNode):
                return item.article.id

    def setData(self, index: QModelIndex, value, role=Qt.EditRole):
        if not index.isValid():
            return False
        if role == Qt.EditRole:
            column = self.columns[index.column()]
            node = self.node_from_index(index)
            if column.name == "name":
                if isinstance(node, CategoryNode):
                    node.category.name = value
                    session.commit()
                elif isinstance(node, ArticleNode):
                    node.article.name = value
                    session.commit()
                return True
        return False

    def headerData(self, section, orientation: Qt.Orientation,
                   role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label

    def flags(self, index: QModelIndex):
        flags = (Qt.ItemIsEnabled | Qt.ItemIsSelectable |
                 Qt.ItemIsDragEnabled | Qt.ItemIsDropEnabled)
        if index.column() == 0:
            flags |= Qt.ItemIsEditable
        return flags

    def node_from_index(self, index):
        return index.internalPointer() if index.isValid() else self.root

    def refresh(self):
        self.root = self.init_root()

    def dropMimeData(self, mimedata: QMimeData, action, row, column, parent):
        if action & Qt.MoveAction:
            if mimedata.hasFormat("application/x-category-ids"):
                # decode mimedata
                enc_data = mimedata.data("application/x-category-ids")
                stream = QDataStream(enc_data, QIODevice.ReadOnly)
                category_ids = []
                while not stream.atEnd():
                    category_ids.append(stream.readInt())
                # move categories
                self.beginResetModel()
                categories = session.query(db.Category)\
                    .filter(db.Category.id.in_(category_ids))
                dest_node = self.node_from_index(parent)
                for category in categories:
                    if (not hasattr(dest_node, 'category') or
                            dest_node.category is None):
                        category.parent_id = 0
                    else:
                        category.parent_id = dest_node.category.id
                self.root = self.init_root()
                self.endResetModel()
                return True

            if mimedata.hasFormat("application/x-list-of-articlenr"):
                # decode mimedata
                enc_data = mimedata.data("application/x-list-of-articlenr")
                stream = QDataStream(enc_data, QIODevice.ReadOnly)
                article_ids = []
                while not stream.atEnd():
                    article_ids.append(stream.readInt())
                # move articles
                self.beginResetModel()
                articles = session.query(db.Article)\
                    .filter(db.Article.id.in_(article_ids))
                dest_node = self.node_from_index(parent)
                if dest_node.category is not None:
                    for article in articles:
                        article.category = dest_node.category
                    session.commit()
                    self.root = self.init_root()
                    self.endResetModel()
                    return True
        return False

    def supportedDropActions(self):
        return Qt.MoveAction | Qt.CopyAction

    def mimeTypes(self):
        return ["application/x-category-ids",
                "application/x-list-of-articlenr"]

    def mimeData(self, indexes):
        mimedata = QMimeData()
        category_data = QByteArray()
        article_data = QByteArray()
        category_stream = QDataStream(category_data, QIODevice.WriteOnly)
        article_stream = QDataStream(article_data, QIODevice.WriteOnly)

        category_ids, article_ids = [], []
        for index in indexes:
            if not index.isValid():
                continue
            node = index.internalPointer()
            if isinstance(node, CategoryNode):
                category_ids.append(node.category.id)
            elif isinstance(node, ArticleNode):
                article_ids.append(node.article.id)

        # category data
        if category_ids:
            for id_ in set(category_ids):
                category_stream.writeInt(id_)
            mimedata.setData("application/x-category-ids", category_data)
        # article data
        if article_ids:
            for id_ in set(article_ids):
                article_stream.writeInt(id_)
            mimedata.setData("application/x-list-of-articlenr", article_data)
        return mimedata

    def add_category(self, parent: QModelIndex, name: str):
        parent_node = self.node_from_index(parent)
        if not isinstance(parent_node, CategoryNode):
            return
        parent_category = parent_node.category
        parent_id = 0 if parent_category is None else parent_category.id

        new_category = db.Category(name=name, parent_id=parent_id)
        session.add(new_category)
        session.commit()
        new_node = CategoryNode(new_category)
        parent_node.insert_child(new_node)
        row = parent_node.row_of_child(new_node)

        self.beginInsertRows(parent, row, row)
        self.endInsertRows()

    def remove_category(self, index: QModelIndex):
        if not index.isValid():
            return
        node = self.node_from_index(index)
        if isinstance(node, CategoryNode):
            if not hasattr(node.parent, 'category'):
                parent_id = 0
            else:
                parent_category = node.parent.category
                parent_id = 0 if parent_category is None else parent_category.id

            for child in node.children:
                child_category = child[NODE].item
                child_category.parent_id = parent_id

            session.delete(node.category)
            session.flush()
            session.commit()
            self.beginRemoveRows(index.parent(), index.row(), index.row())
            self.root = self.init_root()
            self.endRemoveRows()

    def supportedDragActions(self):
        return Qt.MoveAction | Qt.CopyAction


class MyTreeView(QTreeView):

    def startDrag(self, supportedActions: Qt.DropActions):
        index = self.currentIndex()
        node = index.internalPointer()
        if isinstance(node, CategoryNode):
            super().startDrag(Qt.MoveAction)
        else:
            super().startDrag(Qt.MoveAction | Qt.CopyAction)

    def dragMoveEvent(self, event: QDragMoveEvent):
        event.setDropAction(Qt.MoveAction)
        event.accept()

    def dropEvent(self, event: QDropEvent):
        def valid_mimetype():
            for mimetype in self.model().mimeTypes():
                if event.mimeData().hasFormat(mimetype):
                    return True
        if valid_mimetype():
            event.setDropAction(Qt.MoveAction)
            super().dropEvent(event)

    def mousePressEvent(self, event: QMouseEvent):
        index = self.indexAt(event.pos())
        if not index.isValid():
            self.setCurrentIndex(QModelIndex())
        super().mousePressEvent(event)

    def resizeColumnsToContents(self):
        index = self.currentIndex()
        for column in range(self.model().columnCount(index)):
            self.resizeColumnToContents(column)


class CatalogWidget(QWidget):

    """
    Widget for browsing the article catalog.
    :ivar current_category: currently selected category

    """

    category_changed = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.model = CatalogModel()
        self._init_actions()

        self.current_category = None

        # searchbar
        self.searchLabel = QLabel(self.tr("Search:"))
        self.searchLineEdit = QLineEdit()
        self.searchLabel.setBuddy(self.searchLineEdit)
        self.searchButton = QPushButton()
        self.searchButton.setIcon(QIcon(":icons/search.png"))
        self.searchButton.pressed.connect(self.filter)
        self.searchLineEdit.returnPressed.connect(self.filter)

        # treeview
        self.treeview = MyTreeView()
        self.treeview.setModel(self.model)
        self.treeview.setAcceptDrops(True)
        self.treeview.setDragEnabled(True)
        self.treeview.setWordWrap(False)
        self.treeview.header().setStretchLastSection(False)
        self.treeview.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.treeview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeview.clicked.connect(self.treeview_clicked)
        self.treeview.setEditTriggers(QAbstractItemView.EditKeyPressed)
        self.treeview.resizeColumnToContents(0)
        self.treeview.viewport().installEventFilter(self)
        self.treeview.expanded.connect(self.expand)
        self.treeview.customContextMenuRequested.connect(self.show_contextmenu)
        self.treeview.header().setContextMenuPolicy(Qt.ActionsContextMenu)

        # add all columns as actions to header
        for action in self.column_actions:
            self.treeview.header().addAction(action)

        # layout
        layout = QGridLayout()
        layout.addWidget(self.searchLabel, 0, 0)
        layout.addWidget(self.searchLineEdit, 0, 1)
        layout.addWidget(self.searchButton, 0, 2)
        layout.addWidget(self.treeview, 1, 0, 1, 3)
        self.setLayout(layout)

        self.load_settings()

    def _init_actions(self):
        # add category
        self.addCategoryAction = create_action(
            self, self.tr("Add category..."), image=":icons/add.png",
            slot=self.add_category
        )

        # remove category
        self.removeCategoryAction = create_action(
            self, self.tr("Remove category"), image=":icons/remove.png",
            slot=self.remove_category
        )

        # add article
        self.addArticleAction = create_action(
            self, self.tr("Add article..."), image=":icons/add.png",
            slot=self.add_article_slot
        )

        # add article from url
        self.addArticleFromUrlAction = create_action(
            self, self.tr('Add article from URL...'),
            slot=self.add_article_from_url
        )

        # remove article
        self.removeArticleAction = create_action(
            self, self.tr("Remove article"), image=":icons/remove.png",
            slot=self.remove_article
        )

        # edit article
        self.editArticleAction = create_action(
            self, self.tr("Edit article..."), image=":icons/edit.png",
            slot=self.edit_article
        )

        # remove article from stock
        self.removeFromStockAction = create_action(
            self, self.tr("Remove from stock..."),
            slot=self.remove_from_stock
        )

        # change stock quantity
        self.change_stock_action = create_action(
            self, self.tr('Change stock...'),
            slot=self.change_stock
        )

        # show stock changes
        self.show_stock_changes_action = create_action(
            self, self.tr('Show stock changes...'),
            slot=self.show_stock_changes
        )

        # stock only
        self.stockOnlyAction = create_action(
            self, self.tr("show stock only"),
            slot=self.set_stock_only, checkable=True
        )

        # column actions
        self.column_actions = create_column_actions(
            self, self.model.columns, self.toggle_column)

        # open catalog item in browser
        self.openItemInBrowserAction = create_action(
            self, self.tr('open in webbrowser'),
            slot=self.open_item_in_browser
        )

        # copy catalog item data to clipboard
        self.copyCatalogItemToClipboardAction = create_action(
            self, self.tr("copy to clipboard"),
            slot=self.copy_catalog_item_to_clipboard,
            image=":icons/copy.png",
        )

    def eventFilter(self, obj: QObject, event: QEvent):
        if obj.parent() == self.treeview:
            if event.type() == QEvent.MouseButtonDblClick:
                index = self.treeview.currentIndex()
                node = index.internalPointer()
                if isinstance(node, ArticleNode):
                    self.edit_article()
                    return True
                elif isinstance(node, CatalogItemNode):
                    self.edit_article(3)
                    return True
        return False

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_CATALOG)

        # header state
        try:
            self.treeview.header().restoreState(
                settings.value(S_HEADER_STATE))
        except TypeError as e:
            self.treeview.resizeColumnsToContents()
            logger.warning(e)

        # stock only
        try:
            self.stockOnlyAction.setChecked(
                settings.value(S_STOCK_ONLY, False, type=bool))
            self.set_stock_only()

        except TypeError as e:
            logger.warning(e)

        # hidden columns
        hidden_columns = settings.value(S_HIDDEN_COLUMNS)
        try:
            if hidden_columns is not None:
                for i, c in enumerate(self.model.columns):
                    if c.name in hidden_columns:
                        self.column_actions[i].setChecked(False)
                        self.treeview.setColumnHidden(i, True)
        except TypeError as e:
            logger.warning(e)

        # expanded items
        self.load_expanded_items()

        settings.endGroup()

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_CATALOG)

        # header state
        settings.setValue(
            S_HEADER_STATE, self.treeview.header().saveState())

        # stock only
        settings.setValue(S_STOCK_ONLY, self.stockOnlyAction.isChecked())

        # hidden columns
        settings.setValue(
            S_HIDDEN_COLUMNS,
            [c.data().name for c in self.column_actions if not c.isChecked()]
        )

        # expanded items
        self.save_expanded_items()

        settings.endGroup()

    def add_article_slot(self):
        return self.add_article()

    def add_article(self, article: db.Article=None):
        if article is None:
            article = db.Article()

        index = self.treeview.currentIndex()
        parent = index.parent()
        if not index.isValid():
            return
        node = index.internalPointer()

        if isinstance(node, CategoryNode):

            from pysupply.gui.article import ArticleDialog
            article.category = node.category
            articleDlg = ArticleDialog(article, self)
            session.commit()

            if articleDlg.exec_() == QDialog.Accepted:

                session.add(article)
                session.commit()

                # refresh catalog
                child = ArticleNode(article)
                node.insert_child(child)
                row = node.row_of_child(child)
                self.model.beginInsertRows(parent, row, row)
                self.model.endInsertRows()

                # documents dir
                nonedir = get_articledir("None")
                newdir = get_articledir(str(article.id))
                documents = glob.glob(os.path.join(nonedir, "*"))

                if len(documents) > 0:
                    try:
                        os.mkdir(newdir)
                    except FileExistsError:
                        logger.debug(f'Directory {newdir} already exists.')

                for document in documents:
                    head, tail = os.path.split(document)
                    shutil.move(document, os.path.join(newdir, tail))

                if os.path.exists(nonedir):
                    os.rmdir(nonedir)

    def add_article_from_url(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        node = index.internalPointer()

        if isinstance(node, CategoryNode):
            valid_url = False
            url = ''
            while not valid_url:
                url, ok = QInputDialog.getText(self, self.tr('Paste URL'),
                                               'URL:', text=url)
                if not ok:
                    return

                if validators.url(url):
                    valid_url = True
                else:
                    QMessageBox.warning(self, self.tr(
                        'Invalid url'), self.tr('Please insert a valid url.'))

            parse_result = urlparse(url)

            # Try RSOnline
            try:
                from ..plugins.distributors import rsonline, conrad
                if parse_result.netloc in rsonline.netlocs:
                    article = rsonline.import_article(url)
                    if isinstance(article, db.Article):
                        return self.add_article(article)
                    else:
                        raise ValueError()
                elif parse_result.netloc in conrad.netlocs:
                    article = conrad.import_article(url)
                    if isinstance(article, db.Article):
                        return self.add_article(article)
                    else:
                        raise ValueError()
                else:
                    raise ValueError()
            except (ImportError, ValueError, TimeoutError):
                QMessageBox.warning(self, self.tr('Error importing article from url'),
                                    self.tr('Could not import article from the specified url.'))

    def edit_article(self, tab_index=0):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        # check if node is CatalogItem or Article
        node = self.model.data(index, Qt.UserRole)
        assert isinstance(node, (ArticleNode, CatalogItemNode))
        if isinstance(node, CatalogItemNode):
            node = node.parent
            index = index.parent()
        article = node.article

        # show Dialog
        from pysupply.gui.article import ArticleDialog
        articleDlg = ArticleDialog(article, self)
        articleDlg.centralWidget.tabWidget.setCurrentIndex(tab_index)
        items_old = list(article.catalog_items)

        if articleDlg.exec_() == QDialog.Accepted:
            session.commit()

            # look for removed catalog items
            for row in range(len(node)):
                child = node.child_at_row(row)
                if child.catalog_item not in article.catalog_items:
                    self.model.beginRemoveRows(index, row, row)
                    node.children.pop(row)
                    self.model.endRemoveRows()

            # look for new catalog items
            for item in article.catalog_items:
                if item not in items_old:
                    new_child = CatalogItemNode(item)
                    node.insert_child(new_child)
                    row = node.row_of_child(new_child)
                    self.model.beginInsertRows(index, row, row)
                    self.model.endInsertRows()

    def add_category(self):
        name, b = QInputDialog.getText(self, QApplication.applicationName(),
                                       self.tr("Category name:"))
        if b and name:
            parent = self.treeview.currentIndex()
            self.model.add_category(parent, name)

    def expand(self, index: QModelIndex):
        # self.treeview.resizeColumnsToContents()
        pass

    def load_expanded_items(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_CATALOG)

        try:
            for index in settings.value(S_EXPANDED_ITEMS, list):
                items = self.model.match(
                    self.model.index(0, 0, QModelIndex()),
                    Qt.DisplayRole, index, flags=Qt.MatchRecursive
                )
                if items:
                    self.treeview.setExpanded(items[0], True)

        except TypeError as e:
            logger.warning(e)

    def save_expanded_items(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_CATALOG)

        expanded_items = [
            index.data(Qt.DisplayRole)
            for index in self.model.persistentIndexList()
            if self.treeview.isExpanded(index)
        ]
        settings.setValue(S_EXPANDED_ITEMS, expanded_items)

    def find_article(self, article: db.Article):
        if article is None:
            return
        # match article id
        indexes = self.model.match(
            self.model.index(0, 0, QModelIndex()),
            Qt.DisplayRole, article.name, 2, Qt.MatchRecursive
        )
        index = indexes[0] if indexes else None
        if index is None or not index.isValid():
            return
        # get all parents
        indexes = []
        while index.isValid():
            indexes.insert(0, index)
            index = index.parent()
        # expand all
        for index in indexes:
            self.treeview.setExpanded(index, True)
        # select index
        self.treeview.setCurrentIndex(index)

    def remove_category(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        node = index.internalPointer()
        if not isinstance(node, CategoryNode):
            return
        category = node.category
        res = QMessageBox.question(
            self, QCoreApplication.applicationName(),
            self.tr("Do you really want to delete the category '%s'?")
            % category.name, QMessageBox.Yes | QMessageBox.No)
        if res == QMessageBox.Yes:
            self.model.remove_category(self.treeview.currentIndex())

    def remove_article(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        node = index.internalPointer()
        if isinstance(node, ArticleNode):
            article = node.article
            res = QMessageBox.question(
                self,
                QApplication.applicationName(),
                self.tr("Do you want to delete the article") +
                " '%s'?" % article.name
            )
            if res == QMessageBox.Yes:
                self.model.beginRemoveRows(
                    index.parent(), index.row(), index.row())
                parent_node = node.parent
                parent_node.children.pop(index.row())
                session.delete(article)
                session.commit()
                self.model.endRemoveRows()

                # clean document dir
                dir = get_articledir(str(article.id))
                if os.path.exists(dir):
                    shutil.rmtree(dir)

    @pyqtSlot()
    def copy_catalog_item_to_clipboard(self):
        """Copy the currently selected catalog item to the clipboard."""
        # get index and check if it's valid
        index = self.treeview.currentIndex()
        if not index.isValid():
            return

        node = index.internalPointer()

        # only continue for catalog items
        if not isinstance(node, CatalogItemNode):
            return

        catalog_item: CatalogItem = node.catalog_item

        data = [
            catalog_item.article.name,
            catalog_item.order_number,
            catalog_item.article.manufacturer,
            str(catalog_item.price).replace(".", ","),
        ]

        QApplication.clipboard().setText("\t".join(data))

    def filter(self):
        def _expand(index):
            node = self.model.node_from_index(index)
            if isinstance(node, (CategoryNode)):
                self.treeview.expand(index)
            for row in range(len(node)):
                child_index = self.model.index(row, 0, index)
                if child_index.isValid():
                    _expand(child_index)

        text = self.searchLineEdit.text()
        if text == "":
            text = None

        self.model.filter = text

        # self.save_expanded_items()
        self.model.refresh()
        # self.load_expanded_items()

        if text is not None:
            _expand(QModelIndex())

    def refresh(self):
        self.model.refresh()

    def remove_from_stock(self):
        index = self.treeview.currentIndex()
        node = index.internalPointer()
        article = node.article

        res = QMessageBox.question(
            self, QApplication.applicationName(),
            self.tr("Remove article '{}' from stock?").format(article.name)
        )

        if res == QMessageBox.No:
            return

        stockitem = article.stock
        session.delete(stockitem)
        article.stock = None
        session.commit()

    def set_stock_only(self):
        self.model.stock_only = self.stockOnlyAction.isChecked()

        # self.save_expanded_items()
        self.model.refresh()
        # self.load_expanded_items()

    def treeview_clicked(self):
        index = self.treeview.currentIndex()

        if not index.isValid():
            return

        node = self.model.node_from_index(index)

        if isinstance(node, CategoryNode):
            new_category = node.category
        elif isinstance(node, ArticleNode):
            new_category = node.article.category
        else:
            new_category = None

        if new_category is not self.current_category:
            self.current_category = new_category
            self.category_changed.emit(new_category)

    def show_contextmenu(self, pos: QPoint):
        """Show context menu for a specific item in the catalog.

        There are different entries for ArticleNode, CategoryNode and CatalogItemNode
        objects.

        """
        index = self.treeview.currentIndex()
        menu = QMenu(self)

        if index.isValid():
            node = index.internalPointer()

            if isinstance(node, ArticleNode):
                menu.addAction(self.editArticleAction)
                menu.addSeparator()
                menu.addAction(self.removeArticleAction)
                menu.setDefaultAction(self.editArticleAction)
                article = node.article
                if article.stock is not None:
                    menu.addAction(self.change_stock_action)
                    menu.addAction(self.show_stock_changes_action)
                    menu.addAction(self.removeFromStockAction)

            elif isinstance(node, CategoryNode):
                menu.addAction(self.addArticleAction)
                menu.addAction(self.addArticleFromUrlAction)
                menu.setDefaultAction(self.addArticleAction)
                menu.addSeparator()
                menu.addActions([
                    self.addCategoryAction,
                    self.removeCategoryAction
                ])

            elif isinstance(node, CatalogItemNode):
                catalog_item = node.catalog_item
                plugin = catalog_item.distributor.plugin

                if plugin is not None:
                    if hasattr(plugin, 'open_item_in_browser'):
                        menu.addAction(self.openItemInBrowserAction)
                        menu.addSeparator()
        else:
            menu.addAction(self.addCategoryAction)

        menu.addAction(self.stockOnlyAction)
        menu.addAction(self.copyCatalogItemToClipboardAction)

        globalpos = self.treeview.viewport().mapToGlobal(pos)
        menu.popup(globalpos)

    def toggle_column(self):
        for action in self.column_actions:
            column = action.data()
            index = self.model.columns.index(column)
            self.treeview.setColumnHidden(index, not action.isChecked())

    def open_item_in_browser(self):
        index = self.treeview.currentIndex()
        if not index.isValid():
            return
        node = self.model.node_from_index(index)
        if isinstance(node, CatalogItemNode):
            catalog_item = node.catalog_item
            plugin = catalog_item.distributor.plugin
            plugin.open_item_in_browser(catalog_item)

    def change_stock(self):

        # check for valid index
        index = self.treeview.currentIndex()
        if not index.isValid():
            return

        # check for node being ArticleNode
        node = self.model.node_from_index(index)
        if not isinstance(node, ArticleNode):
            return

        article = node.article
        stock_change_item = db.StockChangeItem(article=article)
        dlg = StockChangeDialog(stock_change_item, self)
        dlg.setModal(True)
        if dlg.exec_() == dlg.Accepted:
            stock_change_item.apply_change()
            db.session.add(stock_change_item)
            db.session.commit()
        else:
            db.session.rollback()

    # stock only property
    @property
    def stock_only(self):
        return self.model.stock_only

    @stock_only.setter
    def stock_only(self, value):
        self.model.stock_only = value

    def show_stock_changes(self):
        # check for valid index
        index = self.treeview.currentIndex()
        if not index.isValid():
            return

        # check for node being ArticleNode
        node = self.model.node_from_index(index)
        if not isinstance(node, ArticleNode):
            return

        article = node.article
        dlg = StockHistoryDialog(article, self)
        dlg.setModal(True)
        dlg.exec_()
