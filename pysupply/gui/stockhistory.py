"""
pyads.gui.stockhistory.py
-------------------------

:author: Stefan Lehmann <stefan.st.lehmann@gmail.com
:license: MIT license

:created on 2017-09-29 13:43:18
:last modified by:   Stefan Lehmann
:last modified time: 2017-10-06 14:27:59

"""
from pysupply.db import session, Article
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QGridLayout, \
    QTableView, QWidget
from PyQt5.QtCore import QAbstractTableModel, QModelIndex, Qt
from .helper import Column


class StockHistoryTableModel(QAbstractTableModel):

    def __init__(self, article: Article, parent: QWidget=None) -> None:
        super().__init__(parent)
        self.article = article
        self.stock_changes = list(self.article.stock_changes)

        self.columns = [
            Column('date', self.tr('date')),
            Column('quantity', self.tr('quantity')),
            Column('comment', self.tr('comment')),
            Column('order', self.tr('order')),
        ]

    def rowCount(self, parent: QModelIndex=QModelIndex()):
        return len(self.stock_changes)

    def columnCount(self, parent: QModelIndex=QModelIndex()):
        return len(self.columns)

    def data(self, index: QModelIndex, role: int=Qt.DisplayRole):
        if not index.isValid():
            return

        item = self.stock_changes[index.row()]
        column_name = self.columns[index.column()].name

        if role == Qt.DisplayRole:
            if column_name == 'date':
                try:
                    return item.date.strftime('%d.%m.%Y')
                except AttributeError:
                    return '-'
            elif column_name == 'quantity':
                qty_chg = item.quantity_change
                qty_str = ('+{}'.format(qty_chg) if qty_chg >= 0 else
                           '-{}'.format(qty_chg))

                return '{0} ({1})'.format(item.resulting_quantity,
                                          qty_str)
            elif column_name == 'order':
                if item.order is None:
                    return '-'
                return '{0}: {1}'.format(item.order.number, item.order.name)
            return getattr(item, column_name)

    def headerData(self, section: int, orientation: int,
                   role: int=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            else:
                return section + 1


class StockHistoryDialog(QDialog):
    """
    Dialog that shows the history of stock changes for an article

    """

    def __init__(self, article: Article, parent: QWidget=None) -> None:
        super().__init__(parent)
        assert article is not None

        self.article = article

        self.stock_changes_model = StockHistoryTableModel(article)

        self.stock_history_tableview = QTableView()
        self.stock_history_tableview.setModel(self.stock_changes_model)
        self.stock_history_tableview.resizeColumnsToContents()
        self.buttons = QDialogButtonBox(QDialogButtonBox.Close)
        self.buttons.clicked.connect(self.close)

        layout = QGridLayout()
        layout.addWidget(self.stock_history_tableview, 0, 0)
        layout.addWidget(self.buttons, 1, 0)
        self.setLayout(layout)

        self.resize(640, 480)


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication

    article = session.query(Article).filter(Article.id == 200).first()
    app = QApplication(sys.argv)
    w = StockHistoryDialog(article)
    w.show()
    app.exec_()
