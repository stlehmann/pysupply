"""
materiallist.py,
copyright (c) 2016 by Stefan Lehmann

"""

import logging

from PyQt5.QtCore import Qt, QAbstractTableModel, QSettings
from PyQt5.QtWidgets import QWidget, QTableView, QVBoxLayout, QMessageBox, \
    QApplication, QMenu

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.helper import Column, create_action


S_GRP = 'pysupply/gui/materiallists'
S_HEADER_STATE = 'header_state'

logger = logging.getLogger("pysupply.gui.materiallist")


class MateriallistModel(QAbstractTableModel):

    def __init__(self, project, parent=None):
        super().__init__(parent)

        self.project = project
        self.columns = [
            Column("name", self.tr("name")),
            Column("comment", self.tr("comment")),
            Column("item_count", self.tr("items")),
            Column("removed_from_stock", self.tr("removed from stock"))
        ]
        self.refresh()

    def refresh(self):
        self.beginResetModel()
        self.items = list(
            session.query(db.MaterialList).filter(
                db.MaterialList.project == self.project
            )
        )
        self.endResetModel()

    def rowCount(self, index):
        return len(self.items)

    def columnCount(self, index):
        return len(self.columns)

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        item = self.items[index.row()]
        attr = self.columns[index.column()].name

        if role in (Qt.DisplayRole, Qt.EditRole):
            if attr == 'removed_from_stock':
                return (self.tr('yes') if item.removed_from_stock else
                        self.tr('no'))
            else:
                return getattr(item, attr)

        elif role == Qt.TextAlignmentRole:
            if attr == 'removed_from_stock':
                return Qt.AlignVCenter | Qt.AlignHCenter

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            else:
                return section + 1


class MaterialListWidget(QWidget):

    def __init__(self, project, mainwindow, parent=None):
        super().__init__(parent)
        self.project = project
        self.mainwindow = mainwindow
        self.model = MateriallistModel(project, self)

        self._init_actions()

        # tableview
        self.tableview = QTableView()
        self.tableview.setModel(self.model)
        self.tableview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.tableview.customContextMenuRequested.connect(self.show_contextmenu)
        self.tableview.addAction(self.openMaterialListAction)
        self.tableview.addAction(self.addMaterialListAction)
        self.tableview.addAction(self.deleteMaterialListAction)
        self.tableview.doubleClicked.connect(self.open_materiallist)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.tableview)
        self.setLayout(layout)

        self.load_settings()

    def _init_actions(self):
        # open selected MaterialList
        self.openMaterialListAction = create_action(
            self, self.tr("Open..."), slot=self.open_materiallist
        )

        # add materiallist to current project
        self.addMaterialListAction = create_action(
            self, self.tr("Add Materiallist"), slot=self.add_materiallist
        )

        # delete materiallist
        self.deleteMaterialListAction = create_action(
            self, self.tr("Delete Materiallist"), slot=self.delete_materiallist
        )

    def closeEvent(self, event):
        self.save_settings()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP)

        # header state
        state = settings.value(S_HEADER_STATE)
        try:
            if state is not None:
                self.tableview.horizontalHeader().restoreState(state)
        except TypeError as e:
            logger.debug(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP)

        # header state
        state = self.tableview.horizontalHeader().saveState()
        settings.setValue(S_HEADER_STATE, state)

    def add_materiallist(self):
        materiallist = db.MaterialList()
        materiallist.name = self.tr('new materiallist')
        materiallist.project = self.project

        db.session.add(materiallist)
        db.session.commit()
        self.model.refresh()

    def delete_materiallist(self):

        def delete(materiallist):
            items = materiallist.items
            db.session.delete(materiallist)
            for item in items:
                db.session.delete(item)

        indexes = self.tableview.selectedIndexes()

        item_set = set()
        for index in indexes:
            item_set.add(self.model.items[index.row()])

        delete_all = False
        for materiallist in item_set:
            if not delete_all:
                res = QMessageBox.question(
                    self, QApplication.applicationName(),
                    self.tr('Delete materiallist "{}" with all items?')
                    .format(materiallist.name),
                    buttons=QMessageBox.Yes | QMessageBox.YesAll |
                    QMessageBox.No | QMessageBox.Cancel
                )

                if res == QMessageBox.Yes:
                    delete(materiallist)

                if res == QMessageBox.YesAll:
                    delete_all = True
                    delete(materiallist)
            else:
                delete(materiallist)

        db.session.commit()
        self.model.refresh()

    def open_materiallist(self):
        index = self.tableview.currentIndex()
        if not index.isValid():
            return

        materiallist = self.model.items[index.row()]

        self.mainwindow.open_materiallist(materiallist)

    def show_contextmenu(self, pos):
        index = self.tableview.currentIndex()
        menu = QMenu(self)

        if index.isValid():
            menu.addAction(self.openMaterialListAction)
            menu.addSeparator()

        menu.addAction(self.addMaterialListAction)

        if index.isValid():
            menu.addAction(self.deleteMaterialListAction)
            menu.setDefaultAction(self.openMaterialListAction)

        global_pos = self.tableview.viewport().mapToGlobal(pos)
        menu.popup(global_pos)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)

    project = session.query(db.Project).first()
    wgt = MaterialListWidget(project)
    wgt.show()
    app.exec_()
