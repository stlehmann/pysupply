import logging

from PyQt5.QtCore import Qt, QCoreApplication, QEvent, QDate, QLocale, \
    QModelIndex, QSettings
from PyQt5.QtGui import QBrush
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QDialog, QDialogButtonBox, \
    QTreeView, QMessageBox

import pysupply.db as db
from pysupply.db import session, ORDERSTATE_DRAFT, ORDERSTATE_ORDERED, \
    ORDERSTATE_RECEIVED
from pysupply.gui.helper import create_action, TAX_FACTOR, Column, \
    create_column_actions
from pysupply.gui.treenodes import Node, Leaf, TreeNodeItemModel
import pysupply.gui.resources_rc


S_GRP_ORDERLIST = "pysupply.orderlist"
S_DLG_GEOMETRY = "dlg_geometry"
S_HEADER_STATE = "header_state"
S_HIDDEN_COLUMNS = "hidden_columns"

logger = logging.getLogger("pysupply.gui.orderlist")


class OrderlistModel(TreeNodeItemModel):

    def __init__(self, filter_=None):
        super().__init__()
        self.filter_ = filter_
        self.columns = [
            Column("date", self.tr("date")),
            Column("number", self.tr("order nr.")),
            Column("name", self.tr("name")),
            Column("state", self.tr("state")),
            Column("distributor", self.tr("distributor")),
            Column("net", self.tr("net")),
            Column("gross", self.tr("gross"))
        ]

    def create_rootnode(self):
        root = Node(None, None)
        orders = filter(
            self.filter_, session.query(db.Order).order_by(db.Order.date))

        for order in orders:
            year = None
            if order.date is not None:
                year = order.date.year
                try:
                    yearnode = next(n for n in root.children if n.item == year)
                except StopIteration:
                    yearnode = Node(year)
                    root.add_child(yearnode)
            (yearnode if year is not None else root).add_child(Leaf(order))
        self.root = root

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return

        attr = self.columns[index.column()].name
        node = self.node_from_index(index)
        item = node.item

        if role == Qt.DisplayRole:
            if attr == "date":
                if isinstance(item, db.Order):
                    try:
                        return QDate.fromString(
                            item.date.strftime("%d.%m.%Y"), "dd.MM.yyyy")
                    except AttributeError:
                        pass

                elif isinstance(item, int):
                    return str(item)

            elif attr == "state":
                if isinstance(item, db.Order):
                    try:
                        if item.state == ORDERSTATE_DRAFT:
                            return self.tr("draft")
                        if item.state == ORDERSTATE_ORDERED:
                            return self.tr("ordered")
                        elif item.state == ORDERSTATE_RECEIVED:
                            return self.tr("received")
                    except TypeError:
                        pass
                    return "-"

            elif attr == "distributor":
                if isinstance(item, db.Order):
                    if item.distributor is not None:
                        return item.distributor.name

            elif attr == "net":
                if isinstance(item, db.Order):
                    return QLocale().toCurrencyString(item.total())

            elif attr == "gross":
                if isinstance(item, db.Order):
                    return QLocale().toCurrencyString(
                        item.total() * TAX_FACTOR)

            else:
                if isinstance(item, db.Order):
                    return getattr(item, attr)

        elif role == Qt.ForegroundRole:
            if isinstance(item, db.Order):
                if item.state is None:
                    return
                if item.state == db.ORDERSTATE_ORDERED:
                    return QBrush(Qt.darkYellow)
                if item.state == db.ORDERSTATE_RECEIVED:
                    return QBrush(Qt.darkGreen)

        elif role == Qt.TextAlignmentRole:
            if attr == "gross":
                return Qt.AlignVCenter | Qt.AlignRight
            else:
                return Qt.AlignVCenter | Qt.AlignLeft

        elif role == Qt.UserRole:
            return item

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label
            elif orientation == Qt.Vertical:
                return section + 1

    def flags(self, index):
        return Qt.ItemIsEnabled | Qt.ItemIsSelectable

    def insertRows(self, row, count, parent=QModelIndex()):
        self.beginInsertRows(parent, row, count)
        self.endInsertRows()
        return True

    def removeRows(self, row, count, parent=QModelIndex()):
        for r in range(row, row + count):
            index = self.index(r, 0, parent)
            node = self.node_from_index(index)
            order = node.item

            session.delete(order)
            session.commit()
            self.create_rootnode()
        self.beginResetModel()
        self.endResetModel()
        return True

    def refresh(self):
        self.create_rootnode()


class OrderlistWidget(QWidget):

    """
    Widget for displaying and editing orders.

    """

    def __init__(self, projectWidget=None, filter_=None, parent=None):
        super().__init__(parent)
        self.projectWidget = projectWidget

        # model
        self.model = OrderlistModel()
        self.model.filter_ = filter_
        self.model.create_rootnode()

        self.init_actions()

        # treeview
        self.treeview = QTreeView()
        self.treeview.setModel(self.model)
        self.treeview.header().setStretchLastSection(False)
        self.treeview.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.treeview.header().setContextMenuPolicy(Qt.ActionsContextMenu)
        for action in self.column_actions:
            self.treeview.header().addAction(action)

        # actions for treeview context menu
        if self.projectWidget is not None:
            self.treeview.addAction(self.addOrderAction)
        self.treeview.addAction(self.removeOrderAction)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.treeview)
        self.setLayout(layout)

        # load settings
        self.load_settings()

    def init_actions(self):

        # add order
        self.addOrderAction = create_action(
            self, self.tr("add order"), image=":icons/add.png",
            slot=self.add_order
        )

        # remove order
        self.removeOrderAction = create_action(
            self, self.tr("remove order"), image=":icons/remove.png",
            slot=self.remove_order
        )

        # columns
        self.column_actions = create_column_actions(
            self, self.model.columns, self.toggle_column
        )

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDERLIST)

        # header state
        try:
            self.treeview.header().restoreState(
                settings.value(S_HEADER_STATE)
            )
        except TypeError as e:
            self.resize_columns_to_contents()
            logger.warning(e)

        # hidden columns
        hidden_columns = settings.value(S_HIDDEN_COLUMNS)
        try:
            for i, c in enumerate(self.model.columns):
                if c.name in hidden_columns:
                    self.column_actions[i].setChecked(False)
                    self.treeview.setColumnHidden(i, True)
        except TypeError as e:
            logger.warning(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDERLIST)

        # header state
        settings.setValue(
            S_HEADER_STATE,
            self.treeview.header().saveState()
        )

        # hidden columns
        settings.setValue(
            S_HIDDEN_COLUMNS,
            [c.data().name for c in self.column_actions if not c.isChecked()]
        )

    def add_order(self):
        if self.projectWidget is not None:
            self.model.beginResetModel()
            self.projectWidget.add_order()
            self.model.endResetModel()

    def remove_order(self):
        index = self.treeview.currentIndex()
        order = self.model.data(index, Qt.UserRole)
        if QMessageBox.question(
                self, QCoreApplication.applicationName(),
                self.tr("Do you want to delete the order %s and all of its "
                        "items?") % order.name) == QMessageBox.Yes:
            self.model.removeRow(index.row(), index.parent())

    def refresh(self):
        self.model.refresh()

    def resize_columns_to_contents(self):
        for column in range(self.model.columnCount()):
            self.treeview.resizeColumnToContents(column)

    def toggle_column(self):
        for action in self.column_actions:
            column = action.data()
            index = self.model.columns.index(column)
            self.treeview.setColumnHidden(index, not action.isChecked())

    @property
    def current_order(self):
        index = self.treeview.currentIndex()
        node = index.internalPointer()
        if isinstance(node.item, db.Order):
            return node.item


class OrderDialog(QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)

        # Table
        self.orderlist = OrderlistWidget()
        self.orderlist.treeview.viewport().installEventFilter(self)

        # Buttons
        self.buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel,
            Qt.Horizontal
        )
        self.buttons.rejected.connect(self.reject)
        self.buttons.accepted.connect(self.accept)

        # Layout
        layout = QVBoxLayout()
        layout.addWidget(self.orderlist)
        layout.addWidget(self.buttons)
        self.setLayout(layout)

        self.setWindowTitle(self.tr("Select an order to open."))
        self.accepted.connect(self.save_settings)
        self.rejected.connect(self.save_settings)
        self.resize(640, 480)
        self.load_settings()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDERLIST)

        # geometry
        try:
            self.restoreGeometry(settings.value(S_DLG_GEOMETRY))
        except TypeError as e:
            logger.warning(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(S_GRP_ORDERLIST)

        # geometry
        settings.setValue(S_DLG_GEOMETRY, self.saveGeometry())

    def current_order(self):
        index = self.orderlist.treeview.currentIndex()
        node = index.internalPointer()
        if node is None:
            return None
        if isinstance(node.item, db.Order):
            return node.item

    def eventFilter(self, obj, event):
        if obj == self.orderlist.treeview.viewport():
            if event.type() == QEvent.MouseButtonDblClick:
                if self.orderlist.current_order is not None:
                    self.accept()
                    return True
        return False

    def closeEvent(self, event):
        self.save_settings()
        event.accept()


if __name__ == '__main__':
    from PyQt5.QtWidgets import QApplication
    import sys

    app = QApplication(sys.argv)
    o = OrderDialog()
    o.show()
    app.exec_()