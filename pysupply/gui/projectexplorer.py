import sys
import logging

from sqlalchemy import asc, desc
from PyQt5.QtCore import (
    QAbstractItemModel,
    QModelIndex,
    Qt,
    QPoint,
    pyqtSignal,
    QCoreApplication,
    QMimeData,
    QDataStream,
    QIODevice,
    QLocale,
    QVariant,
    QByteArray,
    QDate,
    QSettings,
    QSize,
)
from PyQt5.QtGui import QPixmap, QMouseEvent, QFont
from PyQt5.QtWidgets import (
    QWidget,
    QTreeView,
    QVBoxLayout,
    QApplication,
    QMenu,
    QDialog,
    QMessageBox,
    QDialogButtonBox,
)

import pysupply.db as db
from pysupply.db import session
from pysupply.gui.helper import TAX_FACTOR, create_action, Column, create_column_actions
from pysupply.gui.treenodes import Node
import pysupply.gui.resources_rc


SETTINGS_GROUP = "pysupply/gui/projectexplorer"
HEADERSTATE_SETTING = "headerstate"
HIDDEN_COLUMNS_SETTING = "hidden_columns"
DIALOG_GEOMETRY_SETTING = "geometry"


logger = logging.getLogger("pysupply.gui.projectexplorer")


def sort_nodes(node):
    item = node.item
    if isinstance(node.item, int):
        return (1, item)
    elif isinstance(item, db.Project):
        return (2, item.number, item.name or "")
    else:
        return (3, item.number or item.name or "")


class ProjectModel(QAbstractItemModel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.columns = [
            Column("number", self.tr("projectnumber")),
            Column("name", self.tr("projectname")),
            Column("net", self.tr("net")),
            Column("gross", self.tr("gross")),
        ]
        self.root = self.create_rootnode()

    def create_rootnode(self, order_by=db.Order.date, sort_order=asc):
        root = Node(None, None)
        for project in session.query(db.Project).order_by(db.Project.number):
            try:
                projectnode = next(
                    node
                    for node in root.children
                    if isinstance(node, Node)
                    and isinstance(node.item, db.Project)
                    and node.item.id == project.id
                )
            except StopIteration:
                projectnode = Node(project)
                root.add_child(projectnode)
        return root

    def index(self, row: int, column: int, parent: QModelIndex):
        parent_node = self.node_from_index(parent)
        return self.createIndex(row, column, parent_node.child_at_row(row))

    def parent(self, index: QModelIndex):
        node = self.node_from_index(index)
        if node is None:
            return QModelIndex()
        parent = node.parent
        if parent is None:
            return QModelIndex()
        grandparent = parent.parent
        if grandparent is None:
            return QModelIndex()
        row = grandparent.row_of_child(parent)
        return self.createIndex(row, 0, parent)

    def rowCount(self, parent=QModelIndex()):
        node = self.node_from_index(parent)
        return len(node)

    def columnCount(self, parent=QModelIndex()):
        return len(self.columns)

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        if not index.isValid():
            return

        attr = self.columns[index.column()].name
        node = self.node_from_index(index)
        item = node.item

        if role in (Qt.DisplayRole, Qt.EditRole):
            # Order number
            if attr == "number":
                return item.number

            # Name
            elif attr == "name":
                return item.name

            # Netto
            elif attr == "net":
                return QLocale().toCurrencyString(item.total())

            # Brutto
            elif attr == "gross":
                return QLocale().toCurrencyString(item.total() * TAX_FACTOR)

        elif role == Qt.DecorationRole:
            if attr == "number":
                node = self.node_from_index(index)
                # Project icon
                if isinstance(node.item, db.Project):
                    return QPixmap(":icons/project.png")

        elif role == Qt.FontRole:
            # Projects are bold
            if isinstance(item, db.Project):
                boldFont = QFont()
                boldFont.setBold(True)
                return boldFont

        elif role == Qt.TextAlignmentRole:
            if attr in ("net", "gross"):
                return Qt.AlignVCenter | Qt.AlignRight

        elif role == Qt.UserRole:
            return item

    def setData(self, index: QModelIndex, value: QVariant, role=Qt.EditRole):
        if not index.isValid():
            return

        node = self.node_from_index(index)
        item = node.item
        attr = self.columns[index.column()].name

        if role == Qt.EditRole:
            if isinstance(item, db.Directory):
                if attr == "name":
                    item.name = value
                    session.commit()
                    return True
        return False

    def node_from_index(self, index: QModelIndex):
        return index.internalPointer() if index.isValid() else self.root

    def flags(self, index: QModelIndex):
        flags = Qt.ItemIsSelectable | Qt.ItemIsEnabled
        return flags

    def refresh(self):
        self.root = self.create_rootnode()

    def headerData(
        self, section: int, orientation: Qt.Orientation, role=Qt.DisplayRole
    ):
        attr = self.columns[section].name
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return self.columns[section].label

        elif role == Qt.TextAlignmentRole:
            if orientation == Qt.Horizontal:
                if attr in ("net", "gross"):
                    return Qt.AlignVCenter | Qt.AlignRight
        return

    def add_project(self, project: db.Project, parent=QModelIndex()):
        parentnode = self.node_from_index(parent)
        child = Node(project, parentnode)
        parentnode.add_child(child)
        row = parentnode.row_of_child(child)
        self.beginInsertRows(parent, row, row)
        self.endInsertRows()

    def sort(self, column, order=Qt.AscendingOrder):
        import functools

        order_dict = {
            "name": db.Order.name,
            "date": db.Order.date,
            "order number": db.Order.number,
        }

        sort_order = asc if order == Qt.AscendingOrder else desc
        sort_property = order_dict.get(self.columns[column])
        create_rootnode_fct = (
            self.create_rootnode
            if sort_property is None
            else functools.partial(self.create_rootnode, sort_property)
        )

        self.beginResetModel()
        self.root = create_rootnode_fct(sort_order=sort_order)
        self.endResetModel()


class ProjectTreeView(QTreeView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.viewport().setAcceptDrops(True)
        self.setDefaultDropAction(Qt.MoveAction)
        self.setDragEnabled(True)
        self.setSortingEnabled(True)
        self.setSelectionMode(QTreeView.ExtendedSelection)
        self.expanded.connect(self.resizeColumnsToContents)
        self.collapsed.connect(self.resizeColumnsToContents)

    def mousePressEvent(self, event: QMouseEvent):
        index = self.indexAt(event.pos())
        if not index.isValid():
            self.setCurrentIndex(QModelIndex())
        super().mousePressEvent(event)

    def resizeColumnsToContents(self):
        for column in range(self.model().columnCount()):
            self.resizeColumnToContents(column)


class ProjectExplorer(QWidget):

    lockstate_changed = pyqtSignal(object, bool)
    project_opened = pyqtSignal()

    def __init__(self, mainwindow, parent=None):
        super().__init__(parent)

        self.model = ProjectModel()
        self.mainwindow = mainwindow

        self._init_actions()

        # treeview
        self.treeview = ProjectTreeView()
        self.treeview.setModel(self.model)
        self.treeview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.treeview.customContextMenuRequested.connect(self.show_contextmenu)
        self.treeview.doubleClicked.connect(self.item_doubleclicked)
        self.treeview.header().setStretchLastSection(False)
        self.treeview.header().setContextMenuPolicy(Qt.ActionsContextMenu)

        for a in self.column_actions:
            self.treeview.header().addAction(a)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.treeview)
        self.setLayout(layout)

        self.load_settings()

    def _init_actions(self):
        self.addProjectAction = create_action(
            self,
            self.tr("add project..."),
            image=":icons/add.png",
            slot=self.add_project,
        )
        self.removeProjectAction = create_action(
            self,
            self.tr("remove project"),
            image=":icons/remove.png",
            slot=self.remove_project,
        )
        self.editProjectAction = create_action(
            self,
            self.tr("edit project..."),
            image=":icons/edit.png",
            slot=self.edit_project,
        )
        self.column_actions = create_column_actions(
            self, self.model.columns, self.toggle_column
        )

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(SETTINGS_GROUP)

        # header state
        try:
            self.treeview.header().restoreState(settings.value(HEADERSTATE_SETTING))
        except TypeError:
            self.treeview.resizeColumnsToContents()

        # hidden columns
        hidden_columns = settings.value(HIDDEN_COLUMNS_SETTING)
        if hidden_columns is not None:
            for i, c in enumerate(self.model.columns):
                if c.name in hidden_columns:
                    self.column_actions[i].setChecked(False)
                    self.treeview.setColumnHidden(i, True)

        settings.endGroup()

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(SETTINGS_GROUP)

        # header state
        settings.setValue(HEADERSTATE_SETTING, self.treeview.header().saveState())

        # invisible columns
        settings.setValue(
            HIDDEN_COLUMNS_SETTING,
            [c.data().name for c in self.column_actions if not c.isChecked()],
        )

        settings.endGroup()

    def closeEvent(self, event):
        self.save_settings()
        event.accept()

    def show_contextmenu(self, pos: QPoint):
        menu = QMenu(self)
        index = self.treeview.currentIndex()
        menu.addAction(self.addProjectAction)

        if index.isValid():
            menu.addAction(self.editProjectAction)
            menu.addSeparator()
            menu.addAction(self.removeProjectAction)
            menu.setDefaultAction(self.editProjectAction)

        menu.popup(self.treeview.viewport().mapToGlobal(pos))

    def add_project(self):
        self.mainwindow.new_project()

    def edit_project(self):
        # get current node
        index = self.treeview.currentIndex()

        if not index.isValid():
            return

        node = index.internalPointer()
        if not isinstance(node, Node):
            return

        self.mainwindow.open_project(node.item)
        self.project_opened.emit()

        # dlg = ProjectDialog(project, self.parent())
        # if dlg.exec_() == QDialog.Accepted:
        #     session.commit()

    def remove_project(self):
        # check for valid index
        index = self.treeview.currentIndex()
        if not index.isValid():
            return

        # check if internal pointer is a Project
        node = index.internalPointer()
        if not isinstance(node.item, db.Project):
            return
        project = node.item

        # if project contains any orders, cancel
        if project.orders:
            QMessageBox.warning(
                self,
                QCoreApplication.applicationName(),
                self.tr(
                    "The project contains orders. Please delete the "
                    "orders or move them to another project."
                ),
                QMessageBox.Ok,
            )
            return

        # safety question
        res = QMessageBox.question(
            self,
            QCoreApplication.applicationName(),
            self.tr(
                "Do you really want to delete the project '%s'? "
                "(All contained orders will not be deleted.)"
            )
            % project.name,
            QMessageBox.Yes | QMessageBox.No,
        )

        # delete, commit and refresh
        if res == QMessageBox.Yes:
            self.model.beginRemoveRows(index.parent(), index.row(), index.row())
            session.delete(project)
            session.commit()
            self.model.refresh()
            self.model.endRemoveRows()

    def item_doubleclicked(self, index: QModelIndex):
        if not index.isValid():
            return
        self.edit_project()

    def refresh(self):
        self.model.beginResetModel()
        self.model.refresh()
        self.model.endResetModel()

    def toggle_column(self):
        for a in self.column_actions:
            column = a.data()
            index = self.model.columns.index(column)
            self.treeview.setColumnHidden(index, not a.isChecked())


class ProjectDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)

        # project explorer
        self.projectexplorer = ProjectExplorer(parent)
        self.projectexplorer.project_opened.connect(self.close)

        # buttons
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)

        # layout
        layout = QVBoxLayout()
        layout.addWidget(self.projectexplorer)
        layout.addWidget(self.buttons)
        self.setLayout(layout)

        # resize
        self.resize(QSize(560, 420))
        self.load_settings()

    def closeEvent(self, event):
        super().closeEvent(event)

    def accept(self):
        self.save_settings()
        self.projectexplorer.edit_project()
        super().accept()

    def reject(self):
        self.save_settings()
        super().reject()

    def load_settings(self):
        settings = QSettings()
        settings.beginGroup(SETTINGS_GROUP)

        # geometry
        try:
            self.restoreGeometry(settings.value(DIALOG_GEOMETRY_SETTING))
        except Exception as e:
            logger.warning(e)

    def save_settings(self):
        settings = QSettings()
        settings.beginGroup(SETTINGS_GROUP)

        # geometry
        settings.setValue(DIALOG_GEOMETRY_SETTING, self.saveGeometry())


if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = ProjectExplorer(db.Session())
    w.show()
    app.exec_()
