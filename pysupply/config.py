import os

# home directory
HOME = os.path.join(os.environ["HOME"], "pysupply")
if not os.path.exists(HOME):
    os.mkdir(HOME)

# Database
basedir = os.path.abspath(os.path.dirname(__file__))
MAX_INT = 1E6
DB_FILENAME = "pysupply.db"
SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(HOME, DB_FILENAME)
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

# articles, orders and projects
ARTICLEDIR = os.path.join(HOME, "articles")
ORDERDIR = os.path.join(HOME, "orders")
PROJECTSDIR = os.path.join(HOME, "projects")
TAX = 1.19
