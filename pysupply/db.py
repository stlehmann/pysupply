"""
Main database module with implemented SQLAlchemy ORM.

:author: Stefan Lehmann <stefan.st.lehmann@gmail.com>
:license: MIT license, see license.txt for details

:created on 2014-04-07 13:03:25
:last modified by:   Stefan Lehmann
:last modified time: 2018-06-21 16:49:41

"""

import sys
import datetime
from typing import Any, List, Dict, Optional
from types import ModuleType

from sqlalchemy import (
    create_engine,
    Column,
    Integer,
    String,
    ForeignKey,
    Float,
    Date,
    Boolean,
)
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base

from .config import SQLALCHEMY_DATABASE_URI

# order states
ORDERSTATE_DRAFT = 0
ORDERSTATE_ORDERED = 1
ORDERSTATE_RECEIVED = 2
orderstates = [ORDERSTATE_DRAFT, ORDERSTATE_ORDERED, ORDERSTATE_RECEIVED]


engine = create_engine(SQLALCHEMY_DATABASE_URI, echo=False)
Session = sessionmaker(bind=engine)
Base: Any = declarative_base()


class Article(Base):
    """
    Mapper class for an article. Connected to the table 'articles'.

    :var int id: Primary Key
    :var str name: name of the article
    :var str manufacturer: name of the manufacturer
    :var str article_nr: manufacturers number of the article
    :var str notes: additional notes

    >>> from pysupply.db import Article
    >>> Article(name="Battery AA", manufacturer="Acme", article_nr="001")
    <Article(name='Battery AA', manufacturer='Acme', article_nr='001')>

    """

    __tablename__ = "articles"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    category_id = Column(Integer, ForeignKey("categories.id"))
    category = relationship("Category", backref=backref("articles", order_by=id))
    manufacturer = Column(String)
    article_nr = Column(String)
    notes = Column(String, default="")
    properties = Column(String)

    stock = relationship("StockItem", uselist=False, backref="article")

    @property
    def distributors(self) -> List["Distributor"]:
        """Get available distributors for this article."""
        return [item.distributor for item in self.catalog_items]

    def __repr__(self) -> str:
        """Return object name."""
        return "<Article id={id}, name={name}>".format(
            id=self.id or "", name=self.name or ""
        )

    @staticmethod
    def properties_str_to_dict(text: str) -> Dict[str, str]:
        """Convert a property string to a dict."""
        text = text.replace(":", "")
        list_ = text.split("\n")
        stripped = map(str.strip, list_)
        properties_dict = dict()

        if "\t" in text:
            keyvalue = (item.split("\t") for item in stripped)
            for item in keyvalue:  # type: Any
                try:
                    key, value = item[0].strip(), item[1].strip()
                    properties_dict[key] = value
                except IndexError:
                    pass
        else:
            step = 0
            key, value = None, None
            for item in stripped:
                if item == "":
                    step = 0
                elif step == 0:
                    key = item
                    step = 1
                elif step == 1:
                    value = item
                    properties_dict[key] = value
                    step = 0

        return properties_dict

    def change_stock(self, quantity: int, order: "Order" = None) -> None:
        """Change stock quantity and generate a StockChangeItem."""
        sc_item = StockChangeItem()
        sc_item.article = self
        sc_item.original_quantity = self.stock.quantity
        sc_item.quantity_change = quantity
        sc_item.resulting_quantity = sc_item.original_quantity + sc_item.quantity_change

        if order is not None:
            sc_item.order = order

        self.stock.quantity += quantity
        session.add(sc_item)
        session.add(self)
        session.commit()


class Category(Base):
    """Mapper class for an article category."""

    __tablename__ = "categories"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    parent_id = Column(Integer)

    def __repr__(self) -> str:
        """Return object name."""
        return "<Category(id=%i, name='%s', parent=%i)>" % (
            self.id,
            self.name,
            self.parent_id,
        )


class Distributor(Base):
    """
    Mapper class for a distributor. Connected to the table 'distributors'.

    :var int id: Primary Key
    :var str name: name of the distributor
    :var str address: postal address of the distributor
    :var str conditions: shopping conditions
    :var float shipping_costs: costs for shipping

    >>> from pysupply.db import Distributor
    >>> Distributor(name="My favorite supplier", shipping_costs=5.00)
    <Distributor(name='My favorite supplier', shipping_costs=5.00)>

    """

    __tablename__ = "distributors"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    address = Column(String)
    contact_person = Column(String(64), default="")
    conditions = Column(String)
    shipping_costs = Column(Float)
    website = Column(String)
    username = Column(String)
    password = Column(String)
    plugin_name = Column(String)

    @property
    def plugin(self) -> Optional[ModuleType]:
        """Return Plugin path."""
        if not self.plugin_name:
            return None

        plugin_path = "pysupply.plugins.distributors." + self.plugin_name
        __import__(plugin_path)
        return sys.modules[plugin_path]

    def __repr__(self) -> str:
        """Return object name."""
        return "<Distributor(name='%s'>" % self.name


class CatalogItem(Base):
    """
    Mapper class for the asssignment of an article to a distributor.

    Connected to the table 'catalog'.

    :ivar int id: Primary Key
    :ivar article_id: Foreign Key of the article
    :ivar pysupply.core.Article article: Article object
    :ivar distributor_id: Foreign Key of the distributor
    :ivar pysupply.core.Distributor distributor: Distributor object
    :ivar int min_qty: minimum quantity
    :ivar float ppu: price per unit

    """

    __tablename__ = "catalog"

    id = Column(Integer, primary_key=True)
    article_id = Column(Integer, ForeignKey("articles.id"))
    article = relationship("Article", backref=backref("catalog_items", order_by=id))
    distributor_id = Column(Integer, ForeignKey("distributors.id"))
    distributor = relationship(
        "Distributor", backref=backref("distributions", order_by=id)
    )
    name = Column(String)
    min_qty = Column(Integer, default=1)
    price = Column(Float)
    order_number = Column(String)
    packing_unit = Column(Integer, default=1)

    def __repr__(self) -> str:
        """Return object name."""
        return "<CatalogItem object id={id}>".format(id=self.id or "")


class Order(Base):
    """Mapper class for an order."""

    __tablename__ = "orders"

    id = Column(Integer, primary_key=True)

    distributor_id = Column(ForeignKey("distributors.id"))
    distributor = relationship("Distributor")

    date = Column(Date)
    number = Column(String)
    name = Column(String)
    notes = Column(String)
    items = relationship(
        "OrderItem",
        cascade="save-update, merge, delete, " "delete-orphan",
        backref=backref("order"),
    )
    shipping_costs = Column(Float)
    state = Column(Integer, default=0)
    locked = Column(Boolean)

    project_id = Column(ForeignKey("projects.id"))
    project = relationship("Project", backref=backref("orders"))

    directory_id = Column(ForeignKey("directories.id"))
    directory = relationship("Directory", backref=backref("orders"))

    def __repr__(self) -> str:
        """Return object name."""
        return "<Order object name={self.name}, number={self.number}>".format(self=self)

    def count(self) -> int:
        """Return count of items."""
        return len(self.items)

    def total(self) -> float:
        """Return total of the order."""
        return sum(item.total() for item in self.items) + (self.shipping_costs or 0)

    def add_material(self, material_item: "MaterialItem") -> None:
        """Add material to the order and convert it to OrderItem."""
        article = material_item.article
        order_item = OrderItem(
            order=self,
            article=article,
            name=article.name,
            manufacturer=article.manufacturer,
            manufacturer_nr=article.article_nr,
            position=len(self.items),
            count=material_item.quantity,
        )

        try:
            catalog_item = next(
                item
                for item in article.catalog_items
                if item.distributor is self.distributor
            )
            order_item.distributor = catalog_item.distributor
            order_item.articlenr = catalog_item.order_number
            order_item.ppu = catalog_item.price
            order_item.packing_unit = catalog_item.packing_unit
        except StopIteration:
            pass

        session.add(order_item)

        # set ordered flag
        material_item.ordered = True
        session.add(material_item)
        session.commit()


class OrderItem(Base):
    """Mapper class for ordered items."""

    __tablename__ = "order_items"

    id = Column(Integer, primary_key=True)

    order_id = Column(ForeignKey("orders.id"))

    catalog_id = Column(ForeignKey("catalog.id"))
    catalog_item = relationship("CatalogItem")

    distributor_id = Column(ForeignKey("distributors.id"))
    distributor = relationship("Distributor")

    article_id = Column(ForeignKey("articles.id"))
    article = relationship("Article")  # type: Article

    count = Column(Integer, default=1)
    name = Column(String, default="")
    articlenr = Column(String, default="")
    manufacturer = Column(String)
    manufacturer_nr = Column(String)
    ppu = Column(Float)
    packing_unit = Column(Float, default=1)
    comment = Column(String, default="")
    position = Column(Integer, default=0)
    added_to_stock = Column(Boolean, default=False)
    received = Column(Boolean, default=False)

    def __repr__(self) -> str:
        """Return object name."""
        return "<OrderItem id={self.id}>".format(self=self)

    def total(self) -> float:
        """Return total amount."""
        if self.count is None or self.ppu is None:
            return 0.
        else:
            return self.count * self.ppu

    def add_to_stock(self) -> None:
        """Add item to stock."""
        # add stock change item
        self.article.change_stock(self.count * self.packing_unit, order=self.order)
        self.added_to_stock = True
        session.add(self)
        session.commit()


class Project(Base):
    """Mapper class for projects."""

    __tablename__ = "projects"

    id = Column(Integer, primary_key=True)

    name = Column(String, default="")
    number = Column(String, default="")
    notes = Column(String, default="")

    directory_id = Column(ForeignKey("directories.id"))
    directory = relationship("Directory", backref=backref("projects"))

    def __repr__(self) -> str:
        """Return object name."""
        s = "<Project object ("
        attr = []
        attr.append("projectnr=%s" % self.number)
        attr.append("name=%s" % self.name)
        attr.append("orders=%i" % len(self.orders))
        s += ",".join(attr) + ")>"
        return s

    def total(self) -> float:
        """Total of all orders belonging to this project."""
        return sum(order.total() for order in self.orders)


class Directory(Base):
    """A directory object contains projects or other directories."""

    __tablename__ = "directories"

    id = Column(Integer, primary_key=True)
    name = Column(String, default="")
    parent_id = Column(Integer, default=0)

    project_id = Column(Integer)

    def __repr__(self) -> str:
        """Return object name."""
        return "<Directory object name=%s>" % self.name

    def total(self) -> float:
        """Total of all orders belonging to this directory."""
        return sum(order.total() for order in self.orders)


class StockItem(Base):
    """An item containing the warehouse stock of a certain article."""

    __tablename__ = "stock"

    id = Column(Integer, primary_key=True)
    article_id = Column(Integer, ForeignKey("articles.id"))
    quantity = Column(Integer, default=0)
    minimum = Column(Integer, default=0)

    def __repr__(self) -> str:
        """Return object name."""
        return "StockItem(article='{}', quantity={}".format(
            self.article.name, self.quantity
        )


class MaterialList(Base):
    """A list of materials items."""

    __tablename__ = "materiallists"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    comment = Column(String)

    project_id = Column(ForeignKey("projects.id"))
    project = relationship("Project", backref=backref("materiallists"))

    def __repr__(self) -> str:
        """Return object name."""
        return "<Materiallist object id={self.id} name='{self.name}'>".format(self=self)

    def remove_from_stock(self) -> None:
        """Remove material list from stock."""
        for item in self.items:
            item.remove_from_stock()

    def return_to_stock(self) -> None:
        """Return material list to stock."""
        for item in self.items:
            item.return_to_stock()

    @property
    def item_count(self) -> int:
        """Return count of all items."""
        return len(self.items)

    @property
    def removed_from_stock(self) -> bool:
        """Return True if all items have been removed from stock."""
        return all(item.removed_from_stock for item in self.items)


class MaterialItem(Base):
    """Mapper class for material items."""

    __tablename__ = "material"

    id = Column(Integer, primary_key=True)
    quantity = Column(Integer, default=0)

    article_id = Column(Integer, ForeignKey("articles.id"))
    article = relationship("Article", backref=backref("material_items"))

    materiallist_id = Column(ForeignKey("materiallists.id"))
    materiallist = relationship("MaterialList", backref=backref("items"))

    order_item_id = Column(ForeignKey("order_items.id"))
    order_item = relationship("OrderItem", backref=backref("material_item"))

    removed_from_stock = Column(Boolean, default=False)
    ordered = Column(Boolean, default=False)
    comment = Column(String, default="")

    def remove_from_stock(self) -> None:
        """Remove item from stock."""
        qty = self.article.stock.quantity
        qty -= self.quantity
        self.article.stock.quantity = qty
        self.removed_from_stock = True

    def return_to_stock(self) -> None:
        """Return item to stock."""
        qty = self.article.stock.quantity
        qty += self.quantity
        self.article.stock.quantity = qty
        self.removed_from_stock = False

    def __repr__(self) -> str:
        """Return object name."""
        try:
            return "<MaterialItem(article='{}', quantity={}, project='{}'>".format(
                self.article.name, self.quantity, self.project.name
            )
        except TypeError:
            return "<MaterialItem object>"

    def __str__(self) -> str:
        """Return printable string representation."""
        return self.article.name


class StockChangeItem(Base):
    """Item that documents the change of stock quantity of an article."""

    __tablename__ = "stockchanges"

    id = Column(Integer, primary_key=True)

    article_id = Column(Integer, ForeignKey("articles.id"))
    article = relationship("Article", backref=backref("stock_changes"))

    original_quantity = Column(Integer)
    quantity_change = Column(Integer)
    resulting_quantity = Column(Integer)

    comment = Column(String(256))
    applied = Column(Boolean, default=False)
    date = Column(Date, default=datetime.datetime.now)

    order_id = Column(Integer, ForeignKey("orders.id"))
    order = relationship("Order")

    materiallist_id = Column(Integer, ForeignKey("materiallists.id"))
    materiallist = relationship("MaterialList")

    def apply_change(self) -> None:
        """Apply change."""
        self.original_quantity = self.article.stock.quantity
        self.resulting_quantity = self.original_quantity + self.quantity_change
        self.article.stock.quantity = self.resulting_quantity
        self.applied = True

    def revert_change(self) -> None:
        """Revert change."""
        qty = self.article.stock.quantity
        qty -= self.quantity_change
        self.article.stock.quantity = qty
        self.applied = False


# Base.metadata.create_all(engine)
session = Session()
