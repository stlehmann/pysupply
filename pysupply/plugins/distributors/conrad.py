#!/usr/bin/env python
"""
conrad.py,
copyright (c) 2016 by Stefan Lehmann

Plugin for conrad.biz

"""
import os
import time
from urllib.request import urlopen, Request
from urllib.error import HTTPError
from urllib.parse import urlparse
import logging
import json
import webbrowser
from pysupply import db
from pysupply.db import CatalogItem, Order, session
from pysupply.config import ARTICLEDIR
from . import to_utf8


logger = logging.getLogger(__name__)
netlocs = ['www.conrad.biz', 'www.conrad.de']


def make_request(url):
    return Request(url, headers={'User-Agent': 'Mozilla'})


def open_item_in_browser(item: CatalogItem):
    """
    Open the given CatalogItem in the browser.

    """
    url = 'http://www.conrad.biz/ce/de/product/' + item.order_number
    webbrowser.open(url, new=2, autoraise=True)


def order_online(order: Order):
    from selenium import webdriver

    driver = webdriver.Firefox()
    driver.get('http://www.conrad.biz/ce/de/DirectOrder.html')
    time.sleep(3)

    for i, item in enumerate(order.items):
        # article number
        el = driver.find_element_by_name('productCode-{}'.format(i))
        el.click()
        el.send_keys(item.articlenr.split('-')[0].strip())

        # quantity
        el = driver.find_element_by_name('productQuantity-{}'.format(i))
        el.click()
        el.clear()
        el.send_keys(item.count)

    el = driver.find_element_by_name('productCode-{}'.format(i))
    el.submit()


def import_article(url: str):
    from pyquery import PyQuery as pq
    from pysupply.db import Article, CatalogItem

    try:
        with urlopen(make_request(url)) as resp:
            html = resp.read()
    except HTTPError as e:
        logger.error(e)

    if html is None:
        return

    doc = pq(html)
    parse_result = urlparse(url)
    if parse_result.netloc == 'www.conrad.biz':

        # article
        article = Article()
        article.name = to_utf8(doc('h1.ccpProductDetail__title__text').text())
        article.manufacturer = to_utf8(doc(
            'img.ccpProductDetail__main__row__cell__topleft__row__brand__image').attr('alt'))
        article.notes = to_utf8(doc('div#description').text())
        article.article_nr = doc(
            'div.ccpProductDetail__main__row__cell__topright__row__info span').eq(1).text()

        # import properties
        tbl = doc('div.tech-data dl')
        properties = {}
        for row in tbl.items():
            key = to_utf8(row('dt').text())
            value = to_utf8(row('dd').text())
            properties[key] = value
        article.properties = json.dumps(properties)

        # catalog item
        distributors = db.session.query(db.Distributor).filter(
            db.Distributor.name == 'Conrad'
        )
        conrad_distributor = distributors.first()
        if conrad_distributor is not None:
            item = CatalogItem()
            item.article = article
            item.distributor = conrad_distributor
            item.price = float(
                doc('div.ccpProductDetailInfo__cell__price__default meta').eq(1).attr('content'))
            item.order_number = doc(
                'div.ccpProductDetail__main__row__cell__topleft__row__info__productid span').text()
            item.name = article.name

        session.add(article)
        session.commit()

        document_dir = os.path.join(ARTICLEDIR, str(article.id))
        _download_documents(doc, document_dir)

        return article

    elif parse_result.netloc == 'www.conrad.de':
        article = Article()
        article.name = to_utf8(doc('h1.ccpProductDetail__title__text').text())
        article.manufacturer = ''
        article.notes = to_utf8(doc('section.block.large.text').text().strip())

        properties = {}
        for el in doc('dl.tech-data__list').items():
            key = to_utf8(el('dt').text())
            value = to_utf8(el('dd').text())
            properties[key] = value
        article.properties = json.dumps(properties)

        distributors = db.session.query(db.Distributor).filter(
            db.Distributor.name == 'Conrad'
        )
        conrad_distributor = distributors.first()
        if conrad_distributor is not None:
            item = CatalogItem()
            item.article = article
            item.distributor = conrad_distributor
            item.price = float(
                doc('div.ccpProductDetailInfo__cell__price__default')
                .text()
                .replace(',', '.')
                .split(' ')[0]
            )
            item.order_number = to_utf8(doc('span[itemprop*="sku"]').text())
            item.name = article.name

        session.add(article)
        session.commit()

        document_dir = os.path.join(ARTICLEDIR, str(article.id))
        _download_documents(doc, document_dir)
        return article


def _get_valid_filename(directory, filename):
    fn_org, ext = os.path.splitext(filename)
    fn = fn_org
    i = 0

    while os.path.isfile(os.path.join(directory, fn + ext)):
        i += 1
        fn = fn_org + str(i)
    return os.path.join(directory, fn + ext)


def _download_documents(doc, directory):
    links = doc('#downloadcenter a')
    for link in links:
        document_name = link.text.strip()
        document_url = link.get('href')

        try:
            with urlopen(document_url) as resp:
                data = resp.read()
        except HTTPError:
            logger.error(f'Could not load document from URL {document_url}')
            continue

        if not os.path.exists(directory):
            os.mkdir(directory)

        document_name = document_name.replace('/', '-')
        fn = _get_valid_filename(directory, document_name + '.pdf')
        with open(fn, 'wb') as f:
            f.write(data)
        logger.info(f'Wrote document {fn}.')
