"""Module for remote control of RSOnline shop.

rsonline.py,
copyright (c) 2016 by Stefan Lehmann

Plugin for rs-online.de

"""
import os
import json
import urllib
import urllib.parse
import urllib.error
import webbrowser
import logging
import socket
from typing import Optional
from pyquery import PyQuery as pq
from urllib.request import urlopen
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from . import to_utf8
from ...db import Article, CatalogItem, Distributor, Order, session
from ...config import ARTICLEDIR


netlocs = ["de.rs-online.com"]
logger = logging.getLogger(__name__)


def make_request(url: str) -> urllib.request.Request:
    """Create request with Mozilla User Agent."""
    user_agent = "Mozilla"
    headers = {"User-Agent": user_agent}
    return urllib.request.Request(url, headers=headers)


def open_item_in_browser(item: CatalogItem) -> None:
    """Open the given CatalogItem in the browser."""
    data = urllib.parse.urlencode({"searchTerm": item.order_number})
    url = "http://de.rs-online.com/web/c/?" + data

    # open url in webbrowser inside a new tab
    webbrowser.open(url, new=2, autoraise=True)


def order_online(order: Order) -> None:
    """Create a new online order."""
    s = ""
    for item in order.items:
        article_nr = item.articlenr
        qty = item.count
        s += "{0},{1}\n".format(article_nr, qty)

    driver = webdriver.Firefox()
    driver.get("http://de.rs-online.com/web/ca/Warenkorb/")

    # click on "Schnell bestellen"
    elem = driver.find_element_by_class_name("toggleAddProductsComponentPanel")
    elem.click()

    try:
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "quickAddSelectionDiv"))
        )
    except TimeoutError:
        print("Timeout")
        return

    elem = driver.find_element_by_class_name("quickAddSelectionDiv")
    elem.click()

    id_ = (
        "shoppingBasketForm:QuickOrderWidgetAction_quickOrderTextBox_decorate"
        ":QuickOrderWidgetAction_listItems"
    )

    try:
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, id_)))
    except TimeoutError:
        print("timeout")
        return

    inputbox = driver.find_element_by_id(id_)
    inputbox.send_keys(s)

    try:
        btn = WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "quickOrderTxtAddBtn"))
        )
    except TimeoutError:
        print("timeout")
        return

    btn.click()


def import_article(url: str) -> Optional[Article]:
    """Import Article from URL."""
    # load url
    req = make_request(url)
    try:
        with urlopen(req) as resp:
            html = resp.read()
    except urllib.error.HTTPError as e:
        logger.error(str(e))
        return None

    doc = pq(html)

    # get article properties
    article = Article()
    article.name = to_utf8(doc("h1").text())
    article.manufacturer = to_utf8(
        doc("ul.keyDetailsLL")("li").eq(1)("span.keyValue").text()
    )
    article.notes = to_utf8(doc("p.overViewList").text())
    article.article_nr = to_utf8(
        doc("ul.keyDetailsLL")("li").eq(0)("span.keyValue").text()
    )

    tbl = doc("div.specifications table")

    properties = {}
    for row in tbl("tr").not_(".table-title").items():
        key = to_utf8(row("td").eq(0).text())
        value = to_utf8(row("td").eq(1).text())
        properties[key] = value
    article.properties = json.dumps(properties)

    # catalog item
    distributors = session.query(Distributor).filter(Distributor.name == "RS")
    rsonline_dist = distributors.first()
    if rsonline_dist is not None:
        item = CatalogItem()
        item.article = article
        item.distributor = rsonline_dist
        item.price = float(doc("div.product-price .price").text().replace(",", "."))
        item.order_number = doc("ul.keyDetailsLL")("li").eq(0)("span.keyValue").text()
        item.name = article.name

    # documents
    session.add(article)
    session.commit()

    document_dir = os.path.join(ARTICLEDIR, str(article.id))
    _download_documents(doc, document_dir)

    return article


def _get_valid_filename(directory: str, filename: str) -> str:
    """Create a valid filename."""
    fn_org, ext = os.path.splitext(filename)
    fn = fn_org
    i = 0

    while os.path.isfile(os.path.join(directory, fn + ext)):
        i += 1
        fn = fn_org + str(i)
    return os.path.join(directory, fn + ext)


def _download_documents(doc: pq, directory: str, timeout: int = 5) -> None:
    links = doc(".technical-row").eq(0)("a")
    for link in links:
        document_name = link.text
        document_url = link.get("href")

        # download document
        try:
            with urlopen(make_request(document_url), timeout=timeout) as resp:
                data = resp.read()
        except socket.timeout as e:
            logger.warning(f"Could not load document from url {document_url}")
        else:

            if not os.path.exists(directory):
                os.makedirs(directory, exist_ok=True)

            document_name = document_name.replace("/", "-")
            fn = _get_valid_filename(directory, document_name + ".pdf")
            with open(fn, "wb") as f:
                f.write(data)
            logger.info(f"Wrote document {fn}.")
