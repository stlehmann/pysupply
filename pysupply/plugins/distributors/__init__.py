"""
__init__.py,
copyright (c) 2016 by Stefan Lehmann

"""

import pkgutil


def get_plugin_list():
    """
    List all available distributor plugins.

    """
    return [module for _, module, ispkg in
            pkgutil.iter_modules(__path__)
            if not ispkg]


def to_utf8(s, codec='latin-1'):
    """Transform website content to utf-8"""
    return s.encode(codec).decode("utf-8", errors="ignore")
