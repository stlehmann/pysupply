import os
from datetime import datetime
from collections import namedtuple
from PyQt5.QtCore import QSettings
from docxtpl import DocxTemplate, RichText
from pysupply.db import session, Order
from pysupply.gui.user_information import USER_NAME_SETTING, USER_SIGN_SETTING

Item = namedtuple('Item', 'name count ppu articlenr')


def str_format_item(item):
    return Item(
        item.name,
        '{}'.format(item.count),
        '{:.2f}'.format(item.ppu),
        item.articlenr
    )


def export_order_to_docx(order, template='templates/template.docx',
                 output='generated_doc.docx'):
    settings = QSettings()
    template = os.path.join(os.path.dirname(__file__), template)
    doc = DocxTemplate(template)

    user_name = settings.value(USER_NAME_SETTING, '')
    user_sign = settings.value(USER_SIGN_SETTING, '')

    items = [str_format_item(item) for item in order.items]
    distributor = order.distributor
    context = {
        'company': RichText(distributor.name),
        'contact_person': RichText(distributor.contact_person),
        'address': RichText(distributor.address),
        'items': items,
        'user_name': RichText(user_name),
        'date': datetime.now().strftime('%d.%m.%Y'),
        'order_nr': RichText(order.number or ""),
        'total_price': '{:.2f}'.format(order.total()),
        'user_sign': RichText(user_sign),
        'shipping_costs': '{:.2f}'.format(order.shipping_costs or 0)
    }
    doc.render(context)
    doc.save(output)


if __name__ == '__main__':
    # export_order(None)
    order = session.query(Order).filter_by(name='Heizdorn').first()
    output = 'output.docx'
    export_order_to_docx(order, output=output)
    os.system('start {}'.format(output))
