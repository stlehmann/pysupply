#! python3
from setuptools import setup
import versioneer

setup(
    name='pysupply',
    version=versioneer.get_version(),
    packages=['pysupply', 'pysupply.gui'],
    url='https://bitbucket.org/mrleeh/pysupply',
    license='GPL',
    author='Stefan Lehmann',
    author_email='stefan.st.lehmann@gmail.com',
    scripts=['run_pysupply.pyw'],
    description='Package for article and supply management and '
                'optimized purchasing ',
    install_requires=[
        'pyqt5>=5.8.1',
        'sqlalchemy>=0.9.8',
        'markdown2>=2.3.0',
        'docxtpl>=0.2.1',
        'validators>=0.11.2',
        'lxml>=3.7.3'
    ],
    cmdclass=versioneer.get_cmdclass()
)
