# pysupply

## Overview

A python package for logistical purchasing of articles. Create an requisition
by choosing the articles you want to order and the quantity. The optimal
purchase order will be created by analysing the prices of each available
distributor.

## Features

* add articles, distributors and catalog items
* arrange requisitions
* create the optimal purchase arrangement considering different distributors
  and shipping costs

