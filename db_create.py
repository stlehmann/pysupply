#!/usr/bin/env python
"""
db_create.py,
copyright (c) 2016 by Stefan Lehmann

"""
import os.path
from migrate.versioning import api

# import database module and configurations,
from pysupply.db import engine, Base
from pysupply.config import SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO


Base.metadata.create_all(engine)


if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
    api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
    api.version_control(
        SQLALCHEMY_DATABASE_URI,
        SQLALCHEMY_MIGRATE_REPO
    )
else:
    api.version_control(
        SQLALCHEMY_DATABASE_URI,
        SQLALCHEMY_MIGRATE_REPO,
        api.version(SQLALCHEMY_MIGRATE_REPO)
    )
